﻿<%@ Assembly Name="HR_learning_And_Mgmt, Version=1.0.0.0, Culture=neutral, PublicKeyToken=c734c90b931f5fb0" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditForm.aspx.cs" Inherits="HR_learning_And_Mgmt.Layouts.HR_learning_And_Mgmt.EditForm" DynamicMasterPageFile="~masterurl/default.master" %>

<asp:Content ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    <SharePoint:ListFormPageTitle runat="server" />
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    <SharePoint:ListProperty Property="LinkTitle" runat="server" ID="ID_LinkTitle" />
    : Edit Form
    <SharePoint:ListItemProperty ID="ID_ItemProperty" MaxLength="40" runat="server" />
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderPageImage" runat="server">
    <img src="/_layouts/images/blank.gif" width="1" height="1" alt="">
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <table bordercolor="#008000" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td style="width: 100%" valign="top" align="left">
                <table class="ms-formtoolbar" id="tblTopButtons" cellspacing="0" cellpadding="2"
                    width="100%" border="0">
                    <tr>
                        <td class="ms-toolbar" nowrap width="99%">
                            <img height="18" alt="" src="/_layouts/images/blank.gif" width="1" />
                        </td>
                        <td class="ms-toolbar" nowrap>
                            <asp:Button class="ms-ButtonHeightWidth" ID="Button1" runat="server" Text="Show Main Form"
                                OnClientClick="javascript:return ShowMainTable();" />
                        </td>
                        <td class="ms-separator">
                            &nbsp;
                        </td>
                        <td class="ms-toolbar" nowrap>
                            <asp:Button class="ms-ButtonHeightWidth180" ID="Button2" runat="server" Text="Show Programme Coordination CheckList"
                                OnClientClick="javascript:return ShowProgrammeCoordinationTable();" />
                        </td>
                        <td class="ms-separator">
                            &nbsp;
                        </td>
                        <td class="ms-toolbar" nowrap>
                            <SharePoint:GoBackButton ID="GoBackButton1" runat="server" ControlMode="Display" />
                        </td>
                    </tr>
                </table>
                <SharePoint:FormToolBar runat="server" ID="aaa" ControlMode="Edit">
                </SharePoint:FormToolBar>
                <table class="ms-formtable" style="margin-top: 8px; display: block;" cellspacing="0"
                    cellpadding="0" width="100%" border="0" id="MainFormTable">
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap width="195">
                            <h3 class="ms-standardheader">
                                <nobr>Line Manager<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblLineManager" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>Senior Manager<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblSeniorManager" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>General Manager<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblGeneralManager" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formbody" valign="top" width="595" colspan="2" style="font-weight: bold">
                            Section 1: Programme Details
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap width="195">
                            <h3 class="ms-standardheader">
                                <nobr>Programme Name<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblProgrammeName" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap width="195">
                            <h3 class="ms-standardheader">
                                <nobr>Programme Date<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblProgrammeDate" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>Programme Venue<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblProgrammeVenue" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap width="195">
                            <h3 class="ms-standardheader">
                                <nobr>Provider<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblProvider" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap width="195">
                            <h3 class="ms-standardheader">
                                Is It an MTN Academy Programme<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblIsMTNAcademyProgramme" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="IsMTNAcademyProgrammeOthersRow" runat="server" style="display: none">
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>Others<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblIsMTNProgrammeOthers" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Programme Coordinator<SPAN class="ms-formvalidation"> *</SPAN>
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblProgrammeCoordinator" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>Programme Facilitator<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblProgrammeFacilitator" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap width="195">
                            <h3 class="ms-standardheader">
                                Faculty<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblFaculty" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Facilitator's Last Programme and Date
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblFacilitatorLastProgrammeAndDate" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Facilitator's Last Evaluation Score
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblFacilitatorLastEvaluationScore" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formbody" valign="top" width="595" colspan="2" style="font-weight: bold">
                            Section 2: Programme Venue
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Venue Arrangement<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblVenueArrangement" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap width="195">
                            <h3 class="ms-standardheader">
                                Equipments Required<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblEquipmentsRequired" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="OthersEquipmentsRequiredRow" runat="server" style="display: none">
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Others Equipments Required<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblOthersEquipmentsRequired" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formbody" valign="top" width="595" colspan="2" style="font-weight: bold">
                            Section 3: Programme Content
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Programme Content
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblProgrammeContent" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Case Study
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblCaseStudy" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Activities
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblActivities" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formbody" valign="top" width="595" colspan="2">
                            <table cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="font-weight: bold">
                                        Section 4: Programme Coordination
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formbody" valign="top" width="595" colspan="2">
                            <table cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="font-size: 11px; padding-left: 5px">
                                        a. Pre-programme delivery checklist
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Venue Booking
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblBooking" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Training Materials
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblTrainingMaterials" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Delegate Availability Sent
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblDelegateAvailabilitySent" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Invites Sent
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblInvitesSent" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Delegate Pre-Work
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblDelegatePreWork" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Customized Training KP1 Evaluation Form
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblCustomizedTrainingKP1EvaluationForm" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="PostProgrammeHeaderRow" runat="server" visible="false">
                        <td class="ms-formbody" valign="top" width="595" colspan="2">
                            <table cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="font-size: 11px; padding-left: 5px">
                                        b. Post-programme delivery checklist
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="PostProgrammeRow1" runat="server" visible="false">
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Completed Training Evaluation Forms
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblCompletedTrainingEvaluationForms" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr id="PostProgrammeRow2" runat="server" visible="false">
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Evaluation Report
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblEvaluationReport" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr runat="server" id="rowTrainingEvaluationReport" visible="false">
                        <td class="ms-formlabel" valign="top" nowrap width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                                Training Evaluation Report<SPAN class="ms-formvalidation"> *</SPAN>
                                </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:HyperLink ID="hlTrainingEvaluationReport" runat="server" Text="&nbsp;" NavigateUrl="#"></asp:HyperLink>
                        </td>
                    </tr>
                </table>
                <table class="ms-formtable" style="margin-top: 0px; display: none;" cellspacing="0"
                    cellpadding="0" width="100%" border="0" id="ProgrammeCoordinationTable">
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap width="195">
                            <h3 class="ms-standardheader">
                                Programme Coordination Checklist<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <asp:Label ID="lblProgrammeCoordinationChecklist" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            ITF Form 6A
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblITFForm6A" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (3 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="ExternalRow1" runat="server" style="display: none">
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Invitation Letter For Visa To Facilitator
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblInvitationLetterForVisaToFacilitator" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (1 month before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="ExternalRow2" runat="server" style="display: none">
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Protocol / Transport Arrangements
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblProtocolOrTransportArrangements" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (1 month before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="ExternalRow3" runat="server" style="display: none">
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Accomodation Arrangements For Facilitator
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblAccomodationArrangementsForFacilitator" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (1 month before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Facilitator's Kit (where applicable)
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblFacilitatorKit" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (3 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Delegate Availability To Line Managers
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblDelegateAvailabilityToLineManagers" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (Minimum 3 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="ExternalRow4" runat="server" style="display: none">
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Retention Contract
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblRetentionContract" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (Minimum 2 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Training Invitation
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblTrainingInvitation" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (Minimum 2 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="ExternalRow5" runat="server" style="display: none">
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Travel Requisitions (where applicable)
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblTravelRequisitions" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (Minimum 2 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Venue Preparation
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblVenuePreparation" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (Minimum 3 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Hall
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblHall" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (Minimum 3 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Projector
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td valign="top">
                                        <nobr>
                                        <asp:Label ID="lblProjector" Text="&nbsp;" runat="server"></asp:Label>
                                        </nobr>
                                    </td>
                                    <td align="right">
                                        (Minimum 3 weeks before training) "confirm 24 hours before training"
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Hall Arrangement
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblHallArrangement" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (Minimum 3 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Location
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblLocation" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (Minimum 3 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Tea Break & Lunch
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblTeaBreakAndLunch" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (Minimum 3 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Seating Arrangement
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblSeatingArrangement" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (Minimum 3 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Location Branding
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblLocationBranding" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (24 hours before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Programme Materials
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblProgrammeMaterials" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (1 month before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Wall Charts
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblWallCharts" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (1 month before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Vidoes / DVD
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblVidoesOrDVD" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (1 month before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Participant Guide
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblParticipantGuide" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (1 month before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Presentation Slides
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPresentationSlides" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (1 month before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Learning Aids - Activity Cards / Case Studies
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblLearningAidsActivityCardsOrCaseStudies" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (1 month before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Stationaries (Biros/pencils/blue tack/post-it)
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblStationaries" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (2 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                External Speakers (where applicable)
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblExternalSpeakers" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (48 hours before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Pre-Course (where applicable)
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPreCourse" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (2 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            KP1 Evaluation
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblKP1Evaluation" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (1 month before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Certificates
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblCertificates" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (48 hours)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Attendance List
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblAttendanceList" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (24 hours)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Photographs
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPhotographs" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (48 hours)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ms-formlabel" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Post Assessment (KP3)
                          </nobr>
                            </h3>
                        </td>
                        <td class="ms-formbody" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPostAssessment" Text="&nbsp;" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        (6 weeks after training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tbody>
                        <tr>
                            <td class="ms-formline">
                                <img height="1" alt="" src="/_layouts/images/blank.gif" width="1" />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="ms-formtoolbar" id="tblBottomButtons" cellspacing="0" cellpadding="2"
                    width="100%" border="0">
                    <tr>
                        <td class="ms-toolbar" nowrap width="99%">
                            <img height="18" alt="" src="/_layouts/images/blank.gif" width="1" />
                        </td>
                        <td class="ms-toolbar" nowrap>
                            <asp:Button class="ms-ButtonHeightWidth" ID="btnShowMainTable" runat="server" Text="Show Main Form"
                                OnClientClick="javascript:return ShowMainTable();" />
                        </td>
                        <td class="ms-separator">
                            &nbsp;
                        </td>
                        <td class="ms-toolbar" nowrap>
                            <asp:Button class="ms-ButtonHeightWidth180" ID="btnShowProgrammeCoordinationTable"
                                runat="server" Text="Show Programme Coordination CheckList" OnClientClick="javascript:return ShowProgrammeCoordinationTable();" />
                        </td>
                        <td class="ms-separator">
                            &nbsp;
                        </td>
                        <td class="ms-toolbar" nowrap>
                            <SharePoint:GoBackButton ID="GoBackButton2" runat="server" ControlMode="Display" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">
        //Load Script:

        //End of Load Script
        function ShowMainTable() {
            // tblBottomButtons.style["display"] = "none";
            // tblShowProgrammeCoordinationTable.style["display"] = "block";
            ProgrammeCoordinationTable.style["display"] = "none";
            MainFormTable.style["display"] = "block";
            return false;
        }

        function ShowProgrammeCoordinationTable() {
            //tblBottomButtons.style["display"] = "block";
            //tblShowProgrammeCoordinationTable.style["display"] = "none";
            ProgrammeCoordinationTable.style["display"] = "block";
            MainFormTable.style["display"] = "none";
            //scroll(0, 0);
            return false;
        }
    </script>

</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderTitleLeftBorder" runat="server">
    <table cellpadding="0" height="100%" width="100%" cellspacing="0">
        <tr>
            <td class="ms-areaseparatorleft">
                <img src="/_layouts/images/blank.gif" width="1" height="1" alt="">
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderTitleAreaClass" runat="server">

    <script id="onetidPageTitleAreaFrameScript">
        document.getElementById("onetidPageTitleAreaFrame").className = "ms-areaseparator";
    </script>

</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderBodyAreaClass" runat="server">
    <style type="text/css">
        .ms-bodyareaframe
        {
            padding: 8px;
            border: none;
        }
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderBodyLeftBorder" runat="server">
    <div class='ms-areaseparatorleft'>
        <img src="/_layouts/images/blank.gif" width="8" height="100%" alt=""></div>
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderTitleRightMargin" runat="server">
    <div class='ms-areaseparatorright'>
        <img src="/_layouts/images/blank.gif" width="8" height="100%" alt=""></div>
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderBodyRightMargin" runat="server">
    <div class='ms-areaseparatorright'>
        <img src="/_layouts/images/blank.gif" width="8" height="100%" alt=""></div>
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderTitleAreaSeparator" runat="server" />

