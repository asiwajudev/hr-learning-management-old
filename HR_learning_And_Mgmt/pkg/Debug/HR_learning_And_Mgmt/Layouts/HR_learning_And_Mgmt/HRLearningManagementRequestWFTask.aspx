﻿<%@ Assembly Name="HR_learning_And_Mgmt, Version=1.0.0.0, Culture=neutral, PublicKeyToken=c734c90b931f5fb0" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HRLearningManagementRequestWFTask.aspx.cs" Inherits="HR_learning_And_Mgmt.Layouts.HR_learning_And_Mgmt.HRLearningManagementRequestWFTask" DynamicMasterPageFile="~masterurl/default.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <link href="ClientStyles/Common.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    HR Learning Management Request</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea"
    runat="server">
    HR Learning Management Request
</asp:Content>
<asp:Content ID="HomePageContent" ContentPlaceHolderID="PlaceHolderMain" runat="Server">
    <style>
        .commentstablestyle
        {
            font-size: 12px;
            font-family: arial;
            color: #505050;
            border: solid 2px #CDCDCD;
            border-collapse: collapse;
            margin-bottom: 2px;
            background-color: #FEFEFE;
        }
        .commentsdate
        {
            font-size: 11px;
            color: #990000;
        }
    </style>
    <table bordercolor="#008000" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td style="width: 100%" valign="top" align="left">
                <table class="ms-formtable" style="margin-top: 8px" cellspacing="0" cellpadding="0"
                    width="100%" border="0">
                    <tr runat="server" id="rowMessage" visible="false">
                        <td colspan="2" valign="top" nowrap width="595" style="color: Red">
                        </td>
                    </tr>
					   <tr>
                        <td class="td_Label" valign="top" width="auto" colspan="2" style="font-weight: bold">
                        <div class="div_Header">
                            Approval
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="195">
                            <h3 class="ms-standardheader">
                                <nobr>User Name</nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblUserName" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="195">
                            <h3 class="ms-standardheader">
                                <nobr>E-Mail</nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblEMail" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr runat="server" id="rowTrainingEvaluationReport" visible="false">
                        <td class="td_Field" valign="top" nowrap width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                                Training Evaluation Report<SPAN class="ms-formvalidation"> *</SPAN>
                                </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:HiddenField ID="_hfTrainingEvaluationReport" runat="server" />
                            <asp:FileUpload ID="_fuTrainingEvaluationReport" runat="server" Width="390px" EnableViewState="true" />
                            <SharePoint:InputFormRequiredFieldValidator ID="_fuTrainingEvaluationReportValidator"
                                class="ms-error" ControlToValidate="_fuTrainingEvaluationReport" Text="Error"
                                runat="server" ErrorMessage="You must specify a value for this required field."
                                EnableClientScript="true" Display="Dynamic">
                            </SharePoint:InputFormRequiredFieldValidator>
                            <SharePoint:InputFormRegularExpressionValidator ID="FileUdpLoadValidator" runat="server"
                                ErrorMessage="Only files with these extensions are allowed .pdf .doc .docx .ppt .pptx"
                                ValidationExpression=".*(\.doc|\.DOC|\.ppt|\.PPT|\.pptx|\.PPTX|\.pdf|\.PDF|\.docx|\.DOCX)$"
                                ControlToValidate="_fuTrainingEvaluationReport"></SharePoint:InputFormRegularExpressionValidator>
                            <table>
                                <tr>
                                    <td nowrap>
                                        <asp:HyperLink ID="_hlTrainingEvaluationReport" runat="server" Text="&nbsp;" NavigateUrl="#"
                                            Style="display: none"></asp:HyperLink>
                                    </td>
                                    <td>
                                        <input id="_btnShowHideFileUpload" onclick="javascript:ShowHideFileUpload()" type="button"
                                            value="Update" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="195">
                            <h3 class="ms-standardheader">
                                <nobr>Comments</nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Literal ID="ltlCommentsHistory" runat="server" Mode="Transform"></asp:Literal>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="195">
                            <h3 class="ms-standardheader">
                                <nobr>New Comment</nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:TextBox ID="txtComment" ClientIDMode="Static" runat="server" Rows="3" TextMode="MultiLine" Width="400px"></asp:TextBox>
                            <span style="display: none" class="ms-error" id="spanTxtComment" runat="server">Comment is Required</span>
                        </td>
                    </tr>
                </table>
                <table class="ms-formtable" style="margin-top: 8px; display: block;" cellspacing="0"
                    cellpadding="0" width="100%" border="0" id="MainFormTable">
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="195">
                            <h3 class="ms-standardheader">
                                <nobr>Line Manager<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblLineManager" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>Senior Manager<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblSeniorManager" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>General Manager<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblGeneralManager" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Label" valign="top" width="595" colspan="2" style="font-weight: bold">
                            Section 1: Programme Details
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="195">
                            <h3 class="ms-standardheader">
                                <nobr>Programme Name<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblProgrammeName" Text="&nbsp;" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="195">
                            <h3 class="ms-standardheader">
                                <nobr>Programme Date<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblProgrammeDate" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:DateTimeControl Visible="false" ID="dtcProgrammeDate" LocaleId="2057"
                                ToolTip="DD/MM/YYYY" runat="server" DateOnly="true" IsRequiredField="true">
                            </SharePoint:DateTimeControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>Programme Venue<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblProgrammeVenue" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:InputFormTextBox Visible="false" ID="txtProgrammeVenue" RichText="false"
                                TextMode="Singleline" runat="server" MaxLength="255" title="Programme Venue"
                                class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator Visible="false" ID="txtProgrammeVenueValidator"
                                class="ms-error" ControlToValidate="txtProgrammeVenue" Text="Error" runat="server"
                                ErrorMessage="You must specify a value for this required field." EnableClientScript="true"
                                Display="Dynamic">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="195">
                            <h3 class="ms-standardheader">
                                <nobr>Provider<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblProvider" Text="&nbsp;" runat="server"></asp:Label>
                            <asp:DropDownList Visible="false" class="ms-RadioText" ID="ddlProvider" runat="server">
                            </asp:DropDownList>
                            <SharePoint:InputFormRequiredFieldValidator Visible="false" ID="ddlProviderValidator"
                                class="ms-error" ControlToValidate="ddlProvider" Text="Error" runat="server"
                                ErrorMessage="You must specify a value for this required field." EnableClientScript="true"
                                Display="Dynamic">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="195">
                            <h3 class="ms-standardheader">
                                Is It an MTN Academy Programme<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblIsMTNAcademyProgramme" Text="&nbsp;" runat="server"></asp:Label>
                            <asp:RadioButtonList Visible="false" ID="rblIsMTNAcademyProgramme" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="Yes" onclick="javascript:ShowHideIsMTNProgrammeOthers();">Yes</asp:ListItem>
                                <asp:ListItem Value="No" onclick="javascript:ShowHideIsMTNProgrammeOthers();">No</asp:ListItem>
                                <asp:ListItem Value="Others" onclick="javascript:ShowHideIsMTNProgrammeOthers();">Others</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr id="IsMTNAcademyProgrammeOthersRow" runat="server" style="display: none">
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>Others<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblIsMTNProgrammeOthers" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:InputFormTextBox Visible="false" ID="txtIsMTNProgrammeOthers" RichText="false"
                                TextMode="Singleline" runat="server" MaxLength="255" title="Others" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator Visible="false" ID="txtIsMTNProgrammeOthersValidator"
                                class="ms-error" ControlToValidate="txtIsMTNProgrammeOthers" Text="Error" runat="server"
                                ErrorMessage="You must specify a value for this required field." EnableClientScript="true"
                                Display="Dynamic" Enabled="false">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Programme Coordinator<SPAN class="ms-formvalidation"> *</SPAN>
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblProgrammeCoordinator" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:PeopleEditor Visible="false" ID="peProgrammeCoordinator" runat="server"
                                Rows="1" MaximumEntities="1" MultiSelect="false" PlaceButtonsUnderEntityEditor="false"
                                PrincipalSource="All" SelectionSet="User" AllowEmpty="false" />
                            <span style="display: none" class="ms-error" id="spanProgrammeCoordinator" runat="server">
                                You must specify a value for this required field. </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>Programme Facilitator<SPAN class="ms-formvalidation"> *</SPAN></nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblProgrammeFacilitator" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:InputFormTextBox Visible="false" ID="txtProgrammeFacilitator" RichText="false"
                                TextMode="Singleline" runat="server" MaxLength="255" title="Programme Facilitator"
                                class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator Visible="false" ID="txtProgrammeFacilitatorValidator"
                                class="ms-error" ControlToValidate="txtProgrammeFacilitator" Text="Error" runat="server"
                                ErrorMessage="You must specify a value for this required field." EnableClientScript="true"
                                Display="Dynamic">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="195">
                            <h3 class="ms-standardheader">
                                Faculty<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblFaculty" Text="&nbsp;" runat="server"></asp:Label>
                            <asp:CheckBoxList Visible="false" ID="cblFaculty" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Internal">Internal</asp:ListItem>
                                <asp:ListItem Value="External">External</asp:ListItem>
                            </asp:CheckBoxList>
                            <span style="display: none" class="ms-error" id="spanFaculty" runat="server">You must
                                specify a value for this required field. </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Facilitator's Last Programme and Date
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblFacilitatorLastProgrammeAndDate" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:InputFormTextBox Visible="false" ID="txtFacilitatorLastProgrammeAndDate"
                                RichText="false" TextMode="Singleline" runat="server" MaxLength="255" title="Facilitator Last Programme and Date"
                                class="ms-long" />
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Facilitator's Last Evaluation Score
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblFacilitatorLastEvaluationScore" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:InputFormTextBox Visible="false" ID="txtFacilitatorLastEvaluationScore"
                                RichText="false" TextMode="Singleline" runat="server" MaxLength="5" title="Facilitator Last Evaluation Score"
                                class="ms-long" />
                            <asp:RangeValidator ID="txtFacilitatorLastEvaluationScoreValidator" Type="Double"
                                Visible="false" runat="server" MinimumValue="0" MaximumValue="100" ErrorMessage="Evaluation score must be in between 0-100"
                                class="ms-error" ControlToValidate="txtFacilitatorLastEvaluationScore" Text="Evaluation score must be in between 0-100"
                                EnableClientScript="true" Display="Dynamic">
                            </asp:RangeValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Label" valign="top" width="595" colspan="2" style="font-weight: bold">
                            Section 2: Programme Venue
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Venue Arrangement<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblVenueArrangement" Text="&nbsp;" runat="server"></asp:Label>
                            <asp:RadioButtonList Visible="false" ID="rblVenueArrangement" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="U-Shape">U-Shape</asp:ListItem>
                                <asp:ListItem Value="Cabaret">Cabaret</asp:ListItem>
                                <asp:ListItem Value="Classroom">Others</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="195">
                            <h3 class="ms-standardheader">
                                Equipments Required<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblEquipmentsRequired" Text="&nbsp;" runat="server"></asp:Label>
                            <asp:CheckBoxList Visible="false" ID="cblEquipmentsRequired" runat="server" RepeatDirection="Horizontal"
                                RepeatColumns="3">
                                <asp:ListItem Value="Projector">Projector</asp:ListItem>
                                <asp:ListItem Value="Flip Chart Stand & Paper">Flip Chart Stand & Paper</asp:ListItem>
                                <asp:ListItem Value="Video">Video</asp:ListItem>
                                <asp:ListItem Value="Speakers">Speakers</asp:ListItem>
                                <asp:ListItem Value="Others" onclick="javascript:ShowHideOthersEquipmentsRequired();">Others</asp:ListItem>
                            </asp:CheckBoxList>
                            <span style="display: none" class="ms-error" id="spanEquipmentsRequired" runat="server">
                                You must specify a value for this required field. </span>
                        </td>
                    </tr>
                    <tr id="OthersEquipmentsRequiredRow" runat="server" style="display: none">
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Others Equipments Required<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblOthersEquipmentsRequired" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:InputFormTextBox Visible="false" ID="txtOthersEquipmentsRequired" RichText="false"
                                TextMode="Singleline" runat="server" MaxLength="255" title="Others Equipments Required" class="ms-long" />
                            <SharePoint:InputFormRequiredFieldValidator Visible="false" ID="txtOthersEquipmentsRequiredValidator"
                                class="ms-error" ControlToValidate="txtOthersEquipmentsRequired" Text="Error"
                                runat="server" ErrorMessage="You must specify a value for this required field."
                                EnableClientScript="true" Display="Dynamic" Enabled="false">
                            </SharePoint:InputFormRequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Label" valign="top" width="595" colspan="2" style="font-weight: bold">
                            Section 3: Programme Content
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Programme Content
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblProgrammeContent" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:DateTimeControl Visible="false" ID="dtcProgrammeContent" LocaleId="2057"
                                ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                            </SharePoint:DateTimeControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Case Study
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblCaseStudy" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:DateTimeControl Visible="false" ID="dtcCaseStudy" LocaleId="2057" ToolTip="DD/MM/YYYY"
                                runat="server" DateOnly="true">
                            </SharePoint:DateTimeControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Activities
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblActivities" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:DateTimeControl Visible="false" ID="dtcActivities" LocaleId="2057" ToolTip="DD/MM/YYYY"
                                runat="server" DateOnly="true">
                            </SharePoint:DateTimeControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Label" valign="top" width="595" colspan="2">
                            <table cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="font-weight: bold">
                                        Section 4: Programme Coordination
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Label" valign="top" width="595" colspan="2">
                            <table cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="font-size: 11px; padding-left: 5px">
                                        a. Pre-programme delivery checklist
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Venue Booking
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblBooking" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:DateTimeControl Visible="false" ID="dtcBooking" LocaleId="2057" ToolTip="DD/MM/YYYY"
                                runat="server" DateOnly="true">
                            </SharePoint:DateTimeControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Training Materials
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblTrainingMaterials" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:DateTimeControl Visible="false" ID="dtcTrainingMaterials" LocaleId="2057"
                                ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                            </SharePoint:DateTimeControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Delegate Availability Sent
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblDelegateAvailabilitySent" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:DateTimeControl Visible="false" ID="dtcDelegateAvailabilitySent" LocaleId="2057"
                                ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                            </SharePoint:DateTimeControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Invites Sent
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblInvitesSent" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:DateTimeControl Visible="false" ID="dtcInvitesSent" LocaleId="2057" ToolTip="DD/MM/YYYY"
                                runat="server" DateOnly="true">
                            </SharePoint:DateTimeControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Delegate Pre-Work
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblDelegatePreWork" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:DateTimeControl Visible="false" ID="dtcDelegatePreWork" LocaleId="2057"
                                ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                            </SharePoint:DateTimeControl>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Customized Training KP1 Evaluation Form
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblCustomizedTrainingKP1EvaluationForm" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:DateTimeControl Visible="false" ID="dtcCustomizedTrainingKP1EvaluationForm"
                                LocaleId="2057" ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                            </SharePoint:DateTimeControl>
                        </td>
                    </tr>
                    <tr id="PostProgrammeHeaderRow" runat="server" visible="false">
                        <td class="td_Label" valign="top" width="595" colspan="2">
                            <table cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="font-size: 11px; padding-left: 5px">
                                        b. Post-programme delivery checklist
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="PostProgrammeRow1" runat="server" visible="false">
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Completed Training Evaluation Forms<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblCompletedTrainingEvaluationForms" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:DateTimeControl Visible="false" ID="dtcCompletedTrainingEvaluationForms"
                                LocaleId="2057" ToolTip="DD/MM/YYYY" runat="server" DateOnly="true" IsRequiredField="true">
                            </SharePoint:DateTimeControl>
                        </td>
                    </tr>
                    <tr id="PostProgrammeRow2" runat="server" visible="false">
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Evaluation Report<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblEvaluationReport" Text="&nbsp;" runat="server"></asp:Label>
                            <SharePoint:DateTimeControl Visible="false" ID="dtcEvaluationReport" LocaleId="2057"
                                ToolTip="DD/MM/YYYY" runat="server" DateOnly="true" IsRequiredField="true">
                            </SharePoint:DateTimeControl>
                        </td>
                    </tr>
                </table>
                <table class="ms-formtable" style="margin-top: 0px; display: none;" cellspacing="0"
                    cellpadding="0" width="100%" border="0" id="ProgrammeCoordinationTable">
                    <tr>
                        <td class="td_Field" valign="top" nowrap width="195">
                            <h3 class="ms-standardheader">
                                Programme Coordination Checklist<span class="ms-formvalidation"> *</span>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <asp:Label ID="lblProgrammeCoordinationChecklist" Text="&nbsp;" runat="server"></asp:Label>
                            <asp:RadioButtonList Visible="false" ID="rblProgrammeCoordinationChecklist" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="Internal" onclick="javascript:ShowHideExternal();">Internal</asp:ListItem>
                                <asp:ListItem Value="External" onclick="javascript:ShowHideExternal();">External / Inplant / TTT</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            ITF Form 6A
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblITFForm6A" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcITFForm6A" LocaleId="2057" ToolTip="DD/MM/YYYY"
                                            runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (3 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="ExternalRow1" runat="server" style="display: none">
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Invitation Letter For Visa To Facilitator
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblInvitationLetterForVisaToFacilitator" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcInvitationLetterForVisaToFacilitator"
                                            LocaleId="2057" ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (1 month before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="ExternalRow2" runat="server" style="display: none">
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Protocol / Transport Arrangements
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblProtocolOrTransportArrangements" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcProtocolOrTransportArrangements"
                                            LocaleId="2057" ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (1 month before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="ExternalRow3" runat="server" style="display: none">
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Accomodation Arrangements For Facilitator
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblAccomodationArrangementsForFacilitator" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcAccomodationArrangementsForFacilitator"
                                            LocaleId="2057" ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (1 month before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Facilitator's Kit (where applicable)
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblFacilitatorKit" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcFacilitatorKit" LocaleId="2057"
                                            ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (3 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Delegate Availability To Line Managers
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblDelegateAvailabilityToLineManagers" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcDelegateAvailabilityToLineManagers"
                                            LocaleId="2057" ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (Minimum 3 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="ExternalRow4" runat="server" style="display: none">
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Retention Contract
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblRetentionContract" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcRetentionContract" LocaleId="2057"
                                            ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (Minimum 2 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Training Invitation
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblTrainingInvitation" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcTrainingInvitation" LocaleId="2057"
                                            ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (Minimum 2 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="ExternalRow5" runat="server" style="display: none">
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Travel Requisitions (where applicable)
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblTravelRequisitions" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcTravelRequisitions" LocaleId="2057"
                                            ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (Minimum 2 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Venue Preparation
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblVenuePreparation" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcVenuePreparation" LocaleId="2057"
                                            ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (Minimum 3 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Hall
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblHall" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcHall" LocaleId="2057" ToolTip="DD/MM/YYYY"
                                            runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (Minimum 3 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Projector
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblProjector" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcProjector" LocaleId="2057" ToolTip="DD/MM/YYYY"
                                            runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (Minimum 3 weeks before training) "confirm 24 hours before training"
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Hall Arrangement
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblHallArrangement" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcHallArrangement" LocaleId="2057"
                                            ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (Minimum 3 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Location
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblLocation" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcLocation" LocaleId="2057" ToolTip="DD/MM/YYYY"
                                            runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (Minimum 3 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Tea Break & Lunch
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblTeaBreakAndLunch" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcTeaBreakAndLunch" LocaleId="2057"
                                            ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (Minimum 3 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Seating Arrangement
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblSeatingArrangement" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcSeatingArrangement" LocaleId="2057"
                                            ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (Minimum 3 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Location Branding
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblLocationBranding" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcLocationBranding" LocaleId="2057"
                                            ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (24 hours before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Programme Materials
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblProgrammeMaterials" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcProgrammeMaterials" LocaleId="2057"
                                            ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (1 month before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Wall Charts
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblWallCharts" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcWallCharts" LocaleId="2057" ToolTip="DD/MM/YYYY"
                                            runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (1 month before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Vidoes / DVD
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblVidoesOrDVD" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcVidoesOrDVD" LocaleId="2057" ToolTip="DD/MM/YYYY"
                                            runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (1 month before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Participant Guide
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblParticipantGuide" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcParticipantGuide" LocaleId="2057"
                                            ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (1 month before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Presentation Slides
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPresentationSlides" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcPresentationSlides" LocaleId="2057"
                                            ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (1 month before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Learning Aids - Activity Cards / Case Studies
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblLearningAidsActivityCardsOrCaseStudies" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcLearningAidsActivityCardsOrCaseStudies"
                                            LocaleId="2057" ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (1 month before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Stationaries (Biros/pencils/blue tack/post-it)
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblStationaries" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcStationaries" LocaleId="2057"
                                            ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (2 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                External Speakers (where applicable)
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblExternalSpeakers" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcExternalSpeakers" LocaleId="2057"
                                            ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (48 hours before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                Pre-Course (where applicable)
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPreCourse" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcPreCourse" LocaleId="2057" ToolTip="DD/MM/YYYY"
                                            runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (2 weeks before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            KP1 Evaluation
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblKP1Evaluation" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcKP1Evaluation" LocaleId="2057"
                                            ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (1 month before training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Certificates
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblCertificates" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcCertificates" LocaleId="2057"
                                            ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (48 hours)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Attendance List
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblAttendanceList" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcAttendanceList" LocaleId="2057"
                                            ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (24 hours)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Photographs
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPhotographs" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcPhotographs" LocaleId="2057" ToolTip="DD/MM/YYYY"
                                            runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (48 hours)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_Field" valign="top" nowrap="" width="195">
                            <h3 class="ms-standardheader">
                                <nobr>
                            Post Assessment (KP3)
                          </nobr>
                            </h3>
                        </td>
                        <td class="td_Label" valign="top" width="400">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPostAssessment" Text="&nbsp;" runat="server"></asp:Label>
                                        <SharePoint:DateTimeControl Visible="false" ID="dtcPostAssessment" LocaleId="2057"
                                            ToolTip="DD/MM/YYYY" runat="server" DateOnly="true">
                                        </SharePoint:DateTimeControl>
                                    </td>
                                    <td align="right">
                                        (6 weeks after training)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tbody>
                        <tr>
                            <td class="ms-formline">
                                <img height="1" alt="" src="/_layouts/images/blank.gif" width="1" />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="ms-formtoolbar" id="tblBottomButtons" cellspacing="0" cellpadding="2"
                    width="100%" border="0">
                    <tr>
                        <td class="ms-toolbar" nowrap width="99%">
                            <img height="18" alt="" src="/_layouts/images/blank.gif" width="1" />
                        </td>
                        <td class="ms-toolbar" nowrap>
                            <asp:Button class="ms-ButtonHeightWidth120" ID="Button1" runat="server" Text="Show Main Form"
                                OnClientClick="javascript:return ShowMainTable();" />
                        </td>
                        <td class="ms-separator">
                            &nbsp;
                        </td>
                        <td class="ms-toolbar" nowrap>
                            <asp:Button class="ms-ButtonHeightWidth180" ID="Button2" runat="server" Text="Show Programme Coordination CheckList"
                                OnClientClick="javascript:return ShowProgrammeCoordinationTable();" />
                        </td>
                        <td class="ms-separator">
                            &nbsp;
                        </td>
                        <td class="ms-toolbar" nowrap>
                            <asp:Button class="ms-ButtonHeightWidth" ID="btnApprove" runat="server" Text="Approve"
                                OnClick="btnApprove_Click" />
                        </td>
                        <td class="ms-separator">
                            &nbsp;
                        </td>
                        <td class="ms-toolbar" nowrap>
                            <asp:Button class="ms-ButtonHeightWidth120" ID="btnRequestChange" runat="server"
                                Text="Request Change" OnClick="btnRequestChange_Click" ClientIDMode="Static" />
                        </td>
                        <td class="ms-separator">
                            &nbsp;
                        </td>
                        <td class="ms-toolbar" nowrap>
                            <asp:Button class="ms-ButtonHeightWidth" ID="btnReject" runat="server" Text="Reject"
                                OnClick="btnReject_Click" ClientIDMode="Static" />
                        </td>
                        <td class="ms-separator">
                            &nbsp;
                        </td>
                        <td class="ms-toolbar" nowrap>
                            <asp:Button class="ms-ButtonHeightWidth" ID="btnClose" runat="server" Text="Close"
                                OnClick="btnClose_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <script type="text/javascript" src="ClientStyles/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="ClientStyles/jquery-ui.min.js"></script>
    <script type="text/javascript">
        //Load Script:
		if (document.getElementById('ctl00_PlaceHolderMain__hlTrainingEvaluationReport').style["display"] == "block") {

		}
		else {
			document.getElementById('ctl00_PlaceHolderMain__btnShowHideFileUpload').style["display"] = "none";
		}
		//End of Load Script
		function ShowHideFileUpload() {
			if (document.getElementById('ctl00_PlaceHolderMain__hlTrainingEvaluationReport').style["display"] == "block") {
				document.getElementById('ctl00_PlaceHolderMain__hlTrainingEvaluationReport').style["display"] = "none";
				document.getElementById('ctl00_PlaceHolderMain__fuTrainingEvaluationReport').style["display"] = "block";
				document.getElementById('ctl00_PlaceHolderMain__btnShowHideFileUpload').style["display"] = "none";
				document.getElementById('ctl00_PlaceHolderMain__fuTrainingEvaluationReportValidator').setAttribute('enabled', '1');
				document.getElementById('ctl00_PlaceHolderMain__hfTrainingEvaluationReport').value = "Update";
			}
			else {
				document.getElementById('ctl00_PlaceHolderMain__hlTrainingEvaluationReport').style["display"] = "block";
				document.getElementById('ctl00_PlaceHolderMain__fuTrainingEvaluationReport').style["display"] = "none";
				document.getElementById('ctl00_PlaceHolderMain__btnShowHideFileUpload').style["display"] = "block";
				document.getElementById('ctl00_PlaceHolderMain__fuTrainingEvaluationReportValidator').setAttribute('enabled', '0');
			}
		}

		function ShowMainTable() {
			// tblBottomButtons.style["display"] = "none";
			// tblShowProgrammeCoordinationTable.style["display"] = "block";
			ProgrammeCoordinationTable.style["display"] = "none";
			MainFormTable.style["display"] = "block";
			return false;
		}

		function ShowProgrammeCoordinationTable() {
			//tblBottomButtons.style["display"] = "block";
			//tblShowProgrammeCoordinationTable.style["display"] = "none";
			ProgrammeCoordinationTable.style["display"] = "block";
			MainFormTable.style["display"] = "none";
			//scroll(0, 0);
			return false;
		}

		function ShowHideOthersEquipmentsRequired() {
			if (document.getElementById('ctl00_PlaceHolderMain_cblEquipmentsRequired_4').getAttribute('CHECKED')) {
				ctl00_PlaceHolderMain_OthersEquipmentsRequiredRow.style["display"] = "block";
				document.getElementById('ctl00_PlaceHolderMain_txtOthersEquipmentsRequiredValidator').setAttribute('enabled', '1');
			}
			else {
				ctl00_PlaceHolderMain_OthersEquipmentsRequiredRow.style["display"] = "none";
				document.getElementById('ctl00_PlaceHolderMain_txtOthersEquipmentsRequiredValidator').setAttribute('enabled', '0');
			}
		}

		function ShowHideIsMTNProgrammeOthers() {
			if (document.getElementById('ctl00_PlaceHolderMain_rblIsMTNAcademyProgramme_2').getAttribute('CHECKED')) {
				ctl00_PlaceHolderMain_IsMTNAcademyProgrammeOthersRow.style["display"] = "block";
				document.getElementById('ctl00_PlaceHolderMain_txtIsMTNProgrammeOthersValidator').setAttribute('enabled', '1');
			}
			else {
				ctl00_PlaceHolderMain_IsMTNAcademyProgrammeOthersRow.style["display"] = "none";
				document.getElementById('ctl00_PlaceHolderMain_txtIsMTNProgrammeOthersValidator').setAttribute('enabled', '0');
			}
		}
		function ShowHideExternal() {

			if (document.getElementById('ctl00_PlaceHolderMain_rblProgrammeCoordinationChecklist_1').getAttribute('CHECKED')) {
				ctl00_PlaceHolderMain_ExternalRow1.style["display"] = "block";
				ctl00_PlaceHolderMain_ExternalRow2.style["display"] = "block";
				ctl00_PlaceHolderMain_ExternalRow3.style["display"] = "block";
				ctl00_PlaceHolderMain_ExternalRow4.style["display"] = "block";
				ctl00_PlaceHolderMain_ExternalRow5.style["display"] = "block";
			}
			else {
				ctl00_PlaceHolderMain_ExternalRow1.style["display"] = "none";
				ctl00_PlaceHolderMain_ExternalRow2.style["display"] = "none";
				ctl00_PlaceHolderMain_ExternalRow3.style["display"] = "none";
				ctl00_PlaceHolderMain_ExternalRow4.style["display"] = "none";
				ctl00_PlaceHolderMain_ExternalRow5.style["display"] = "none";
			}
        }      
	</script>

    <script type="text/javascript">
         $(document).ready(function () {
            $('#btnRequestChange').click(function () {
                if ($('#txtComment').val().length < 1) {
                    alert("Comment is Required");
                    return false;
                }
                return true;
            });
        });

        $(document).ready(function () {
            $('#btnReject').click(function () {
                if ($('#txtComment').val().length < 1) {
                    alert("Comment is Required");
                    return false;
                }
                return true;
            });
        });
    </script>

</asp:Content>

