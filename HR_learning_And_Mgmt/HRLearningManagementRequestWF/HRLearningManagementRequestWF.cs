﻿using System;
using System.Workflow.Activities;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Workflow;
using System.Collections.Generic;
using Microsoft.Office.Workflow.Utility;
using TaviaTech.SPExtensions;
using System.IO;

namespace HR_learning_And_Mgmt.HRLearningManagementRequestWF
{
    public sealed partial class HRLearningManagementRequestWF : StateMachineWorkflowActivity
    {
        public HRLearningManagementRequestWF()
        {
            InitializeComponent();
        }

        #region WorkflowVariables

        public SPWorkflowActivationProperties workflowProperties = new Microsoft.SharePoint.Workflow.SPWorkflowActivationProperties();

        //General Manager Task
        public Int32 ctGeneralManager_ListItemId = default(System.Int32);
        public System.Collections.Specialized.HybridDictionary ctGeneralManager_SpecialPermissions = new System.Collections.Specialized.HybridDictionary();
        public Guid ctGeneralManager_TaskId = default(System.Guid);
        public SPWorkflowTaskProperties ctGeneralManager_TaskProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();
        public String lthGeneralManagerTaskCreated_HistoryDescription = default(System.String);
        public String lthGeneralManagerTaskCreated_HistoryOutcome = default(System.String);
        public SPWorkflowTaskProperties otcGeneralManager_AfterProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();
        public String lthAfterGeneralManagerAction_HistoryDescription = default(System.String);
        public String lthAfterGeneralManagerAction_HistoryOutcome = default(System.String);
        public Int32 lthAfterGeneralManagerAction_UserId = default(System.Int32);

        //Senior Manager Task
        public Int32 ctSeniorManager_ListItemId = default(System.Int32);
        public System.Collections.Specialized.HybridDictionary ctSeniorManager_SpecialPermissions = new System.Collections.Specialized.HybridDictionary();
        public Guid ctSeniorManager_TaskId = default(System.Guid);
        public SPWorkflowTaskProperties ctSeniorManager_TaskProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();
        public String lthSeniorManagerTaskCreated_HistoryDescription = default(System.String);
        public String lthSeniorManagerTaskCreated_HistoryOutcome = default(System.String);
        public SPWorkflowTaskProperties otcSeniorManager_AfterProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();
        public String lthAfterSeniorManagerAction_HistoryDescription = default(System.String);
        public String lthAfterSeniorManagerAction_HistoryOutcome = default(System.String);
        public Int32 lthAfterSeniorManagerAction_UserId = default(System.Int32);

        //Line Manager Task
        public Int32 ctLineManager_ListItemId = default(System.Int32);
        public System.Collections.Specialized.HybridDictionary ctLineManager_SpecialPermissions = new System.Collections.Specialized.HybridDictionary();
        public Guid ctLineManager_TaskId = default(System.Guid);
        public SPWorkflowTaskProperties ctLineManager_TaskProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();
        public String lthLineManagerTaskCreated_HistoryDescription = default(System.String);
        public String lthLineManagerTaskCreated_HistoryOutcome = default(System.String);
        public SPWorkflowTaskProperties otcLineManager_AfterProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();
        public String lthAfterLineManagerAction_HistoryDescription = default(System.String);
        public String lthAfterLineManagerAction_HistoryOutcome = default(System.String);
        public Int32 lthAfterLineManagerAction_UserId = default(System.Int32);

        //Learning Management Team
        public Int32 ctLearningManagementTeam_ListItemId = default(System.Int32);
        public System.Collections.Specialized.HybridDictionary ctLearningManagementTeam_SpecialPermissions = new System.Collections.Specialized.HybridDictionary();
        public Guid ctLearningManagementTeam_TaskId = default(System.Guid);
        public SPWorkflowTaskProperties ctLearningManagementTeam_TaskProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();
        public String lthLearningManagementTeamTaskCreated_HistoryDescription = default(System.String);
        public String lthLearningManagementTeamTaskCreated_HistoryOutcome = default(System.String);
        public SPWorkflowTaskProperties otcLearningManagementTeam_AfterProperties = new Microsoft.SharePoint.Workflow.SPWorkflowTaskProperties();
        public String lthAfterLearningManagementTeamAction_HistoryDescription = default(System.String);
        public String lthAfterLearningManagementTeamAction_HistoryOutcome = default(System.String);
        public Int32 lthAfterLearningManagementTeamAction_UserId = default(System.Int32);

        #endregion WorkflowVariables

        #region GlobalVariables
        string CurrentWebURL;
        //string VacancyTitle = "";
        //string Department = "";
        List<SPUserRole> _rols;
        int RequestItemID = -1;
        Guid RequestsListId;
        string RequestsListTitle;
        string _LineManagerAction = "";
        string _SeniorManagerAction = "";
        string _GeneralManagerAction = "";
        string _LearningManagementTeamAction = "";
        Contact _LineManager;
        Contact _SeniorManager;
        Contact _GeneralManager;
        Contact _Requester;
        Contact _LearningManagementTeam;
        string _Comments = "";
        HRLearningManagementRequestWFRecjector _Rejector;//to know who return the request to Requests
        //string _ProgrammeDate = "";
        //string _ProgrammeName = "";
        string _WorkflowTaskASPX = "/_layouts/HR_learning_And_Mgmt/HRLearningManagementRequestWFTask.aspx?";
        string LearningManagementTeamEmailAddress = "";
        bool isFirstTime = true;
        bool isRequestChange = false;
        #endregion GlobalVariables

        private void ImpersonatedonWorkfrloActivated()
        {
            SPSite _ImpersonatedSite = null;
            SPWeb _ImpersonatedWeb = null;
            SPList _RequestsList = null;
            SPListItem _RequestItem = null;
            try
            {
                _ImpersonatedSite = new SPSite(CurrentWebURL);
                _ImpersonatedWeb = _ImpersonatedSite.OpenWeb();
                _RequestsList = _ImpersonatedWeb.Lists[RequestsListId];
                _RequestItem = _RequestsList.GetItemById(RequestItemID);

                _ImpersonatedWeb.AllowUnsafeUpdates = true;

                //_RequestItem["Title"] = "IS_ESM_Service_Request_" + _RequestItem.ID.ToString();
                //_RequestItem.Update();
                //_ProgrammeName = workflowProperties.List.GetItemByIdSelectedFields(workflowProperties.ItemId, "Programme name").
                //ReadItemFieldString("Programme name");
                //_RequestItem["Programme name"].ToString();//Programme Name
                //_ProgrammeDate = workflowProperties.List.GetItemByIdSelectedFields(workflowProperties.ItemId, "Programme date").
                //    ReadItemDateTime("Programme date").ToString("dd MM yyyy");
                //((DateTime)_RequestItem["Programme date"]).ToString("dd MMM yyyy");
                _ImpersonatedWeb.AllowUnsafeUpdates = false;

                SPUser lineManagerValue = SPHandler.GetSPUserFromFieldValue(_RequestItem["Line manager"].ToString(), _RequestItem.Web);
                _LineManager = Contact.FromName(lineManagerValue.LoginName, _RequestItem.Web);

                SPUser seniorManagerValue = SPHandler.GetSPUserFromFieldValue(_RequestItem["Senior manager"].ToString(), _RequestItem.Web);
                _SeniorManager = Contact.FromName(seniorManagerValue.LoginName, _RequestItem.Web);

                SPUser _GeneralManagerValue = SPHandler.GetSPUserFromFieldValue(_RequestItem["General manager"].ToString(), _RequestItem.Web);
                _GeneralManager = Contact.FromName(_GeneralManagerValue.LoginName, _RequestItem.Web);


                //We actually use the LearningManagementTeam when we request change, The requester here is kept just for history
                //to know who within the team initiated the workflow
                _Requester = Contact.FromName(_RequestItem["Author"].ToString().Split('#')[1], _ImpersonatedWeb);
                _LearningManagementTeam = Contact.FromPrincipalID(_ImpersonatedWeb.SiteGroups["Learning Management Team"].ID, _ImpersonatedWeb);

                _rols = new List<SPUserRole>();
                _rols.Add(new SPUserRole(_LineManager, SPRoleType.Contributor, false));
                _rols.Add(new SPUserRole(_SeniorManager, SPRoleType.Contributor, false));
                _rols.Add(new SPUserRole(_GeneralManager, SPRoleType.Contributor, false));
                _rols.Add(new SPUserRole(_LearningManagementTeam, SPRoleType.Contributor, true));
                WorkflowHandler.SetListItemSecurity(workflowProperties, _rols);

            }
            catch (Exception ex)
            {
                ErrorHandler.LogToWindows(ex, "HRLearningManagementRequestWF, ImpersonatedonWorkfrloActivated");
            }
            finally
            {
                if (_ImpersonatedWeb != null)
                    _ImpersonatedWeb.Dispose();
                if (_ImpersonatedSite != null)
                    _ImpersonatedSite.Dispose();
            }
        }

        private void onWorkflowActivated1_Invoked(object sender, ExternalDataEventArgs e)
        {
            try
            {
                LearningManagementTeamEmailAddress = System.Web.Configuration.WebConfigurationManager.AppSettings["HRLearningManagementRequest_LearningManagementTeamEmail"].ToString();
                CurrentWebURL = workflowProperties.Web.Url;
                RequestItemID = workflowProperties.ItemId;
                RequestsListId = workflowProperties.ListId;
                RequestsListTitle = workflowProperties.List.Title;
                SPSecurity.RunWithElevatedPrivileges(ImpersonatedonWorkfrloActivated);
            }
            catch
            {
            }
        }


        #region Line Manager

        private void ctLineManager_MethodInvoking(object sender, EventArgs e)
        {
            ctLineManager_TaskId = Guid.NewGuid();
            ctLineManager_TaskProperties.Title = "Line Manager Task - " + DateTime.Now.ToString();
            //SPFieldUserValue lM = new SPFieldUserValue(workflowProperties.Item.Web, workflowProperties.Item["Line manager"].ToString());
            //ctLineManager_TaskProperties.AssignedTo = lM.LoginName;// _LineManager.LoginName;
            ctLineManager_TaskProperties.AssignedTo = _LineManager.LoginName;// _LineManager.LoginName;
            ctLineManager_TaskProperties.ExtendedProperties["DisplayForm"] = "LineManager";
            ctLineManager_TaskProperties.ExtendedProperties["ItemID"] = RequestItemID;
            ctLineManager_TaskProperties.ExtendedProperties["ListTitle"] = RequestsListTitle;
            ctLineManager_TaskProperties.ExtendedProperties["Comments"] = _Comments;
            ctLineManager_TaskProperties.ExtendedProperties["isFirstTime"] = isFirstTime;

            lthLineManagerTaskCreated_HistoryDescription = "Line Manager Task Created";
            if (isFirstTime)
                lthLineManagerTaskCreated_HistoryOutcome = "The system awaits for Line Manager Approval";
            else
                lthLineManagerTaskCreated_HistoryOutcome = "The system awaits for Line Manager evaluation report reviewal";

            WorkflowHandler.ChangeTaskItemSecurity(ctLineManager_SpecialPermissions, ctLineManager_TaskProperties);
        }

        private void isLineManagerFinalApproval(object sender, ConditionalEventArgs e)
        {
            if (isFirstTime)
                e.Result = true;
            else
                e.Result = false;
        }

        private void caSendLineManagerTaskCreatedMail_ExecuteCode(object sender, EventArgs e)
        {
            //SendEmail
            List<string> To = new List<string>();
            List<string> Cc = new List<string>();

            string _ProgrammeName = workflowProperties.List.GetItemByIdSelectedFields(workflowProperties.ItemId, "Programme name").
               ReadItemFieldString("Programme name");
            string _ProgrammeDate = workflowProperties.List.GetItemByIdSelectedFields(workflowProperties.ItemId, "Programme date").
                    ReadItemDateTime("Programme date").ToString("dd MM yyyy");

            if (!String.IsNullOrEmpty(_LineManager.EmailAddress))
                To.Add(_LineManager.EmailAddress);

            string Body = "Dear " + _LineManager.DisplayName + ",<br><br>";
            //Body += "A new Request from " + _Requester.DisplayName + " is awaiting your kind approval. ";
            if (isFirstTime)
                Body += "An HR Learning Management request has been forwarded for your approval.<br>";
            else
                Body += "An HR Learning Management Evaluation Report has been forwarded for your review & approval.<br>";
            Body += "Requested by Learning Management Team.";
            Body += "<br>Programme Name: " + _ProgrammeName;
            Body += "<br>Programme Date: " + _ProgrammeDate;
            Body += "<br><hr>For more details please " + WorkflowHandler.getTaskUrl(ctLineManager.ListItemId.ToString(), workflowProperties, _WorkflowTaskASPX) + " To Approve/Disapprove the request.";
            Body += "<br><br>Kind Regards,<br>";
            Body += "HR Learning Management";
            Body += "<br><br>NOTE: This email message is auto generated, Please do not reply to the email address.";
            EMailHandler.SendEmail(To, "workflows@mtn.com", Cc, "HR Learning Management Request"/* by " + _Requester.DisplayName*/, Body);

            string requestChangeLink = WorkflowHandler.getTaskUrl(ctLineManager.ListItemId.ToString(), workflowProperties, _WorkflowTaskASPX);
            SPFieldUrlValue statusChange = new SPFieldUrlValue();
            statusChange.Description = "Pending Line Manager Approval";
            statusChange.Url = requestChangeLink;
            workflowProperties.Item["Status Stage"] = statusChange.Description;
            workflowProperties.Item.Update();
        }

        private void otcLineManager_Invoked(object sender, ExternalDataEventArgs e)
        {
            string _RejectComment = workflowProperties.List.GetItemByIdSelectedFields(workflowProperties.ItemId, "ChangeRequesterComment").
                ReadItemFieldString("ChangeRequesterComment");

            string _ProgrammeName = workflowProperties.List.GetItemByIdSelectedFields(workflowProperties.ItemId, "Programme name").
               ReadItemFieldString("Programme name");
            string _ProgrammeDate = workflowProperties.List.GetItemByIdSelectedFields(workflowProperties.ItemId, "Programme date").
                    ReadItemDateTime("Programme date").ToString("dd MM yyyy");

            _LineManagerAction = otcLineManager_AfterProperties.ExtendedProperties["Action"].ToString();
            _Comments = otcLineManager_AfterProperties.ExtendedProperties["Comments"].ToString();
            if (otcLineManager_AfterProperties.ExtendedProperties["CurrentUserId"] != null)
                lthAfterLineManagerAction_UserId = int.Parse(otcLineManager_AfterProperties.ExtendedProperties["CurrentUserId"].ToString());

            //Approve or Reject Mail
            //SendEmail
            List<string> To = new List<string>();
            List<string> Cc = new List<string>();

            if (!String.IsNullOrEmpty(_Requester.EmailAddress))
                To.Add(_Requester.EmailAddress);

            if (!String.IsNullOrEmpty(LearningManagementTeamEmailAddress))
                To.Add(LearningManagementTeamEmailAddress);

            //string Body = "Dear " + _Requester.DisplayName + ",<br><br>";
            string Body = "Dear Learning Management Team Member,<br><br>";
            if (!String.IsNullOrEmpty(_LineManager.EmailAddress))
                Cc.Add(_LineManager.EmailAddress);

            if (_LineManagerAction == "Approved")
            {
                if (isFirstTime)
                {
                    lthAfterLineManagerAction_HistoryDescription = "Line Manager Approved Requisition";
                    lthAfterLineManagerAction_HistoryOutcome = "Line Manager Approved The requisition.";
                    Body += "Your request has been approved by your Line Manager (" + _LineManager.DisplayName + ") and has been forwarded to your Senior Manager for approval.";
                }
                else
                {
                    lthAfterLineManagerAction_HistoryDescription = "Line Manager Reviewed Evaluation Report";
                    lthAfterLineManagerAction_HistoryOutcome = "Line Manager Approved The Evaluation Report.";
                    Body += "Your request evaluation report has been reviewed & approved by your Line Manager (" + _LineManager.DisplayName + ") and has been forwarded to your Senior Manager for approval.";
                }
            }
            else if (_LineManagerAction == "Rejected")//Recjected
            {
                lthAfterLineManagerAction_HistoryDescription = "Line Manager Rejected Requisition";
                lthAfterLineManagerAction_HistoryOutcome = "Line Manager Rejected The Requisition.";
                Body += "Your request has been rejected by your Line Manager (" + _LineManager.DisplayName + ").<br>Comment: " + _RejectComment + "<br>";
            }
            else //Request Change
            {
                lthAfterLineManagerAction_HistoryDescription = "Line Manager Request Change";
                lthAfterLineManagerAction_HistoryOutcome = "Line Manager Request Change From the Requester.";
            }
            Body += "<br>Programme Name: " + _ProgrammeName;
            Body += "<br>Programme Date: " + _ProgrammeDate;
            Body += "<br><br>Kind Regards,<br>";
            Body += "HR Learning Management";
            Body += "<br><br>NOTE: This email message is auto generated, Please do not reply to the email address.";
            if (_LineManagerAction == "Approved")
            {
                if (isFirstTime)
                    EMailHandler.SendEmail(To, "workflows@mtn.com", Cc, "HR Learning Management Request Approved by Line Manager", Body);
                else
                    EMailHandler.SendEmail(To, "workflows@mtn.com", Cc, "Training Evaluation Report Approved by Line Manager", Body);
            }
            else if (_LineManagerAction == "Rejected")//Recjected
                EMailHandler.SendEmail(To, "workflows@mtn.com", Cc, "HR Learning Management Request Rejected by Line Manager", Body);
        }

        private void LineManagerApprovedAction(object sender, ConditionalEventArgs e)
        {
            if (_LineManagerAction == "Approved")
            {
                e.Result = true;//Assign Task to the Senior Manager
            }
            else //i.e Did Not Approve
            {
                e.Result = false;
            }
        }

        private void LineManagerRejectedAction(object sender, ConditionalEventArgs e)
        {
            if (_LineManagerAction == "Rejected")
            {
                e.Result = true;//Workflow Ends - Cycle Completed & Requisition Rejected
            }
            else
            {
                e.Result = false;//Assign Task to the Requester
                isRequestChange = true;
                _Rejector = HRLearningManagementRequestWFRecjector.LineManager;
            }
        }

        #endregion Line Manager

        #region Senior Manager

        private void ctSeniorManager_MethodInvoking(object sender, EventArgs e)
        {
            ctSeniorManager_TaskId = Guid.NewGuid();
            ctSeniorManager_TaskProperties.Title = "Senior Manager Task - " + DateTime.Now.ToString();
            ctSeniorManager_TaskProperties.AssignedTo = _SeniorManager.LoginName;
            ctSeniorManager_TaskProperties.ExtendedProperties["DisplayForm"] = "SeniorManager";
            ctSeniorManager_TaskProperties.ExtendedProperties["ItemID"] = RequestItemID;
            ctSeniorManager_TaskProperties.ExtendedProperties["ListTitle"] = RequestsListTitle;
            ctSeniorManager_TaskProperties.ExtendedProperties["Comments"] = _Comments;
            ctSeniorManager_TaskProperties.ExtendedProperties["isFirstTime"] = isFirstTime;

            lthSeniorManagerTaskCreated_HistoryDescription = "Senior Manager Task Created";
            if (isFirstTime)
                lthSeniorManagerTaskCreated_HistoryOutcome = "The system awaits for Senior Manager Approval";
            else
                lthSeniorManagerTaskCreated_HistoryOutcome = "The system awaits for Senior Manager evaluation report reviewal";

            WorkflowHandler.ChangeTaskItemSecurity(ctSeniorManager_SpecialPermissions, ctSeniorManager_TaskProperties);
        }

        private void isSeniorManagerFinalApproval(object sender, ConditionalEventArgs e)
        {
            if (isFirstTime)
                e.Result = true;
            else
                e.Result = false;
        }

        private void caSendSeniorManagerTaskCreatedMail_ExecuteCode(object sender, EventArgs e)
        {
            //SendEmail
            List<string> To = new List<string>();
            List<string> Cc = new List<string>();

            string _ProgrammeName = workflowProperties.List.GetItemByIdSelectedFields(workflowProperties.ItemId, "Programme name").
               ReadItemFieldString("Programme name");
            string _ProgrammeDate = workflowProperties.List.GetItemByIdSelectedFields(workflowProperties.ItemId, "Programme date").
                    ReadItemDateTime("Programme date").ToString("dd MM yyyy");

            if (!String.IsNullOrEmpty(_SeniorManager.EmailAddress))
                To.Add(_SeniorManager.EmailAddress);

            string Body = "Dear " + _SeniorManager.DisplayName + ",<br><br>";
            //Body += "A new Request from " + _Requester.DisplayName + " is awaiting your kind approval. ";
            if (isFirstTime)
                Body += "An HR Learning Management request has been forwarded for your approval.<br>";
            else
                Body += "An HR Learning Management Evaluation Report has been forwarded for your review & approval.<br>";
            Body += "Requested by Learning Management Team.";
            Body += "<br>Programme Name: " + _ProgrammeName;
            Body += "<br>Programme Date: " + _ProgrammeDate;
            Body += "<br><hr>For more details please " + WorkflowHandler.getTaskUrl(ctSeniorManager.ListItemId.ToString(), workflowProperties, _WorkflowTaskASPX) + " To Approve/Disapprove the request.";
            Body += "<br><br>Kind Regards,<br>";
            Body += "HR Learning Management";
            Body += "<br><br>NOTE: This email message is auto generated, Please do not reply to the email address.";
            EMailHandler.SendEmail(To, "workflows@mtn.com", Cc, "HR Learning Management Request"/* by " + _Requester.DisplayName*/, Body);

            string requestChangeLink = WorkflowHandler.getTaskUrl(ctSeniorManager.ListItemId.ToString(), workflowProperties, _WorkflowTaskASPX);
            SPFieldUrlValue statusChange = new SPFieldUrlValue();
            statusChange.Description = "Pending Senior Manager Approval";
            statusChange.Url = requestChangeLink;
            workflowProperties.Item["Status Stage"] = statusChange.Description;
            workflowProperties.Item.Update();
        }

        private void otcSeniorManager_Invoked(object sender, ExternalDataEventArgs e)
        {
            string _RejectComment = workflowProperties.List.GetItemByIdSelectedFields(workflowProperties.ItemId, "ChangeRequesterComment").
                ReadItemFieldString("ChangeRequesterComment");

            string _ProgrammeName = workflowProperties.List.GetItemByIdSelectedFields(workflowProperties.ItemId, "Programme name").
               ReadItemFieldString("Programme name");
            string _ProgrammeDate = workflowProperties.List.GetItemByIdSelectedFields(workflowProperties.ItemId, "Programme date").
                    ReadItemDateTime("Programme date").ToString("dd MM yyyy");

            _SeniorManagerAction = otcSeniorManager_AfterProperties.ExtendedProperties["Action"].ToString();
            _Comments = otcSeniorManager_AfterProperties.ExtendedProperties["Comments"].ToString();
            if (otcSeniorManager_AfterProperties.ExtendedProperties["CurrentUserId"] != null)
                lthAfterSeniorManagerAction_UserId = int.Parse(otcSeniorManager_AfterProperties.ExtendedProperties["CurrentUserId"].ToString());

            //Approve or Reject Mail
            //SendEmail
            List<string> To = new List<string>();
            List<string> Cc = new List<string>();

            if (!String.IsNullOrEmpty(_Requester.EmailAddress))
                To.Add(_Requester.EmailAddress);

            if (!String.IsNullOrEmpty(LearningManagementTeamEmailAddress))
                To.Add(LearningManagementTeamEmailAddress);

            //string Body = "Dear " + _Requester.DisplayName + ",<br><br>";
            string Body = "Dear Learning Management Team Member,<br><br>";
            if (!String.IsNullOrEmpty(_SeniorManager.EmailAddress))
                Cc.Add(_SeniorManager.EmailAddress);

            if (_SeniorManagerAction == "Approved")
            {
                if (isFirstTime)
                {
                    lthAfterSeniorManagerAction_HistoryDescription = "Senior Manager Approved Requisition";
                    lthAfterSeniorManagerAction_HistoryOutcome = "Senior Manager Approved The requisition.";
                    Body += "Your request has been approved by your Senior Manager (" + _SeniorManager.DisplayName + ") and has been forwarded to your General Manager for approval.";
                }
                else
                {
                    lthAfterSeniorManagerAction_HistoryDescription = "Senior Manager Approved Evaluation Report";
                    lthAfterSeniorManagerAction_HistoryOutcome = "Senior Manager Approved The Evaluation Report.";
                    Body += "Your request evaluation report has been reviewed & approved by your Senior Manager (" + _SeniorManager.DisplayName + ") and has been forwarded to your General Manager for approval.";
                }
            }
            else if (_SeniorManagerAction == "Rejected")//Recjected
            {
                lthAfterSeniorManagerAction_HistoryDescription = "Senior Manager Rejected Requisition";
                lthAfterSeniorManagerAction_HistoryOutcome = "Senior Manager Rejected The Requisition.";
                Body += "Your request has been rejected by your Senior Manager (" + _SeniorManager.DisplayName + ").<br>Comment: " + _RejectComment + "<br>";
            }
            else //Request Change
            {
                lthAfterSeniorManagerAction_HistoryDescription = "Senior Manager Request Change";
                lthAfterSeniorManagerAction_HistoryOutcome = "Senior Manager Request Change From the Requester.";
            }
            Body += "<br>Programme Name: " + _ProgrammeName;
            Body += "<br>Programme Date: " + _ProgrammeDate;
            Body += "<br><br>Kind Regards,<br>";
            Body += "HR Learning Management";
            Body += "<br><br>NOTE: This email message is auto generated, Please do not reply to the email address.";
            if (_SeniorManagerAction == "Approved")
            {
                if (isFirstTime)
                    EMailHandler.SendEmail(To, "workflows@mtn.com", Cc, "HR Learning Management Request Approved by Senior Manager", Body);
                else
                    EMailHandler.SendEmail(To, "workflows@mtn.com", Cc, "Training Evaluation Report Approved by Senior Manager", Body);
            }
            else if (_SeniorManagerAction == "Rejected")//Recjected
                EMailHandler.SendEmail(To, "workflows@mtn.com", Cc, "HR Learning Management Request Rejected by Senior Manager", Body);
        }

        private void SeniorManagerApprovedAction(object sender, ConditionalEventArgs e)
        {
            if (_SeniorManagerAction == "Approved")
            {
                e.Result = true;//Assign Task to the Senior Manager
            }
            else //i.e Did Not Approve
            {
                e.Result = false;
            }
        }

        private void SeniorManagerRejectedAction(object sender, ConditionalEventArgs e)
        {
            if (_SeniorManagerAction == "Rejected")
            {
                e.Result = true;//Workflow Ends - Cycle Completed & Requisition Rejected
            }
            else
            {
                e.Result = false;//Assign Task to the Requester
                isRequestChange = true;
                _Rejector = HRLearningManagementRequestWFRecjector.SeniorManager;
            }
        }

        #endregion Senior Manager

        #region General Manager

        private void ctGeneralManager_MethodInvoking(object sender, EventArgs e)
        {
            ctGeneralManager_TaskId = Guid.NewGuid();
            ctGeneralManager_TaskProperties.Title = "General Manager Task - " + DateTime.Now.ToString();
            ctGeneralManager_TaskProperties.AssignedTo = _GeneralManager.LoginName;
            ctGeneralManager_TaskProperties.ExtendedProperties["DisplayForm"] = "GeneralManager";
            ctGeneralManager_TaskProperties.ExtendedProperties["ItemID"] = RequestItemID;
            ctGeneralManager_TaskProperties.ExtendedProperties["ListTitle"] = RequestsListTitle;
            ctGeneralManager_TaskProperties.ExtendedProperties["Comments"] = _Comments;
            ctGeneralManager_TaskProperties.ExtendedProperties["isFirstTime"] = isFirstTime;

            lthGeneralManagerTaskCreated_HistoryDescription = "General Manager Task Created";
            if (isFirstTime)
                lthGeneralManagerTaskCreated_HistoryOutcome = "The system awaits for General Manager Approval";
            else
                lthGeneralManagerTaskCreated_HistoryOutcome = "The system awaits for General Manager evaluation report reviewal";

            WorkflowHandler.ChangeTaskItemSecurity(ctGeneralManager_SpecialPermissions, ctGeneralManager_TaskProperties);
        }

        private void isGeneralManagerFinalApproval(object sender, ConditionalEventArgs e)
        {
            if (isFirstTime)
                e.Result = true;
            else
                e.Result = false;
        }

        private void caSendGeneralManagerTaskCreatedMail_ExecuteCode(object sender, EventArgs e)
        {
            //SendEmail
            List<string> To = new List<string>();
            List<string> Cc = new List<string>();

            string _ProgrammeName = workflowProperties.List.GetItemByIdSelectedFields(workflowProperties.ItemId, "Programme name").
               ReadItemFieldString("Programme name");
            string _ProgrammeDate = workflowProperties.List.GetItemByIdSelectedFields(workflowProperties.ItemId, "Programme date").
                    ReadItemDateTime("Programme date").ToString("dd MM yyyy");

            if (!String.IsNullOrEmpty(_GeneralManager.EmailAddress))
                To.Add(_GeneralManager.EmailAddress);

            string Body = "Dear " + _GeneralManager.DisplayName + ",<br><br>";
            //Body += "A new Request from " + _Requester.DisplayName + " is awaiting your kind approval. ";
            if (isFirstTime)
                Body += "An HR Learning Management request has been forwarded for your approval.<br>";
            else
                Body += "An HR Learning Management Evaluation Report has been forwarded for your review & approval.<br>";
            Body += "Requested by Learning Management Team.";
            Body += "<br>Programme Name: " + _ProgrammeName;
            Body += "<br>Programme Date: " + _ProgrammeDate;
            Body += "<br><hr>For more details please " + WorkflowHandler.getTaskUrl(ctGeneralManager.ListItemId.ToString(), workflowProperties, _WorkflowTaskASPX) + " To Approve/Disapprove the request.";
            Body += "<br><br>Kind Regards,<br>";
            Body += "HR Learning Management";
            Body += "<br><br>NOTE: This email message is auto generated, Please do not reply to the email address.";
            EMailHandler.SendEmail(To, "workflows@mtn.com", Cc, "HR Learning Management Request"/* by " + _Requester.DisplayName*/, Body);

            string requestChangeLink = WorkflowHandler.getTaskUrl(ctGeneralManager.ListItemId.ToString(), workflowProperties, _WorkflowTaskASPX);
            SPFieldUrlValue statusChange = new SPFieldUrlValue();
            statusChange.Description = "Pending General Manager Approval";
            statusChange.Url = requestChangeLink;
            workflowProperties.Item["Status Stage"] = statusChange.Description;
            workflowProperties.Item.Update();
        }

        private void otcGeneralManager_Invoked(object sender, ExternalDataEventArgs e)
        {
            string _RejectComment = workflowProperties.List.GetItemByIdSelectedFields(workflowProperties.ItemId, "ChangeRequesterComment").
                ReadItemFieldString("ChangeRequesterComment");

            string _ProgrammeName = workflowProperties.List.GetItemByIdSelectedFields(workflowProperties.ItemId, "Programme name").
               ReadItemFieldString("Programme name");
            string _ProgrammeDate = workflowProperties.List.GetItemByIdSelectedFields(workflowProperties.ItemId, "Programme date").
                    ReadItemDateTime("Programme date").ToString("dd MM yyyy");

            _GeneralManagerAction = otcGeneralManager_AfterProperties.ExtendedProperties["Action"].ToString();
            _Comments = otcGeneralManager_AfterProperties.ExtendedProperties["Comments"].ToString();
            if (otcGeneralManager_AfterProperties.ExtendedProperties["CurrentUserId"] != null)
                lthAfterGeneralManagerAction_UserId = int.Parse(otcGeneralManager_AfterProperties.ExtendedProperties["CurrentUserId"].ToString());

            //Approve or Reject Mail
            //SendEmail
            List<string> To = new List<string>();
            List<string> Cc = new List<string>();

            if (!String.IsNullOrEmpty(_Requester.EmailAddress))
                To.Add(_Requester.EmailAddress);

            if (!String.IsNullOrEmpty(LearningManagementTeamEmailAddress))
                To.Add(LearningManagementTeamEmailAddress);

            //string Body = "Dear " + _Requester.DisplayName + ",<br><br>";
            string Body = "Dear Learning Management Team Member,<br><br>";
            if (!String.IsNullOrEmpty(_GeneralManager.EmailAddress))
                Cc.Add(_GeneralManager.EmailAddress);

            if (_GeneralManagerAction == "Approved")
            {
                if (isFirstTime)
                {
                    lthAfterGeneralManagerAction_HistoryDescription = "General Manager Approved Requisition";
                    lthAfterGeneralManagerAction_HistoryOutcome = "General Manager Approved The requisition.";
                    Body += "Your request has been approved by your General Manager (" + _GeneralManager.DisplayName + ").";
                }
                else
                {
                    lthAfterGeneralManagerAction_HistoryDescription = "General Manager Approved Evaluation Report";
                    lthAfterGeneralManagerAction_HistoryOutcome = "General Manager Approved The Evaluation Report.";
                    Body += "Your request evaluation report has been reviewed & approved by your General Manager (" + _GeneralManager.DisplayName + ").";
                }
            }
            else if (_GeneralManagerAction == "Rejected")//Recjected
            {
                lthAfterGeneralManagerAction_HistoryDescription = "General Manager Rejected Requisition";
                lthAfterGeneralManagerAction_HistoryOutcome = "General Manager Rejected The Requisition.";
                Body += "Your request has been rejected by your General Manager (" + _GeneralManager.DisplayName + ").<br>Comment: " + _RejectComment + "<br>";
            }
            else //Request Change
            {
                lthAfterGeneralManagerAction_HistoryDescription = "General Manager Request Change";
                lthAfterGeneralManagerAction_HistoryOutcome = "General Manager Request Change From the Requester.";
            }
            Body += "<br>Programme Name: " + _ProgrammeName;
            Body += "<br>Programme Date: " + _ProgrammeDate;
            Body += "<br><br>Kind Regards,<br>";
            Body += "HR Learning Management";
            Body += "<br><br>NOTE: This email message is auto generated, Please do not reply to the email address.";
            if (_GeneralManagerAction == "Approved")
            {
                if (isFirstTime)
                    EMailHandler.SendEmail(To, "workflows@mtn.com", Cc, "HR Learning Management Request Approved by General Manager", Body);
                else
                    EMailHandler.SendEmail(To, "workflows@mtn.com", Cc, "Training Evaluation Report Approved by General Manager", Body);
            }
            else if (_GeneralManagerAction == "Rejected")//Recjected
                EMailHandler.SendEmail(To, "workflows@mtn.com", Cc, "HR Learning Management Request Rejected by General Manager", Body);
        }

        private void GeneralManagerApprovedAction(object sender, ConditionalEventArgs e)
        {
            if (_GeneralManagerAction == "Approved" && isFirstTime)
            {
                isFirstTime = false;
                isRequestChange = false;
                e.Result = true;//Cycle Completed for the 1st time
            }
            else //i.e Did Not Approve or Finally Approved
            {
                e.Result = false;
            }
        }

        private void GeneralManagerFinallyApprovedAction(object sender, ConditionalEventArgs e)
        {
            if (_GeneralManagerAction == "Approved" && !isFirstTime)
            {
                e.Result = true;//Cycle Completed for the 2nd time (Workflow Approved)
            }
            else //i.e Did Not Approve
            {
                e.Result = false;
            }
        }

        private void GeneralManagerRejectedAction(object sender, ConditionalEventArgs e)
        {
            if (_GeneralManagerAction == "Rejected")
            {
                e.Result = true;//Workflow Ends - Cycle Completed & Requisition Rejected
            }
            else
            {
                e.Result = false;//Assign Task to the Requester
                isRequestChange = true;
                _Rejector = HRLearningManagementRequestWFRecjector.GeneralManager;
            }
        }

        #endregion General Manager

        #region Learning Management Team

        private void ctLearningManagementTeam_MethodInvoking(object sender, EventArgs e)
        {
            ctLearningManagementTeam_TaskId = Guid.NewGuid();
            ctLearningManagementTeam_TaskProperties.Title = "Requester Task - " + DateTime.Now.ToString();

            SPFieldUserValue _LearningManagementTeamFieldUserValue;
            SPGroup LearningManagementTeamGroup = workflowProperties.Web.SiteGroups["Learning Management Team"];
            _LearningManagementTeamFieldUserValue = new SPFieldUserValue(workflowProperties.Web, LearningManagementTeamGroup.ID, LearningManagementTeamGroup.Name);
            ctLearningManagementTeam_TaskProperties.AssignedTo = _LearningManagementTeamFieldUserValue.LookupValue;

            ctLearningManagementTeam_TaskProperties.ExtendedProperties["DisplayForm"] = "Requester";
            ctLearningManagementTeam_TaskProperties.ExtendedProperties["ItemID"] = RequestItemID;
            ctLearningManagementTeam_TaskProperties.ExtendedProperties["ListTitle"] = RequestsListTitle;
            ctLearningManagementTeam_TaskProperties.ExtendedProperties["Comments"] = _Comments;
            ctLearningManagementTeam_TaskProperties.ExtendedProperties["Rejector"] = _Rejector;
            ctLearningManagementTeam_TaskProperties.ExtendedProperties["isFirstTime"] = isFirstTime;
            ctLearningManagementTeam_TaskProperties.ExtendedProperties["isRequestChange"] = isRequestChange;

            File.WriteAllText(@"C:\Temp\rejector.log", _Rejector.ToString());

            lthLearningManagementTeamTaskCreated_HistoryDescription = "Learning Management Team Task Created";
            if (isFirstTime)
                lthLearningManagementTeamTaskCreated_HistoryOutcome = "The system awaits for Learning Management Team Update";
            else
            {
                if (isRequestChange)
                    lthLearningManagementTeamTaskCreated_HistoryOutcome = "The system awaits for Learning Management Team to update Training Evaluation Report";
                else
                    lthLearningManagementTeamTaskCreated_HistoryOutcome = "The system awaits for Learning Management Team to upload Training Evaluation Report";
            }

            WorkflowHandler.ChangeTaskItemSecurity(ctLearningManagementTeam_SpecialPermissions, ctLearningManagementTeam_TaskProperties);
        }

        private void isLearningManagementTeamPendingReportUpload(object sender, ConditionalEventArgs e)
        {
            if (isRequestChange)
                e.Result = false;
            else
                e.Result = true;
        }

        private void caSendLearningManagementTeamTaskCreatedMail_ExecuteCode(object sender, EventArgs e)
        {
            //SendEmail
            List<string> To = new List<string>();
            List<string> Cc = new List<string>();

            string _ProgrammeName = workflowProperties.List.GetItemByIdSelectedFields(workflowProperties.ItemId, "Programme name").
               ReadItemFieldString("Programme name");
            string _ProgrammeDate = workflowProperties.List.GetItemByIdSelectedFields(workflowProperties.ItemId, "Programme date").
                    ReadItemDateTime("Programme date").ToString("dd MM yyyy");

            File.WriteAllText(@"C:\Temp\HRgroup.log", LearningManagementTeamEmailAddress.ToString());

            if (!String.IsNullOrEmpty(LearningManagementTeamEmailAddress))
                To.Add(LearningManagementTeamEmailAddress);

            if (!String.IsNullOrEmpty(_Requester.EmailAddress))
                To.Add(_Requester.EmailAddress);

            //string Body = "Dear " + _Requester.DisplayName + ",<br><br>";
            string Body = "Dear Learning Management Team Member,<br><br>";
            Body += "The General Manager (" + _GeneralManager.DisplayName + ") ";
            //A new requisition is awaiting your kind approval
            //_Rejector = 0;

            //if (_Rejector == HRLearningManagementRequestWFRecjector.LineManager)
            //{
            //    if (!String.IsNullOrEmpty(_LineManager.EmailAddress))
            //        Cc.Add(_LineManager.EmailAddress);
            //    Body += "The Line Manager (" + _LineManager.DisplayName + ") ";
            //}
            //else if (_Rejector == HRLearningManagementRequestWFRecjector.SeniorManager)
            //{
            //    if (!String.IsNullOrEmpty(_SeniorManager.EmailAddress))
            //        Cc.Add(_SeniorManager.EmailAddress);
            //    Body += "The Senior Manager (" + _SeniorManager.DisplayName + ") ";
            //}
            //else if (_Rejector == HRLearningManagementRequestWFRecjector.GeneralManager)
            //{
            //    if (!String.IsNullOrEmpty(_GeneralManager.EmailAddress))
            //        Cc.Add(_GeneralManager.EmailAddress);
            //    Body += "The General Manager (" + _GeneralManager.DisplayName + ") ";
            //}
            if (isFirstTime)
                Body += "has not approved your request and require some updates from you.";
            else
            {
                if (isRequestChange)
                    Body += "has not approved your Training Evaluation Report and require some updates from you.";
                else
                    Body += "has approved your request and currently the system awaits you to upload the Training Evaluation Report.";
            }
            Body += "<br>Programme Name: " + _ProgrammeName;
            Body += "<br>Programme Date: " + _ProgrammeDate;
            Body += "<br><hr>For more details please " + WorkflowHandler.getTaskUrl(ctLearningManagementTeam.ListItemId.ToString(), workflowProperties, _WorkflowTaskASPX) + " To Update/Cancel your request.";
            Body += "<br><br>Kind Regards,<br>";
            Body += "HR Learning Management";
            Body += "<br><br>NOTE: This email message is auto generated, Please do not reply to the email address.";
            EMailHandler.SendEmail(To, "workflows@mtn.com", Cc, "HR Learning Management Request"/* by " + _Requester.DisplayName*/, Body);
        }

        private void otcLearningManagementTeam_Invoked(object sender, ExternalDataEventArgs e)
        {
            _LearningManagementTeamAction = otcLearningManagementTeam_AfterProperties.ExtendedProperties["Action"].ToString();
            _Comments = otcLearningManagementTeam_AfterProperties.ExtendedProperties["Comments"].ToString();
            if (otcLearningManagementTeam_AfterProperties.ExtendedProperties["CurrentUserId"] != null)
                lthAfterLearningManagementTeamAction_UserId = int.Parse(otcLearningManagementTeam_AfterProperties.ExtendedProperties["CurrentUserId"].ToString());
            if (_LearningManagementTeamAction == "Approved")
            {
                if (isFirstTime)
                {
                    lthAfterLearningManagementTeamAction_HistoryDescription = "Learning Management Team Updated Requisition";
                    lthAfterLearningManagementTeamAction_HistoryOutcome = "Learning Management Team Updated The requisition.";
                }
                else
                {
                    if (isRequestChange)
                    {
                        lthAfterLearningManagementTeamAction_HistoryDescription = "Learning Management Team Updated Training Evaluation Report";
                        lthAfterLearningManagementTeamAction_HistoryOutcome = "Learning Management Team Updated Training Evaluation Report.";
                    }
                    else
                    {
                        lthAfterLearningManagementTeamAction_HistoryDescription = "Learning Management Team Uploaded Training Evaluation Report";
                        lthAfterLearningManagementTeamAction_HistoryOutcome = "Learning Management Team Uploaded Training Evaluation Report.";
                    }
                }
            }
            else// if (_RequesterAction == "Rejected")//Recjected i.e he canceled the request
            {
                lthAfterLearningManagementTeamAction_HistoryDescription = "Learning Management Team Canceled Requisition";
                lthAfterLearningManagementTeamAction_HistoryOutcome = "Learning Management Team Canceled The Requisition.";
            }
        }

        private void LearningManagementTeamUpdatedAction(object sender, ConditionalEventArgs e)
        {
            if (_LearningManagementTeamAction == "Approved") //Request Updated or Report Uploaded
            {
                e.Result = true;//Assign Task to the Line Manager
            }
            else //i.e Request Canceled
            {
                e.Result = false;
            }
        }

        #endregion Learning Management Team

        private void dtLineManager_MethodInvoking(object sender, EventArgs e)
        {

        }
    }
    public enum HRLearningManagementRequestWFRecjector
	{
		LineManager = 0, SeniorManager = 1, GeneralManager = 2
	}
}
