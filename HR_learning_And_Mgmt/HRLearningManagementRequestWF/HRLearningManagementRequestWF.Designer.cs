﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Drawing;
using System.Reflection;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;

namespace HR_learning_And_Mgmt.HRLearningManagementRequestWF
{
    public sealed partial class HRLearningManagementRequestWF
    {
        #region Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCode]
        private void InitializeComponent()
        {
            this.CanModifyActivities = true;
            System.Workflow.Runtime.CorrelationToken correlationtoken1 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.Runtime.CorrelationToken correlationtoken2 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.Runtime.CorrelationToken correlationtoken3 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.Runtime.CorrelationToken correlationtoken4 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.Runtime.CorrelationToken correlationtoken5 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.Runtime.CorrelationToken correlationtoken6 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.Runtime.CorrelationToken correlationtoken7 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.Runtime.CorrelationToken correlationtoken8 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.Runtime.CorrelationToken correlationtoken9 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.Runtime.CorrelationToken correlationtoken10 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.Runtime.CorrelationToken correlationtoken11 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.Runtime.CorrelationToken correlationtoken12 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.Runtime.CorrelationToken correlationtoken13 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.Activities.CodeCondition codecondition1 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition2 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition3 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition4 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition5 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition6 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition7 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition8 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition9 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition10 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition11 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Activities.CodeCondition codecondition12 = new System.Workflow.Activities.CodeCondition();
            System.Workflow.Runtime.CorrelationToken correlationtoken14 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind1 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind2 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind3 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind4 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind5 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken15 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind6 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind7 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind8 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken16 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind9 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind10 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind11 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind12 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken17 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind13 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind14 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind15 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind16 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind17 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken18 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind18 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind19 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind20 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken19 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind21 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind22 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind23 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind24 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken20 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind25 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind26 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind27 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind28 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind29 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken21 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind30 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind31 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind32 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken22 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind33 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind34 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind35 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind36 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken23 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind37 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind38 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind39 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind40 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind41 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken24 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind42 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind43 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind44 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken25 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind45 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind46 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind47 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind48 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.Runtime.CorrelationToken correlationtoken26 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.ComponentModel.ActivityBind activitybind49 = new System.Workflow.ComponentModel.ActivityBind();
            this.ssaLearningManagementTeamToCompleteState = new System.Workflow.Activities.SetStateActivity();
            this.ssLearningManagementTeamCanceledRequest = new Microsoft.SharePoint.WorkflowActions.SetState();
            this.ssaLearningManagementTeamToLineManager = new System.Workflow.Activities.SetStateActivity();
            this.ssPendingLearningManagementTeamUpdate = new Microsoft.SharePoint.WorkflowActions.SetState();
            this.ssPendingLearningManagementTeamReportUpload = new Microsoft.SharePoint.WorkflowActions.SetState();
            this.ssaGeneralManagerToRequester = new System.Workflow.Activities.SetStateActivity();
            this.ssaGeneralManagerRejected = new System.Workflow.Activities.SetStateActivity();
            this.ssGeneralManagerRejected = new Microsoft.SharePoint.WorkflowActions.SetState();
            this.ssaGeneralManagerFinallyApproved = new System.Workflow.Activities.SetStateActivity();
            this.ssRequisitionFinallyApproved = new Microsoft.SharePoint.WorkflowActions.SetState();
            this.ssaGeneralManagerApproved = new System.Workflow.Activities.SetStateActivity();
            this.ssPendingGeneralManagerFinalApproval = new Microsoft.SharePoint.WorkflowActions.SetState();
            this.ssPendingGeneralManagerApproval = new Microsoft.SharePoint.WorkflowActions.SetState();
            this.ssaSeniorManagerToRequester = new System.Workflow.Activities.SetStateActivity();
            this.ssaSeniorManagerRejected = new System.Workflow.Activities.SetStateActivity();
            this.ssSeniorManagerRejected = new Microsoft.SharePoint.WorkflowActions.SetState();
            this.ssaSeniorManagerToGeneralManager = new System.Workflow.Activities.SetStateActivity();
            this.ssPendingSeniorManagerFinalApproval = new Microsoft.SharePoint.WorkflowActions.SetState();
            this.ssPendingSeniorManagerApproval = new Microsoft.SharePoint.WorkflowActions.SetState();
            this.ssaLineManagerToRequester = new System.Workflow.Activities.SetStateActivity();
            this.ssaLineManagerRejected = new System.Workflow.Activities.SetStateActivity();
            this.ssLineManagerRejected = new Microsoft.SharePoint.WorkflowActions.SetState();
            this.ssaLineManagerToSeniorManager = new System.Workflow.Activities.SetStateActivity();
            this.ssPendingLineManagerFinalApproval = new Microsoft.SharePoint.WorkflowActions.SetState();
            this.ssPendingLineManagerApproval = new Microsoft.SharePoint.WorkflowActions.SetState();
            this.LearningManagementTeamCanceledRequest = new System.Workflow.Activities.IfElseBranchActivity();
            this.LearningManagementTeamUpdatedRequest = new System.Workflow.Activities.IfElseBranchActivity();
            this.LearningManagementTeamUpdate = new System.Workflow.Activities.IfElseBranchActivity();
            this.LearningManagementTeamReportUpload = new System.Workflow.Activities.IfElseBranchActivity();
            this.GeneralManagerRequestChange = new System.Workflow.Activities.IfElseBranchActivity();
            this.GeneralManagerRejected = new System.Workflow.Activities.IfElseBranchActivity();
            this.GeneralManagerFinallyApproved = new System.Workflow.Activities.IfElseBranchActivity();
            this.GeneralManagerApproved = new System.Workflow.Activities.IfElseBranchActivity();
            this.GeneralManagerFinalApproval = new System.Workflow.Activities.IfElseBranchActivity();
            this.GeneralManagerFirstApproval = new System.Workflow.Activities.IfElseBranchActivity();
            this.SeniorManagerRequestChange = new System.Workflow.Activities.IfElseBranchActivity();
            this.SeniorManagerRejected = new System.Workflow.Activities.IfElseBranchActivity();
            this.SeniorManagerApproved = new System.Workflow.Activities.IfElseBranchActivity();
            this.SeniorManagerFinalApproval = new System.Workflow.Activities.IfElseBranchActivity();
            this.SeniorManagerFirstApproval = new System.Workflow.Activities.IfElseBranchActivity();
            this.LineManagerRequestChange = new System.Workflow.Activities.IfElseBranchActivity();
            this.LineManagerRejected = new System.Workflow.Activities.IfElseBranchActivity();
            this.LineManagerApproved = new System.Workflow.Activities.IfElseBranchActivity();
            this.LineManagerFinalApproval = new System.Workflow.Activities.IfElseBranchActivity();
            this.LineManagerFirstApproval = new System.Workflow.Activities.IfElseBranchActivity();
            this.ifElseActivity4 = new System.Workflow.Activities.IfElseActivity();
            this.dtLearningManagementTeam = new Microsoft.SharePoint.WorkflowActions.DeleteTask();
            this.lthAfterLearningManagementTeamAction = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.otcLearningManagementTeam = new Microsoft.SharePoint.WorkflowActions.OnTaskChanged();
            this.caSendLearningManagementTeamTaskCreatedMail = new System.Workflow.Activities.CodeActivity();
            this.lthLearningManagementTeamTaskCreated = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.ifElseActivity8 = new System.Workflow.Activities.IfElseActivity();
            this.ctLearningManagementTeam = new Microsoft.SharePoint.WorkflowActions.CreateTask();
            this.ifElseActivity3 = new System.Workflow.Activities.IfElseActivity();
            this.dtGeneralManager = new Microsoft.SharePoint.WorkflowActions.DeleteTask();
            this.lthAfterGeneralManagerAction = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.otcGeneralManager = new Microsoft.SharePoint.WorkflowActions.OnTaskChanged();
            this.caSendGeneralManagerTaskCreatedMail = new System.Workflow.Activities.CodeActivity();
            this.lthGeneralManagerTaskCreated = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.ifElseActivity7 = new System.Workflow.Activities.IfElseActivity();
            this.ctGeneralManager = new Microsoft.SharePoint.WorkflowActions.CreateTask();
            this.ifElseActivity2 = new System.Workflow.Activities.IfElseActivity();
            this.dtSeniorManager = new Microsoft.SharePoint.WorkflowActions.DeleteTask();
            this.lthAfterSeniorManagerAction = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.otcSeniorManager = new Microsoft.SharePoint.WorkflowActions.OnTaskChanged();
            this.caSendSeniorManagerTaskCreatedMail = new System.Workflow.Activities.CodeActivity();
            this.lthSeniorManagerTaskCreated = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.ifElseActivity6 = new System.Workflow.Activities.IfElseActivity();
            this.ctSeniorManager = new Microsoft.SharePoint.WorkflowActions.CreateTask();
            this.ifElseActivity1 = new System.Workflow.Activities.IfElseActivity();
            this.dtLineManager = new Microsoft.SharePoint.WorkflowActions.DeleteTask();
            this.lthAfterLineManagerAction = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.otcLineManager = new Microsoft.SharePoint.WorkflowActions.OnTaskChanged();
            this.caSendLineManagerTaskCreatedMail = new System.Workflow.Activities.CodeActivity();
            this.lthLineManagerTaskCreated = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.ifElseActivity5 = new System.Workflow.Activities.IfElseActivity();
            this.ctLineManager = new Microsoft.SharePoint.WorkflowActions.CreateTask();
            this.ssaToLineManager = new System.Workflow.Activities.SetStateActivity();
            this.onWorkflowActivated1 = new Microsoft.SharePoint.WorkflowActions.OnWorkflowActivated();
            this.edaLearningManagementTeam = new System.Workflow.Activities.EventDrivenActivity();
            this.siaLearningManagementTeam = new System.Workflow.Activities.StateInitializationActivity();
            this.edaGeneralManager = new System.Workflow.Activities.EventDrivenActivity();
            this.siaGeneralManager = new System.Workflow.Activities.StateInitializationActivity();
            this.edaSeniorManager = new System.Workflow.Activities.EventDrivenActivity();
            this.siaSeniorManager = new System.Workflow.Activities.StateInitializationActivity();
            this.edaLineManager = new System.Workflow.Activities.EventDrivenActivity();
            this.siaLineManager = new System.Workflow.Activities.StateInitializationActivity();
            this.eventDrivenActivity1 = new System.Workflow.Activities.EventDrivenActivity();
            this.saCompleteState = new System.Workflow.Activities.StateActivity();
            this.saLearningManagementTeam = new System.Workflow.Activities.StateActivity();
            this.saGeneralManager = new System.Workflow.Activities.StateActivity();
            this.saSeniorManager = new System.Workflow.Activities.StateActivity();
            this.saLineManager = new System.Workflow.Activities.StateActivity();
            this.HRLearningManagementRequestWFInitialState = new System.Workflow.Activities.StateActivity();
            // 
            // ssaLearningManagementTeamToCompleteState
            // 
            this.ssaLearningManagementTeamToCompleteState.Name = "ssaLearningManagementTeamToCompleteState";
            this.ssaLearningManagementTeamToCompleteState.TargetStateName = "saCompleteState";
            // 
            // ssLearningManagementTeamCanceledRequest
            // 
            correlationtoken1.Name = "workflowToken";
            correlationtoken1.OwnerActivityName = "HRLearningManagementRequestWF";
            this.ssLearningManagementTeamCanceledRequest.CorrelationToken = correlationtoken1;
            this.ssLearningManagementTeamCanceledRequest.Name = "ssLearningManagementTeamCanceledRequest";
            this.ssLearningManagementTeamCanceledRequest.State = 0;
            // 
            // ssaLearningManagementTeamToLineManager
            // 
            this.ssaLearningManagementTeamToLineManager.Name = "ssaLearningManagementTeamToLineManager";
            this.ssaLearningManagementTeamToLineManager.TargetStateName = "saLineManager";
            // 
            // ssPendingLearningManagementTeamUpdate
            // 
            correlationtoken2.Name = "workflowToken";
            correlationtoken2.OwnerActivityName = "HRLearningManagementRequestWF";
            this.ssPendingLearningManagementTeamUpdate.CorrelationToken = correlationtoken2;
            this.ssPendingLearningManagementTeamUpdate.Name = "ssPendingLearningManagementTeamUpdate";
            this.ssPendingLearningManagementTeamUpdate.State = 15;
            // 
            // ssPendingLearningManagementTeamReportUpload
            // 
            correlationtoken3.Name = "workflowToken";
            correlationtoken3.OwnerActivityName = "HRLearningManagementRequestWF";
            this.ssPendingLearningManagementTeamReportUpload.CorrelationToken = correlationtoken3;
            this.ssPendingLearningManagementTeamReportUpload.Name = "ssPendingLearningManagementTeamReportUpload";
            this.ssPendingLearningManagementTeamReportUpload.State = 19;
            // 
            // ssaGeneralManagerToRequester
            // 
            this.ssaGeneralManagerToRequester.Name = "ssaGeneralManagerToRequester";
            this.ssaGeneralManagerToRequester.TargetStateName = "saLearningManagementTeam";
            // 
            // ssaGeneralManagerRejected
            // 
            this.ssaGeneralManagerRejected.Name = "ssaGeneralManagerRejected";
            this.ssaGeneralManagerRejected.TargetStateName = "saCompleteState";
            // 
            // ssGeneralManagerRejected
            // 
            correlationtoken4.Name = "workflowToken";
            correlationtoken4.OwnerActivityName = "HRLearningManagementRequestWF";
            this.ssGeneralManagerRejected.CorrelationToken = correlationtoken4;
            this.ssGeneralManagerRejected.Name = "ssGeneralManagerRejected";
            this.ssGeneralManagerRejected.State = 24;
            // 
            // ssaGeneralManagerFinallyApproved
            // 
            this.ssaGeneralManagerFinallyApproved.Name = "ssaGeneralManagerFinallyApproved";
            this.ssaGeneralManagerFinallyApproved.TargetStateName = "saCompleteState";
            // 
            // ssRequisitionFinallyApproved
            // 
            correlationtoken5.Name = "workflowToken";
            correlationtoken5.OwnerActivityName = "HRLearningManagementRequestWF";
            this.ssRequisitionFinallyApproved.CorrelationToken = correlationtoken5;
            this.ssRequisitionFinallyApproved.Name = "ssRequisitionFinallyApproved";
            this.ssRequisitionFinallyApproved.State = 23;
            // 
            // ssaGeneralManagerApproved
            // 
            this.ssaGeneralManagerApproved.Name = "ssaGeneralManagerApproved";
            this.ssaGeneralManagerApproved.TargetStateName = "saLearningManagementTeam";
            // 
            // ssPendingGeneralManagerFinalApproval
            // 
            correlationtoken6.Name = "workflowToken";
            correlationtoken6.OwnerActivityName = "HRLearningManagementRequestWF";
            this.ssPendingGeneralManagerFinalApproval.CorrelationToken = correlationtoken6;
            this.ssPendingGeneralManagerFinalApproval.Name = "ssPendingGeneralManagerFinalApproval";
            this.ssPendingGeneralManagerFinalApproval.State = 22;
            // 
            // ssPendingGeneralManagerApproval
            // 
            correlationtoken7.Name = "workflowToken";
            correlationtoken7.OwnerActivityName = "HRLearningManagementRequestWF";
            this.ssPendingGeneralManagerApproval.CorrelationToken = correlationtoken7;
            this.ssPendingGeneralManagerApproval.Name = "ssPendingGeneralManagerApproval";
            this.ssPendingGeneralManagerApproval.State = 18;
            // 
            // ssaSeniorManagerToRequester
            // 
            this.ssaSeniorManagerToRequester.Name = "ssaSeniorManagerToRequester";
            this.ssaSeniorManagerToRequester.TargetStateName = "saLearningManagementTeam";
            // 
            // ssaSeniorManagerRejected
            // 
            this.ssaSeniorManagerRejected.Name = "ssaSeniorManagerRejected";
            this.ssaSeniorManagerRejected.TargetStateName = "saCompleteState";
            // 
            // ssSeniorManagerRejected
            // 
            correlationtoken8.Name = "workflowToken";
            correlationtoken8.OwnerActivityName = "HRLearningManagementRequestWF";
            this.ssSeniorManagerRejected.CorrelationToken = correlationtoken8;
            this.ssSeniorManagerRejected.Name = "ssSeniorManagerRejected";
            this.ssSeniorManagerRejected.State = 24;
            // 
            // ssaSeniorManagerToGeneralManager
            // 
            this.ssaSeniorManagerToGeneralManager.Name = "ssaSeniorManagerToGeneralManager";
            this.ssaSeniorManagerToGeneralManager.TargetStateName = "saGeneralManager";
            // 
            // ssPendingSeniorManagerFinalApproval
            // 
            correlationtoken9.Name = "workflowToken";
            correlationtoken9.OwnerActivityName = "HRLearningManagementRequestWF";
            this.ssPendingSeniorManagerFinalApproval.CorrelationToken = correlationtoken9;
            this.ssPendingSeniorManagerFinalApproval.Name = "ssPendingSeniorManagerFinalApproval";
            this.ssPendingSeniorManagerFinalApproval.State = 21;
            // 
            // ssPendingSeniorManagerApproval
            // 
            correlationtoken10.Name = "workflowToken";
            correlationtoken10.OwnerActivityName = "HRLearningManagementRequestWF";
            this.ssPendingSeniorManagerApproval.CorrelationToken = correlationtoken10;
            this.ssPendingSeniorManagerApproval.Name = "ssPendingSeniorManagerApproval";
            this.ssPendingSeniorManagerApproval.State = 17;
            // 
            // ssaLineManagerToRequester
            // 
            this.ssaLineManagerToRequester.Name = "ssaLineManagerToRequester";
            this.ssaLineManagerToRequester.TargetStateName = "saLearningManagementTeam";
            // 
            // ssaLineManagerRejected
            // 
            this.ssaLineManagerRejected.Name = "ssaLineManagerRejected";
            this.ssaLineManagerRejected.TargetStateName = "saCompleteState";
            // 
            // ssLineManagerRejected
            // 
            correlationtoken11.Name = "workflowToken";
            correlationtoken11.OwnerActivityName = "HRLearningManagementRequestWF";
            this.ssLineManagerRejected.CorrelationToken = correlationtoken11;
            this.ssLineManagerRejected.Name = "ssLineManagerRejected";
            this.ssLineManagerRejected.State = 24;
            // 
            // ssaLineManagerToSeniorManager
            // 
            this.ssaLineManagerToSeniorManager.Name = "ssaLineManagerToSeniorManager";
            this.ssaLineManagerToSeniorManager.TargetStateName = "saSeniorManager";
            // 
            // ssPendingLineManagerFinalApproval
            // 
            correlationtoken12.Name = "workflowToken";
            correlationtoken12.OwnerActivityName = "HRLearningManagementRequestWF";
            this.ssPendingLineManagerFinalApproval.CorrelationToken = correlationtoken12;
            this.ssPendingLineManagerFinalApproval.Name = "ssPendingLineManagerFinalApproval";
            this.ssPendingLineManagerFinalApproval.State = 20;
            // 
            // ssPendingLineManagerApproval
            // 
            correlationtoken13.Name = "workflowToken";
            correlationtoken13.OwnerActivityName = "HRLearningManagementRequestWF";
            this.ssPendingLineManagerApproval.CorrelationToken = correlationtoken13;
            this.ssPendingLineManagerApproval.Name = "ssPendingLineManagerApproval";
            this.ssPendingLineManagerApproval.State = 16;
            // 
            // LearningManagementTeamCanceledRequest
            // 
            this.LearningManagementTeamCanceledRequest.Activities.Add(this.ssLearningManagementTeamCanceledRequest);
            this.LearningManagementTeamCanceledRequest.Activities.Add(this.ssaLearningManagementTeamToCompleteState);
            this.LearningManagementTeamCanceledRequest.Name = "LearningManagementTeamCanceledRequest";
            // 
            // LearningManagementTeamUpdatedRequest
            // 
            this.LearningManagementTeamUpdatedRequest.Activities.Add(this.ssaLearningManagementTeamToLineManager);
            codecondition1.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.LearningManagementTeamUpdatedAction);
            this.LearningManagementTeamUpdatedRequest.Condition = codecondition1;
            this.LearningManagementTeamUpdatedRequest.Name = "LearningManagementTeamUpdatedRequest";
            // 
            // LearningManagementTeamUpdate
            // 
            this.LearningManagementTeamUpdate.Activities.Add(this.ssPendingLearningManagementTeamUpdate);
            this.LearningManagementTeamUpdate.Name = "LearningManagementTeamUpdate";
            // 
            // LearningManagementTeamReportUpload
            // 
            this.LearningManagementTeamReportUpload.Activities.Add(this.ssPendingLearningManagementTeamReportUpload);
            codecondition2.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.isLearningManagementTeamPendingReportUpload);
            this.LearningManagementTeamReportUpload.Condition = codecondition2;
            this.LearningManagementTeamReportUpload.Name = "LearningManagementTeamReportUpload";
            // 
            // GeneralManagerRequestChange
            // 
            this.GeneralManagerRequestChange.Activities.Add(this.ssaGeneralManagerToRequester);
            this.GeneralManagerRequestChange.Name = "GeneralManagerRequestChange";
            // 
            // GeneralManagerRejected
            // 
            this.GeneralManagerRejected.Activities.Add(this.ssGeneralManagerRejected);
            this.GeneralManagerRejected.Activities.Add(this.ssaGeneralManagerRejected);
            codecondition3.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.GeneralManagerRejectedAction);
            this.GeneralManagerRejected.Condition = codecondition3;
            this.GeneralManagerRejected.Name = "GeneralManagerRejected";
            // 
            // GeneralManagerFinallyApproved
            // 
            this.GeneralManagerFinallyApproved.Activities.Add(this.ssRequisitionFinallyApproved);
            this.GeneralManagerFinallyApproved.Activities.Add(this.ssaGeneralManagerFinallyApproved);
            codecondition4.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.GeneralManagerFinallyApprovedAction);
            this.GeneralManagerFinallyApproved.Condition = codecondition4;
            this.GeneralManagerFinallyApproved.Name = "GeneralManagerFinallyApproved";
            // 
            // GeneralManagerApproved
            // 
            this.GeneralManagerApproved.Activities.Add(this.ssaGeneralManagerApproved);
            codecondition5.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.GeneralManagerApprovedAction);
            this.GeneralManagerApproved.Condition = codecondition5;
            this.GeneralManagerApproved.Name = "GeneralManagerApproved";
            // 
            // GeneralManagerFinalApproval
            // 
            this.GeneralManagerFinalApproval.Activities.Add(this.ssPendingGeneralManagerFinalApproval);
            this.GeneralManagerFinalApproval.Name = "GeneralManagerFinalApproval";
            // 
            // GeneralManagerFirstApproval
            // 
            this.GeneralManagerFirstApproval.Activities.Add(this.ssPendingGeneralManagerApproval);
            codecondition6.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.isGeneralManagerFinalApproval);
            this.GeneralManagerFirstApproval.Condition = codecondition6;
            this.GeneralManagerFirstApproval.Name = "GeneralManagerFirstApproval";
            // 
            // SeniorManagerRequestChange
            // 
            this.SeniorManagerRequestChange.Activities.Add(this.ssaSeniorManagerToRequester);
            this.SeniorManagerRequestChange.Name = "SeniorManagerRequestChange";
            // 
            // SeniorManagerRejected
            // 
            this.SeniorManagerRejected.Activities.Add(this.ssSeniorManagerRejected);
            this.SeniorManagerRejected.Activities.Add(this.ssaSeniorManagerRejected);
            codecondition7.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.SeniorManagerRejectedAction);
            this.SeniorManagerRejected.Condition = codecondition7;
            this.SeniorManagerRejected.Name = "SeniorManagerRejected";
            // 
            // SeniorManagerApproved
            // 
            this.SeniorManagerApproved.Activities.Add(this.ssaSeniorManagerToGeneralManager);
            codecondition8.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.SeniorManagerApprovedAction);
            this.SeniorManagerApproved.Condition = codecondition8;
            this.SeniorManagerApproved.Name = "SeniorManagerApproved";
            // 
            // SeniorManagerFinalApproval
            // 
            this.SeniorManagerFinalApproval.Activities.Add(this.ssPendingSeniorManagerFinalApproval);
            this.SeniorManagerFinalApproval.Name = "SeniorManagerFinalApproval";
            // 
            // SeniorManagerFirstApproval
            // 
            this.SeniorManagerFirstApproval.Activities.Add(this.ssPendingSeniorManagerApproval);
            codecondition9.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.isSeniorManagerFinalApproval);
            this.SeniorManagerFirstApproval.Condition = codecondition9;
            this.SeniorManagerFirstApproval.Name = "SeniorManagerFirstApproval";
            // 
            // LineManagerRequestChange
            // 
            this.LineManagerRequestChange.Activities.Add(this.ssaLineManagerToRequester);
            this.LineManagerRequestChange.Name = "LineManagerRequestChange";
            // 
            // LineManagerRejected
            // 
            this.LineManagerRejected.Activities.Add(this.ssLineManagerRejected);
            this.LineManagerRejected.Activities.Add(this.ssaLineManagerRejected);
            codecondition10.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.LineManagerRejectedAction);
            this.LineManagerRejected.Condition = codecondition10;
            this.LineManagerRejected.Name = "LineManagerRejected";
            // 
            // LineManagerApproved
            // 
            this.LineManagerApproved.Activities.Add(this.ssaLineManagerToSeniorManager);
            codecondition11.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.LineManagerApprovedAction);
            this.LineManagerApproved.Condition = codecondition11;
            this.LineManagerApproved.Name = "LineManagerApproved";
            // 
            // LineManagerFinalApproval
            // 
            this.LineManagerFinalApproval.Activities.Add(this.ssPendingLineManagerFinalApproval);
            this.LineManagerFinalApproval.Name = "LineManagerFinalApproval";
            // 
            // LineManagerFirstApproval
            // 
            this.LineManagerFirstApproval.Activities.Add(this.ssPendingLineManagerApproval);
            codecondition12.Condition += new System.EventHandler<System.Workflow.Activities.ConditionalEventArgs>(this.isLineManagerFinalApproval);
            this.LineManagerFirstApproval.Condition = codecondition12;
            this.LineManagerFirstApproval.Name = "LineManagerFirstApproval";
            // 
            // ifElseActivity4
            // 
            this.ifElseActivity4.Activities.Add(this.LearningManagementTeamUpdatedRequest);
            this.ifElseActivity4.Activities.Add(this.LearningManagementTeamCanceledRequest);
            this.ifElseActivity4.Name = "ifElseActivity4";
            // 
            // dtLearningManagementTeam
            // 
            correlationtoken14.Name = "LearningManagementTeamToken";
            correlationtoken14.OwnerActivityName = "saLearningManagementTeam";
            this.dtLearningManagementTeam.CorrelationToken = correlationtoken14;
            this.dtLearningManagementTeam.Name = "dtLearningManagementTeam";
            activitybind1.Name = "HRLearningManagementRequestWF";
            activitybind1.Path = "ctLearningManagementTeam_TaskId";
            this.dtLearningManagementTeam.SetBinding(Microsoft.SharePoint.WorkflowActions.DeleteTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind1)));
            // 
            // lthAfterLearningManagementTeamAction
            // 
            this.lthAfterLearningManagementTeamAction.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.lthAfterLearningManagementTeamAction.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind2.Name = "HRLearningManagementRequestWF";
            activitybind2.Path = "lthAfterLearningManagementTeamAction_HistoryDescription";
            activitybind3.Name = "HRLearningManagementRequestWF";
            activitybind3.Path = "lthAfterLearningManagementTeamAction_HistoryOutcome";
            this.lthAfterLearningManagementTeamAction.Name = "lthAfterLearningManagementTeamAction";
            this.lthAfterLearningManagementTeamAction.OtherData = "";
            activitybind4.Name = "HRLearningManagementRequestWF";
            activitybind4.Path = "lthAfterLearningManagementTeamAction_UserId";
            this.lthAfterLearningManagementTeamAction.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind2)));
            this.lthAfterLearningManagementTeamAction.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind3)));
            this.lthAfterLearningManagementTeamAction.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.UserIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind4)));
            // 
            // otcLearningManagementTeam
            // 
            activitybind5.Name = "HRLearningManagementRequestWF";
            activitybind5.Path = "otcLearningManagementTeam_AfterProperties";
            this.otcLearningManagementTeam.BeforeProperties = null;
            correlationtoken15.Name = "LearningManagementTeamToken";
            correlationtoken15.OwnerActivityName = "saLearningManagementTeam";
            this.otcLearningManagementTeam.CorrelationToken = correlationtoken15;
            this.otcLearningManagementTeam.Executor = null;
            this.otcLearningManagementTeam.Name = "otcLearningManagementTeam";
            activitybind6.Name = "HRLearningManagementRequestWF";
            activitybind6.Path = "ctLearningManagementTeam_TaskId";
            this.otcLearningManagementTeam.Invoked += new System.EventHandler<System.Workflow.Activities.ExternalDataEventArgs>(this.otcLearningManagementTeam_Invoked);
            this.otcLearningManagementTeam.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind6)));
            this.otcLearningManagementTeam.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.AfterPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind5)));
            // 
            // caSendLearningManagementTeamTaskCreatedMail
            // 
            this.caSendLearningManagementTeamTaskCreatedMail.Name = "caSendLearningManagementTeamTaskCreatedMail";
            this.caSendLearningManagementTeamTaskCreatedMail.ExecuteCode += new System.EventHandler(this.caSendLearningManagementTeamTaskCreatedMail_ExecuteCode);
            // 
            // lthLearningManagementTeamTaskCreated
            // 
            this.lthLearningManagementTeamTaskCreated.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.lthLearningManagementTeamTaskCreated.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind7.Name = "HRLearningManagementRequestWF";
            activitybind7.Path = "lthLearningManagementTeamTaskCreated_HistoryDescription";
            activitybind8.Name = "HRLearningManagementRequestWF";
            activitybind8.Path = "lthLearningManagementTeamTaskCreated_HistoryOutcome";
            this.lthLearningManagementTeamTaskCreated.Name = "lthLearningManagementTeamTaskCreated";
            this.lthLearningManagementTeamTaskCreated.OtherData = "";
            this.lthLearningManagementTeamTaskCreated.UserId = -1;
            this.lthLearningManagementTeamTaskCreated.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind7)));
            this.lthLearningManagementTeamTaskCreated.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind8)));
            // 
            // ifElseActivity8
            // 
            this.ifElseActivity8.Activities.Add(this.LearningManagementTeamReportUpload);
            this.ifElseActivity8.Activities.Add(this.LearningManagementTeamUpdate);
            this.ifElseActivity8.Name = "ifElseActivity8";
            // 
            // ctLearningManagementTeam
            // 
            correlationtoken16.Name = "LearningManagementTeamToken";
            correlationtoken16.OwnerActivityName = "saLearningManagementTeam";
            this.ctLearningManagementTeam.CorrelationToken = correlationtoken16;
            activitybind9.Name = "HRLearningManagementRequestWF";
            activitybind9.Path = "ctLearningManagementTeam_ListItemId";
            this.ctLearningManagementTeam.Name = "ctLearningManagementTeam";
            activitybind10.Name = "HRLearningManagementRequestWF";
            activitybind10.Path = "ctLearningManagementTeam_SpecialPermissions";
            activitybind11.Name = "HRLearningManagementRequestWF";
            activitybind11.Path = "ctLearningManagementTeam_TaskId";
            activitybind12.Name = "HRLearningManagementRequestWF";
            activitybind12.Path = "ctLearningManagementTeam_TaskProperties";
            this.ctLearningManagementTeam.MethodInvoking += new System.EventHandler(this.ctLearningManagementTeam_MethodInvoking);
            this.ctLearningManagementTeam.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind12)));
            this.ctLearningManagementTeam.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind11)));
            this.ctLearningManagementTeam.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.ListItemIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind9)));
            this.ctLearningManagementTeam.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.SpecialPermissionsProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind10)));
            // 
            // ifElseActivity3
            // 
            this.ifElseActivity3.Activities.Add(this.GeneralManagerApproved);
            this.ifElseActivity3.Activities.Add(this.GeneralManagerFinallyApproved);
            this.ifElseActivity3.Activities.Add(this.GeneralManagerRejected);
            this.ifElseActivity3.Activities.Add(this.GeneralManagerRequestChange);
            this.ifElseActivity3.Name = "ifElseActivity3";
            // 
            // dtGeneralManager
            // 
            correlationtoken17.Name = "GeneralManagerToken";
            correlationtoken17.OwnerActivityName = "saGeneralManager";
            this.dtGeneralManager.CorrelationToken = correlationtoken17;
            this.dtGeneralManager.Name = "dtGeneralManager";
            activitybind13.Name = "HRLearningManagementRequestWF";
            activitybind13.Path = "ctGeneralManager_TaskId";
            this.dtGeneralManager.SetBinding(Microsoft.SharePoint.WorkflowActions.DeleteTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind13)));
            // 
            // lthAfterGeneralManagerAction
            // 
            this.lthAfterGeneralManagerAction.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.lthAfterGeneralManagerAction.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind14.Name = "HRLearningManagementRequestWF";
            activitybind14.Path = "lthAfterGeneralManagerAction_HistoryDescription";
            activitybind15.Name = "HRLearningManagementRequestWF";
            activitybind15.Path = "lthAfterGeneralManagerAction_HistoryOutcome";
            this.lthAfterGeneralManagerAction.Name = "lthAfterGeneralManagerAction";
            this.lthAfterGeneralManagerAction.OtherData = "";
            activitybind16.Name = "HRLearningManagementRequestWF";
            activitybind16.Path = "lthAfterGeneralManagerAction_UserId";
            this.lthAfterGeneralManagerAction.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind14)));
            this.lthAfterGeneralManagerAction.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind15)));
            this.lthAfterGeneralManagerAction.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.UserIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind16)));
            // 
            // otcGeneralManager
            // 
            activitybind17.Name = "HRLearningManagementRequestWF";
            activitybind17.Path = "otcGeneralManager_AfterProperties";
            this.otcGeneralManager.BeforeProperties = null;
            correlationtoken18.Name = "GeneralManagerToken";
            correlationtoken18.OwnerActivityName = "saGeneralManager";
            this.otcGeneralManager.CorrelationToken = correlationtoken18;
            this.otcGeneralManager.Executor = null;
            this.otcGeneralManager.Name = "otcGeneralManager";
            activitybind18.Name = "HRLearningManagementRequestWF";
            activitybind18.Path = "ctGeneralManager_TaskId";
            this.otcGeneralManager.Invoked += new System.EventHandler<System.Workflow.Activities.ExternalDataEventArgs>(this.otcGeneralManager_Invoked);
            this.otcGeneralManager.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind18)));
            this.otcGeneralManager.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.AfterPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind17)));
            // 
            // caSendGeneralManagerTaskCreatedMail
            // 
            this.caSendGeneralManagerTaskCreatedMail.Name = "caSendGeneralManagerTaskCreatedMail";
            this.caSendGeneralManagerTaskCreatedMail.ExecuteCode += new System.EventHandler(this.caSendGeneralManagerTaskCreatedMail_ExecuteCode);
            // 
            // lthGeneralManagerTaskCreated
            // 
            this.lthGeneralManagerTaskCreated.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.lthGeneralManagerTaskCreated.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind19.Name = "HRLearningManagementRequestWF";
            activitybind19.Path = "lthGeneralManagerTaskCreated_HistoryDescription";
            activitybind20.Name = "HRLearningManagementRequestWF";
            activitybind20.Path = "lthGeneralManagerTaskCreated_HistoryOutcome";
            this.lthGeneralManagerTaskCreated.Name = "lthGeneralManagerTaskCreated";
            this.lthGeneralManagerTaskCreated.OtherData = "";
            this.lthGeneralManagerTaskCreated.UserId = -1;
            this.lthGeneralManagerTaskCreated.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind19)));
            this.lthGeneralManagerTaskCreated.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind20)));
            // 
            // ifElseActivity7
            // 
            this.ifElseActivity7.Activities.Add(this.GeneralManagerFirstApproval);
            this.ifElseActivity7.Activities.Add(this.GeneralManagerFinalApproval);
            this.ifElseActivity7.Name = "ifElseActivity7";
            // 
            // ctGeneralManager
            // 
            correlationtoken19.Name = "GeneralManagerToken";
            correlationtoken19.OwnerActivityName = "saGeneralManager";
            this.ctGeneralManager.CorrelationToken = correlationtoken19;
            activitybind21.Name = "HRLearningManagementRequestWF";
            activitybind21.Path = "ctGeneralManager_ListItemId";
            this.ctGeneralManager.Name = "ctGeneralManager";
            activitybind22.Name = "HRLearningManagementRequestWF";
            activitybind22.Path = "ctGeneralManager_SpecialPermissions";
            activitybind23.Name = "HRLearningManagementRequestWF";
            activitybind23.Path = "ctGeneralManager_TaskId";
            activitybind24.Name = "HRLearningManagementRequestWF";
            activitybind24.Path = "ctGeneralManager_TaskProperties";
            this.ctGeneralManager.MethodInvoking += new System.EventHandler(this.ctGeneralManager_MethodInvoking);
            this.ctGeneralManager.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind24)));
            this.ctGeneralManager.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind23)));
            this.ctGeneralManager.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.ListItemIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind21)));
            this.ctGeneralManager.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.SpecialPermissionsProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind22)));
            // 
            // ifElseActivity2
            // 
            this.ifElseActivity2.Activities.Add(this.SeniorManagerApproved);
            this.ifElseActivity2.Activities.Add(this.SeniorManagerRejected);
            this.ifElseActivity2.Activities.Add(this.SeniorManagerRequestChange);
            this.ifElseActivity2.Name = "ifElseActivity2";
            // 
            // dtSeniorManager
            // 
            correlationtoken20.Name = "SeniorManagerToken";
            correlationtoken20.OwnerActivityName = "saSeniorManager";
            this.dtSeniorManager.CorrelationToken = correlationtoken20;
            this.dtSeniorManager.Name = "dtSeniorManager";
            activitybind25.Name = "HRLearningManagementRequestWF";
            activitybind25.Path = "ctSeniorManager_TaskId";
            this.dtSeniorManager.SetBinding(Microsoft.SharePoint.WorkflowActions.DeleteTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind25)));
            // 
            // lthAfterSeniorManagerAction
            // 
            this.lthAfterSeniorManagerAction.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.lthAfterSeniorManagerAction.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind26.Name = "HRLearningManagementRequestWF";
            activitybind26.Path = "lthAfterSeniorManagerAction_HistoryDescription";
            activitybind27.Name = "HRLearningManagementRequestWF";
            activitybind27.Path = "lthAfterSeniorManagerAction_HistoryOutcome";
            this.lthAfterSeniorManagerAction.Name = "lthAfterSeniorManagerAction";
            this.lthAfterSeniorManagerAction.OtherData = "";
            activitybind28.Name = "HRLearningManagementRequestWF";
            activitybind28.Path = "lthAfterSeniorManagerAction_UserId";
            this.lthAfterSeniorManagerAction.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind26)));
            this.lthAfterSeniorManagerAction.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind27)));
            this.lthAfterSeniorManagerAction.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.UserIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind28)));
            // 
            // otcSeniorManager
            // 
            activitybind29.Name = "HRLearningManagementRequestWF";
            activitybind29.Path = "otcSeniorManager_AfterProperties";
            this.otcSeniorManager.BeforeProperties = null;
            correlationtoken21.Name = "SeniorManagerToken";
            correlationtoken21.OwnerActivityName = "saSeniorManager";
            this.otcSeniorManager.CorrelationToken = correlationtoken21;
            this.otcSeniorManager.Executor = null;
            this.otcSeniorManager.Name = "otcSeniorManager";
            activitybind30.Name = "HRLearningManagementRequestWF";
            activitybind30.Path = "ctSeniorManager_TaskId";
            this.otcSeniorManager.Invoked += new System.EventHandler<System.Workflow.Activities.ExternalDataEventArgs>(this.otcSeniorManager_Invoked);
            this.otcSeniorManager.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind30)));
            this.otcSeniorManager.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.AfterPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind29)));
            // 
            // caSendSeniorManagerTaskCreatedMail
            // 
            this.caSendSeniorManagerTaskCreatedMail.Name = "caSendSeniorManagerTaskCreatedMail";
            this.caSendSeniorManagerTaskCreatedMail.ExecuteCode += new System.EventHandler(this.caSendSeniorManagerTaskCreatedMail_ExecuteCode);
            // 
            // lthSeniorManagerTaskCreated
            // 
            this.lthSeniorManagerTaskCreated.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.lthSeniorManagerTaskCreated.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind31.Name = "HRLearningManagementRequestWF";
            activitybind31.Path = "lthSeniorManagerTaskCreated_HistoryDescription";
            activitybind32.Name = "HRLearningManagementRequestWF";
            activitybind32.Path = "lthSeniorManagerTaskCreated_HistoryOutcome";
            this.lthSeniorManagerTaskCreated.Name = "lthSeniorManagerTaskCreated";
            this.lthSeniorManagerTaskCreated.OtherData = "";
            this.lthSeniorManagerTaskCreated.UserId = -1;
            this.lthSeniorManagerTaskCreated.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind31)));
            this.lthSeniorManagerTaskCreated.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind32)));
            // 
            // ifElseActivity6
            // 
            this.ifElseActivity6.Activities.Add(this.SeniorManagerFirstApproval);
            this.ifElseActivity6.Activities.Add(this.SeniorManagerFinalApproval);
            this.ifElseActivity6.Name = "ifElseActivity6";
            // 
            // ctSeniorManager
            // 
            correlationtoken22.Name = "SeniorManagerToken";
            correlationtoken22.OwnerActivityName = "saSeniorManager";
            this.ctSeniorManager.CorrelationToken = correlationtoken22;
            activitybind33.Name = "HRLearningManagementRequestWF";
            activitybind33.Path = "ctSeniorManager_ListItemId";
            this.ctSeniorManager.Name = "ctSeniorManager";
            activitybind34.Name = "HRLearningManagementRequestWF";
            activitybind34.Path = "ctSeniorManager_SpecialPermissions";
            activitybind35.Name = "HRLearningManagementRequestWF";
            activitybind35.Path = "ctSeniorManager_TaskId";
            activitybind36.Name = "HRLearningManagementRequestWF";
            activitybind36.Path = "ctSeniorManager_TaskProperties";
            this.ctSeniorManager.MethodInvoking += new System.EventHandler(this.ctSeniorManager_MethodInvoking);
            this.ctSeniorManager.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind36)));
            this.ctSeniorManager.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind35)));
            this.ctSeniorManager.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.ListItemIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind33)));
            this.ctSeniorManager.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.SpecialPermissionsProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind34)));
            // 
            // ifElseActivity1
            // 
            this.ifElseActivity1.Activities.Add(this.LineManagerApproved);
            this.ifElseActivity1.Activities.Add(this.LineManagerRejected);
            this.ifElseActivity1.Activities.Add(this.LineManagerRequestChange);
            this.ifElseActivity1.Name = "ifElseActivity1";
            // 
            // dtLineManager
            // 
            correlationtoken23.Name = "LineManagerToken";
            correlationtoken23.OwnerActivityName = "saLineManager";
            this.dtLineManager.CorrelationToken = correlationtoken23;
            this.dtLineManager.Name = "dtLineManager";
            activitybind37.Name = "HRLearningManagementRequestWF";
            activitybind37.Path = "ctLineManager_TaskId";
            this.dtLineManager.MethodInvoking += new System.EventHandler(this.dtLineManager_MethodInvoking);
            this.dtLineManager.SetBinding(Microsoft.SharePoint.WorkflowActions.DeleteTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind37)));
            // 
            // lthAfterLineManagerAction
            // 
            this.lthAfterLineManagerAction.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.lthAfterLineManagerAction.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind38.Name = "HRLearningManagementRequestWF";
            activitybind38.Path = "lthAfterLineManagerAction_HistoryDescription";
            activitybind39.Name = "HRLearningManagementRequestWF";
            activitybind39.Path = "lthAfterLineManagerAction_HistoryOutcome";
            this.lthAfterLineManagerAction.Name = "lthAfterLineManagerAction";
            this.lthAfterLineManagerAction.OtherData = "";
            activitybind40.Name = "HRLearningManagementRequestWF";
            activitybind40.Path = "lthAfterLineManagerAction_UserId";
            this.lthAfterLineManagerAction.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind38)));
            this.lthAfterLineManagerAction.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind39)));
            this.lthAfterLineManagerAction.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.UserIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind40)));
            // 
            // otcLineManager
            // 
            activitybind41.Name = "HRLearningManagementRequestWF";
            activitybind41.Path = "otcLineManager_AfterProperties";
            this.otcLineManager.BeforeProperties = null;
            correlationtoken24.Name = "LineManagerToken";
            correlationtoken24.OwnerActivityName = "saLineManager";
            this.otcLineManager.CorrelationToken = correlationtoken24;
            this.otcLineManager.Executor = null;
            this.otcLineManager.Name = "otcLineManager";
            activitybind42.Name = "HRLearningManagementRequestWF";
            activitybind42.Path = "ctLineManager_TaskId";
            this.otcLineManager.Invoked += new System.EventHandler<System.Workflow.Activities.ExternalDataEventArgs>(this.otcLineManager_Invoked);
            this.otcLineManager.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind42)));
            this.otcLineManager.SetBinding(Microsoft.SharePoint.WorkflowActions.OnTaskChanged.AfterPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind41)));
            // 
            // caSendLineManagerTaskCreatedMail
            // 
            this.caSendLineManagerTaskCreatedMail.Name = "caSendLineManagerTaskCreatedMail";
            this.caSendLineManagerTaskCreatedMail.ExecuteCode += new System.EventHandler(this.caSendLineManagerTaskCreatedMail_ExecuteCode);
            // 
            // lthLineManagerTaskCreated
            // 
            this.lthLineManagerTaskCreated.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.lthLineManagerTaskCreated.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            activitybind43.Name = "HRLearningManagementRequestWF";
            activitybind43.Path = "lthLineManagerTaskCreated_HistoryDescription";
            activitybind44.Name = "HRLearningManagementRequestWF";
            activitybind44.Path = "lthLineManagerTaskCreated_HistoryOutcome";
            this.lthLineManagerTaskCreated.Name = "lthLineManagerTaskCreated";
            this.lthLineManagerTaskCreated.OtherData = "";
            this.lthLineManagerTaskCreated.UserId = -1;
            this.lthLineManagerTaskCreated.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind43)));
            this.lthLineManagerTaskCreated.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind44)));
            // 
            // ifElseActivity5
            // 
            this.ifElseActivity5.Activities.Add(this.LineManagerFirstApproval);
            this.ifElseActivity5.Activities.Add(this.LineManagerFinalApproval);
            this.ifElseActivity5.Name = "ifElseActivity5";
            // 
            // ctLineManager
            // 
            correlationtoken25.Name = "LineManagerToken";
            correlationtoken25.OwnerActivityName = "saLineManager";
            this.ctLineManager.CorrelationToken = correlationtoken25;
            activitybind45.Name = "HRLearningManagementRequestWF";
            activitybind45.Path = "ctLineManager_ListItemId";
            this.ctLineManager.Name = "ctLineManager";
            activitybind46.Name = "HRLearningManagementRequestWF";
            activitybind46.Path = "ctLineManager_SpecialPermissions";
            activitybind47.Name = "HRLearningManagementRequestWF";
            activitybind47.Path = "ctLineManager_TaskId";
            activitybind48.Name = "HRLearningManagementRequestWF";
            activitybind48.Path = "ctLineManager_TaskProperties";
            this.ctLineManager.MethodInvoking += new System.EventHandler(this.ctLineManager_MethodInvoking);
            this.ctLineManager.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind48)));
            this.ctLineManager.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.TaskIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind47)));
            this.ctLineManager.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.ListItemIdProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind45)));
            this.ctLineManager.SetBinding(Microsoft.SharePoint.WorkflowActions.CreateTask.SpecialPermissionsProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind46)));
            // 
            // ssaToLineManager
            // 
            this.ssaToLineManager.Name = "ssaToLineManager";
            this.ssaToLineManager.TargetStateName = "saLineManager";
            // 
            // onWorkflowActivated1
            // 
            correlationtoken26.Name = "workflowToken";
            correlationtoken26.OwnerActivityName = "HRLearningManagementRequestWF";
            this.onWorkflowActivated1.CorrelationToken = correlationtoken26;
            this.onWorkflowActivated1.EventName = "OnWorkflowActivated";
            this.onWorkflowActivated1.Name = "onWorkflowActivated1";
            activitybind49.Name = "HRLearningManagementRequestWF";
            activitybind49.Path = "workflowProperties";
            this.onWorkflowActivated1.Invoked += new System.EventHandler<System.Workflow.Activities.ExternalDataEventArgs>(this.onWorkflowActivated1_Invoked);
            this.onWorkflowActivated1.SetBinding(Microsoft.SharePoint.WorkflowActions.OnWorkflowActivated.WorkflowPropertiesProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind49)));
            // 
            // edaLearningManagementTeam
            // 
            this.edaLearningManagementTeam.Activities.Add(this.otcLearningManagementTeam);
            this.edaLearningManagementTeam.Activities.Add(this.lthAfterLearningManagementTeamAction);
            this.edaLearningManagementTeam.Activities.Add(this.dtLearningManagementTeam);
            this.edaLearningManagementTeam.Activities.Add(this.ifElseActivity4);
            this.edaLearningManagementTeam.Name = "edaLearningManagementTeam";
            // 
            // siaLearningManagementTeam
            // 
            this.siaLearningManagementTeam.Activities.Add(this.ctLearningManagementTeam);
            this.siaLearningManagementTeam.Activities.Add(this.ifElseActivity8);
            this.siaLearningManagementTeam.Activities.Add(this.lthLearningManagementTeamTaskCreated);
            this.siaLearningManagementTeam.Activities.Add(this.caSendLearningManagementTeamTaskCreatedMail);
            this.siaLearningManagementTeam.Name = "siaLearningManagementTeam";
            // 
            // edaGeneralManager
            // 
            this.edaGeneralManager.Activities.Add(this.otcGeneralManager);
            this.edaGeneralManager.Activities.Add(this.lthAfterGeneralManagerAction);
            this.edaGeneralManager.Activities.Add(this.dtGeneralManager);
            this.edaGeneralManager.Activities.Add(this.ifElseActivity3);
            this.edaGeneralManager.Name = "edaGeneralManager";
            // 
            // siaGeneralManager
            // 
            this.siaGeneralManager.Activities.Add(this.ctGeneralManager);
            this.siaGeneralManager.Activities.Add(this.ifElseActivity7);
            this.siaGeneralManager.Activities.Add(this.lthGeneralManagerTaskCreated);
            this.siaGeneralManager.Activities.Add(this.caSendGeneralManagerTaskCreatedMail);
            this.siaGeneralManager.Name = "siaGeneralManager";
            // 
            // edaSeniorManager
            // 
            this.edaSeniorManager.Activities.Add(this.otcSeniorManager);
            this.edaSeniorManager.Activities.Add(this.lthAfterSeniorManagerAction);
            this.edaSeniorManager.Activities.Add(this.dtSeniorManager);
            this.edaSeniorManager.Activities.Add(this.ifElseActivity2);
            this.edaSeniorManager.Name = "edaSeniorManager";
            // 
            // siaSeniorManager
            // 
            this.siaSeniorManager.Activities.Add(this.ctSeniorManager);
            this.siaSeniorManager.Activities.Add(this.ifElseActivity6);
            this.siaSeniorManager.Activities.Add(this.lthSeniorManagerTaskCreated);
            this.siaSeniorManager.Activities.Add(this.caSendSeniorManagerTaskCreatedMail);
            this.siaSeniorManager.Name = "siaSeniorManager";
            // 
            // edaLineManager
            // 
            this.edaLineManager.Activities.Add(this.otcLineManager);
            this.edaLineManager.Activities.Add(this.lthAfterLineManagerAction);
            this.edaLineManager.Activities.Add(this.dtLineManager);
            this.edaLineManager.Activities.Add(this.ifElseActivity1);
            this.edaLineManager.Name = "edaLineManager";
            // 
            // siaLineManager
            // 
            this.siaLineManager.Activities.Add(this.ctLineManager);
            this.siaLineManager.Activities.Add(this.ifElseActivity5);
            this.siaLineManager.Activities.Add(this.lthLineManagerTaskCreated);
            this.siaLineManager.Activities.Add(this.caSendLineManagerTaskCreatedMail);
            this.siaLineManager.Name = "siaLineManager";
            // 
            // eventDrivenActivity1
            // 
            this.eventDrivenActivity1.Activities.Add(this.onWorkflowActivated1);
            this.eventDrivenActivity1.Activities.Add(this.ssaToLineManager);
            this.eventDrivenActivity1.Name = "eventDrivenActivity1";
            // 
            // saCompleteState
            // 
            this.saCompleteState.Name = "saCompleteState";
            // 
            // saLearningManagementTeam
            // 
            this.saLearningManagementTeam.Activities.Add(this.siaLearningManagementTeam);
            this.saLearningManagementTeam.Activities.Add(this.edaLearningManagementTeam);
            this.saLearningManagementTeam.Name = "saLearningManagementTeam";
            // 
            // saGeneralManager
            // 
            this.saGeneralManager.Activities.Add(this.siaGeneralManager);
            this.saGeneralManager.Activities.Add(this.edaGeneralManager);
            this.saGeneralManager.Name = "saGeneralManager";
            // 
            // saSeniorManager
            // 
            this.saSeniorManager.Activities.Add(this.siaSeniorManager);
            this.saSeniorManager.Activities.Add(this.edaSeniorManager);
            this.saSeniorManager.Name = "saSeniorManager";
            // 
            // saLineManager
            // 
            this.saLineManager.Activities.Add(this.siaLineManager);
            this.saLineManager.Activities.Add(this.edaLineManager);
            this.saLineManager.Name = "saLineManager";
            // 
            // HRLearningManagementRequestWFInitialState
            // 
            this.HRLearningManagementRequestWFInitialState.Activities.Add(this.eventDrivenActivity1);
            this.HRLearningManagementRequestWFInitialState.Name = "HRLearningManagementRequestWFInitialState";
            // 
            // HRLearningManagementRequestWF
            // 
            this.Activities.Add(this.HRLearningManagementRequestWFInitialState);
            this.Activities.Add(this.saLineManager);
            this.Activities.Add(this.saSeniorManager);
            this.Activities.Add(this.saGeneralManager);
            this.Activities.Add(this.saLearningManagementTeam);
            this.Activities.Add(this.saCompleteState);
            this.CompletedStateName = "saCompleteState";
            this.DynamicUpdateCondition = null;
            this.InitialStateName = "HRLearningManagementRequestWFInitialState";
            this.Name = "HRLearningManagementRequestWF";
            this.CanModifyActivities = false;

        }

        #endregion

        private SetStateActivity ssaLearningManagementTeamToCompleteState;
        private Microsoft.SharePoint.WorkflowActions.SetState ssLearningManagementTeamCanceledRequest;
        private SetStateActivity ssaLearningManagementTeamToLineManager;
        private Microsoft.SharePoint.WorkflowActions.SetState ssPendingLearningManagementTeamUpdate;
        private Microsoft.SharePoint.WorkflowActions.SetState ssPendingLearningManagementTeamReportUpload;
        private SetStateActivity ssaGeneralManagerToRequester;
        private SetStateActivity ssaGeneralManagerRejected;
        private Microsoft.SharePoint.WorkflowActions.SetState ssGeneralManagerRejected;
        private SetStateActivity ssaGeneralManagerFinallyApproved;
        private Microsoft.SharePoint.WorkflowActions.SetState ssRequisitionFinallyApproved;
        private SetStateActivity ssaGeneralManagerApproved;
        private Microsoft.SharePoint.WorkflowActions.SetState ssPendingGeneralManagerFinalApproval;
        private Microsoft.SharePoint.WorkflowActions.SetState ssPendingGeneralManagerApproval;
        private IfElseBranchActivity LearningManagementTeamCanceledRequest;
        private IfElseBranchActivity LearningManagementTeamUpdatedRequest;
        private IfElseBranchActivity LearningManagementTeamUpdate;
        private IfElseBranchActivity LearningManagementTeamReportUpload;
        private IfElseBranchActivity GeneralManagerRequestChange;
        private IfElseBranchActivity GeneralManagerRejected;
        private IfElseBranchActivity GeneralManagerFinallyApproved;
        private IfElseBranchActivity GeneralManagerApproved;
        private IfElseBranchActivity GeneralManagerFinalApproval;
        private IfElseBranchActivity GeneralManagerFirstApproval;
        private IfElseActivity ifElseActivity4;
        private Microsoft.SharePoint.WorkflowActions.DeleteTask dtLearningManagementTeam;
        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity lthAfterLearningManagementTeamAction;
        private Microsoft.SharePoint.WorkflowActions.OnTaskChanged otcLearningManagementTeam;
        private CodeActivity caSendLearningManagementTeamTaskCreatedMail;
        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity lthLearningManagementTeamTaskCreated;
        private IfElseActivity ifElseActivity8;
        private Microsoft.SharePoint.WorkflowActions.CreateTask ctLearningManagementTeam;
        private IfElseActivity ifElseActivity3;
        private Microsoft.SharePoint.WorkflowActions.DeleteTask dtGeneralManager;
        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity lthAfterGeneralManagerAction;
        private Microsoft.SharePoint.WorkflowActions.OnTaskChanged otcGeneralManager;
        private CodeActivity caSendGeneralManagerTaskCreatedMail;
        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity lthGeneralManagerTaskCreated;
        private IfElseActivity ifElseActivity7;
        private Microsoft.SharePoint.WorkflowActions.CreateTask ctGeneralManager;
        private SetStateActivity ssaToLineManager;
        private Microsoft.SharePoint.WorkflowActions.OnWorkflowActivated onWorkflowActivated1;
        private EventDrivenActivity edaLearningManagementTeam;
        private StateInitializationActivity siaLearningManagementTeam;
        private EventDrivenActivity edaGeneralManager;
        private StateInitializationActivity siaGeneralManager;
        private EventDrivenActivity eventDrivenActivity1;
        private StateActivity saCompleteState;
        private StateActivity saLearningManagementTeam;
        private StateActivity saGeneralManager;
        private SetStateActivity ssaSeniorManagerToRequester;
        private SetStateActivity ssaSeniorManagerRejected;
        private Microsoft.SharePoint.WorkflowActions.SetState ssSeniorManagerRejected;
        private SetStateActivity ssaSeniorManagerToGeneralManager;
        private Microsoft.SharePoint.WorkflowActions.SetState ssPendingSeniorManagerFinalApproval;
        private Microsoft.SharePoint.WorkflowActions.SetState ssPendingSeniorManagerApproval;
        private IfElseBranchActivity SeniorManagerRequestChange;
        private IfElseBranchActivity SeniorManagerRejected;
        private IfElseBranchActivity SeniorManagerApproved;
        private IfElseBranchActivity SeniorManagerFinalApproval;
        private IfElseBranchActivity SeniorManagerFirstApproval;
        private IfElseActivity ifElseActivity2;
        private Microsoft.SharePoint.WorkflowActions.DeleteTask dtSeniorManager;
        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity lthAfterSeniorManagerAction;
        private Microsoft.SharePoint.WorkflowActions.OnTaskChanged otcSeniorManager;
        private CodeActivity caSendSeniorManagerTaskCreatedMail;
        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity lthSeniorManagerTaskCreated;
        private IfElseActivity ifElseActivity6;
        private Microsoft.SharePoint.WorkflowActions.CreateTask ctSeniorManager;
        private EventDrivenActivity edaSeniorManager;
        private StateInitializationActivity siaSeniorManager;
        private StateActivity saSeniorManager;
        private SetStateActivity ssaLineManagerToRequester;
        private SetStateActivity ssaLineManagerRejected;
        private Microsoft.SharePoint.WorkflowActions.SetState ssLineManagerRejected;
        private SetStateActivity ssaLineManagerToSeniorManager;
        private Microsoft.SharePoint.WorkflowActions.SetState ssPendingLineManagerFinalApproval;
        private Microsoft.SharePoint.WorkflowActions.SetState ssPendingLineManagerApproval;
        private IfElseBranchActivity LineManagerRequestChange;
        private IfElseBranchActivity LineManagerRejected;
        private IfElseBranchActivity LineManagerApproved;
        private IfElseBranchActivity LineManagerFinalApproval;
        private IfElseBranchActivity LineManagerFirstApproval;
        private IfElseActivity ifElseActivity1;
        private Microsoft.SharePoint.WorkflowActions.DeleteTask dtLineManager;
        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity lthAfterLineManagerAction;
        private Microsoft.SharePoint.WorkflowActions.OnTaskChanged otcLineManager;
        private CodeActivity caSendLineManagerTaskCreatedMail;
        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity lthLineManagerTaskCreated;
        private IfElseActivity ifElseActivity5;
        private Microsoft.SharePoint.WorkflowActions.CreateTask ctLineManager;
        private EventDrivenActivity edaLineManager;
        private StateInitializationActivity siaLineManager;
        private StateActivity saLineManager;
        private StateActivity HRLearningManagementRequestWFInitialState;
    }
}
