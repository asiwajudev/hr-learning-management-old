﻿using System;
using System.Data;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using HR_learning_And_Mgmt.Code;

namespace HR_learning_And_Mgmt.Layouts.HR_learning_And_Mgmt
{
	public partial class NewForm : LayoutsPageBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LoadProvider();

                #region DATE REGULATION
                //Set dates according to requirements
                dtcProgrammeDate.MinDate = DateTime.Now;
                dtcProgrammeContent.MinDate = DateTime.Now;
                dtcCaseStudy.MinDate = DateTime.Now;
                dtcActivities.MinDate = DateTime.Now;
                dtcBooking.MinDate = DateTime.Now;
                dtcTrainingMaterials.MinDate = DateTime.Now;
                dtcDelegateAvailabilitySent.MinDate = DateTime.Now;
                dtcInvitesSent.MinDate = DateTime.Now;
                dtcDelegatePreWork.MinDate = DateTime.Now;
                dtcCustomizedTrainingKP1EvaluationForm.MinDate = DateTime.Now;
                dtcCompletedTrainingEvaluationForms.MinDate = DateTime.Now;
                dtcEvaluationReport.MinDate = DateTime.Now;
                dtcITFForm6A.MinDate = DateTime.Now.AddDays(21);
                dtcInvitationLetterForVisaToFacilitator.MinDate = DateTime.Now.AddMonths(1);
                dtcProtocolOrTransportArrangements.MinDate = DateTime.Now.AddMonths(1);
                dtcAccomodationArrangementsForFacilitator.MinDate = DateTime.Now.AddMonths(1);
                dtcFacilitatorKit.MinDate = DateTime.Now.AddDays(21);
                dtcDelegateAvailabilityToLineManagers.MinDate = DateTime.Now.AddDays(21);
                dtcRetentionContract.MinDate = DateTime.Now.AddDays(14);
                dtcTrainingInvitation.MinDate = DateTime.Now.AddDays(14);
                dtcTravelRequisitions.MinDate = DateTime.Now.AddDays(14);
                dtcVenuePreparation.MinDate = DateTime.Now.AddDays(21);
                dtcHall.MinDate = DateTime.Now.AddDays(21);
                dtcProjector.MinDate = DateTime.Now.AddDays(21);
                dtcHallArrangement.MinDate = DateTime.Now.AddDays(21);
                dtcLocation.MinDate = DateTime.Now.AddDays(21);
                dtcTeaBreakAndLunch.MinDate = DateTime.Now.AddDays(21);
                dtcSeatingArrangement.MinDate = DateTime.Now.AddDays(21);
                dtcLocationBranding.MinDate = DateTime.Now.AddHours(24);
                dtcProgrammeMaterials.MinDate = DateTime.Now.AddMonths(1);
                dtcWallCharts.MinDate = DateTime.Now.AddMonths(1);
                dtcVidoesOrDVD.MinDate = DateTime.Now.AddMonths(1);
                dtcParticipantGuide.MinDate = DateTime.Now.AddMonths(1);
                dtcPresentationSlides.MinDate = DateTime.Now.AddMonths(1);
                dtcLearningAidsActivityCardsOrCaseStudies.MinDate = DateTime.Now.AddMonths(1);
                dtcStationaries.MinDate = DateTime.Now.AddDays(14);
                dtcExternalSpeakers.MinDate = DateTime.Now.AddHours(48);
                dtcPreCourse.MinDate = DateTime.Now.AddDays(14);
                dtcKP1Evaluation.MinDate = DateTime.Now.AddMonths(1);
                dtcCertificates.MinDate = DateTime.Now.AddHours(48);
                dtcAttendanceList.MinDate = DateTime.Now.AddHours(24);
                dtcPhotographs.MinDate = DateTime.Now.AddHours(48);
                dtcPostAssessment.MinDate = DateTime.Now;
                #endregion 

                spanLineManager.Visible = false;
                spanSeniorManager.Visible = false;
                spanGeneralManager.Visible = false;
                spanProgrammeCoordinator.Visible = false;
			}
		}

		protected void _btnCancel_Click(object sender, EventArgs e)
		{
			Response.Redirect(Web.Url + "/_layouts/HR_learning_And_Mgmt/NewForm.aspx");
		}

		private void LoadProvider()
		{
			ddlProvider.DataSource = this.Web.Lists["Providers"].Items.GetDataTable();
			ddlProvider.DataValueField = "Title";
			ddlProvider.DataTextField = "Title";
			ddlProvider.DataBind();
			ddlProvider.Items.Insert(0, new ListItem("", ""));
		}

		protected void _btnOK_Click(object sender, EventArgs e)
		{
			string faculty = string.Empty;
			foreach (ListItem item in this.cblFaculty.Items)
			{
				if (item.Selected)
				{
					faculty += item + ",";
				}
			}

			string equipment = string.Empty;
			foreach (ListItem item in this.cblEquipmentsRequired.Items)
			{
				if (item.Selected)
				{
					equipment += item + ",";
				}
			}

			try
			{
				using (SPSite site = new SPSite(SPContext.Current.Web.Url))
				{
					using (SPWeb web = site.OpenWeb())
					{
                        SPFieldUserValue checkLineManager = GetPeopleFromPickerControl(peLineManager, web);
                        SPFieldUserValue checkSeniorManager = GetPeopleFromPickerControl(peSeniorManager, web);
                        SPFieldUserValue checkGeneralManager = GetPeopleFromPickerControl(peGeneralManager, web);
                        SPFieldUserValue checkProgrammeCoordinator = GetPeopleFromPickerControl(peProgrammeCoordinator, web);
                        if (checkLineManager != null && checkSeniorManager != null && checkGeneralManager != null && checkProgrammeCoordinator != null)
                        {
                            web.AllowUnsafeUpdates = true;
                            SPList list = web.Lists["HR_Learning_And_Mgmt_List"];
                            SPListItem item = list.Items.Add();
                            item["Title"] = "HR_learning_And_Mgmt" + DateTime.Now;
                            //item["User name"] = SPContext.Current.Web.CurrentUser.Name;
                            item["Programme name"] = txtProgrammeName.Text;
                            item["Programme venue"] = txtProgrammeVenue.Text;
                            item["Line manager"] = Helper.GetUserValue(peLineManager, web);
                            item["Senior manager"] = Helper.GetUserValue(peSeniorManager, web);
                            item["General manager"] = Helper.GetUserValue(peGeneralManager, web);
                            item["Programme date"] = Convert.ToDateTime(dtcProgrammeDate.SelectedDate.ToString());
                            item["Provider"] = ddlProvider.SelectedItem.Text;
                            item["Is it mtn academy"] = rblIsMTNAcademyProgramme.SelectedItem.Value;
                            item["Programme coordinator"] = Helper.GetUserValue(peProgrammeCoordinator, web);
                            item["Programme facilitator"] = txtProgrammeFacilitator.Text;
                            item["Faculty"] = faculty;
                            item["Facilitator last programme and date"] = txtFacilitatorLastProgrammeAndDate.Text;
                            item["Facilitator last evaluation score"] = txtFacilitatorLastEvaluationScore.Text;
                            item["Venue arrangement"] = rblVenueArrangement.SelectedItem.Value;
                            item["Equipments required"] = equipment;
                            item["Programme coordination checklist"] = rblProgrammeCoordinationChecklist.SelectedItem.Text;
                            item["Programme content"] = Convert.ToDateTime(dtcProgrammeContent.SelectedDate.ToString());
                            item["Case study"] = Convert.ToDateTime(dtcCaseStudy.SelectedDate.ToString());
                            item["Activities"] = Convert.ToDateTime(dtcActivities.SelectedDate.ToString());
                            item["Venue booking"] = Convert.ToDateTime(dtcBooking.SelectedDate.ToString());
                            item["Training materials"] = Convert.ToDateTime(dtcTrainingMaterials.SelectedDate.ToString());
                            item["Delegate availabilty sent"] = Convert.ToDateTime(dtcDelegateAvailabilitySent.SelectedDate.ToString());
                            item["Invites sent"] = Convert.ToDateTime(dtcInvitesSent.SelectedDate.ToString());
                            item["Delegate pre-work"] = Convert.ToDateTime(dtcDelegatePreWork.SelectedDate.ToString());
                            item["Customized training kp1"] = Convert.ToDateTime(dtcCustomizedTrainingKP1EvaluationForm.SelectedDate.ToString());
                            item["Itf form 6a"] = Convert.ToDateTime(dtcITFForm6A.SelectedDate.ToString());
                            item["Facilitators kit"] = Convert.ToDateTime(dtcFacilitatorKit.SelectedDate.ToString());
                            item["Delegate available to line manager"] = Convert.ToDateTime(dtcDelegateAvailabilityToLineManagers.SelectedDate.ToString());
                            item["Training invitation"] = Convert.ToDateTime(dtcTrainingInvitation.SelectedDate.ToString());
                            item["Venue preparation"] = Convert.ToDateTime(dtcVenuePreparation.SelectedDate.ToString());
                            item["Hall"] = Convert.ToDateTime(dtcHall.SelectedDate.ToString());
                            item["Projector"] = Convert.ToDateTime(dtcProjector.SelectedDate.ToString());
                            item["Hall arrangement"] = Convert.ToDateTime(dtcHallArrangement.SelectedDate.ToString());
                            item["Location"] = Convert.ToDateTime(dtcLocation.SelectedDate.ToString());
                            item["Tea break and lunch"] = Convert.ToDateTime(dtcTeaBreakAndLunch.SelectedDate.ToString());
                            item["Seating arrangement"] = Convert.ToDateTime(dtcSeatingArrangement.SelectedDate.ToString());
                            item["Location branding"] = Convert.ToDateTime(dtcLocationBranding.SelectedDate.ToString());
                            item["Programme materials"] = Convert.ToDateTime(dtcProgrammeMaterials.SelectedDate.ToString());
                            item["Wall charts"] = Convert.ToDateTime(dtcWallCharts.SelectedDate.ToString());
                            item["Videos/dvd"] = Convert.ToDateTime(dtcVidoesOrDVD.SelectedDate.ToString());
                            item["Participant guide"] = Convert.ToDateTime(dtcParticipantGuide.SelectedDate.ToString());
                            item["Presentation slides"] = Convert.ToDateTime(dtcPresentationSlides.SelectedDate.ToString());
                            item["Learning aids"] = Convert.ToDateTime(dtcLearningAidsActivityCardsOrCaseStudies.SelectedDate.ToString());
                            item["Stationaries"] = Convert.ToDateTime(dtcStationaries.SelectedDate.ToString());
                            item["External speakers"] = Convert.ToDateTime(dtcExternalSpeakers.SelectedDate.ToString());
                            item["Pre-course"] = Convert.ToDateTime(dtcPreCourse.SelectedDate.ToString());
                            item["Kp1 evaluation"] = Convert.ToDateTime(dtcKP1Evaluation.SelectedDate.ToString());
                            item["Certificates"] = Convert.ToDateTime(dtcCertificates.SelectedDate.ToString());
                            item["Attendance list"] = Convert.ToDateTime(dtcAttendanceList.SelectedDate.ToString());
                            item["Photographs"] = Convert.ToDateTime(dtcPhotographs.SelectedDate.ToString());
                            item["Post assessment"] = Convert.ToDateTime(dtcPostAssessment.SelectedDate.ToString());
                            item.Update();

                            //string URL = SPContext.Current.Web.Url + "/_layouts/HR_learning_And_Mgmt/NewForm.aspx";
                            //MessagePrompt.ShowMessage(this, "Request Submitted Successfully!!!", URL);


                            //Response.Redirect(SPContext.Current.Web.Url + "/_layouts/HR_learning_And_Mgmt/NewForm.aspx");

                            //Response.Redirect("
                            //https
                            //://sharepoint.mtnnigeria.net/sites/HumanResources/Pages/Human-Resource.aspx#/playlist1/local1
                            //");
                            web.AllowUnsafeUpdates = false;
                            Response.Redirect(SPContext.Current.Web.Url);
                        }
                        else
                        {
                            spanLineManager.Visible = true;
                            spanSeniorManager.Visible = true;
                            spanGeneralManager.Visible = true;
                            spanProgrammeCoordinator.Visible = false;
                        }
					}
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.LogToWindows(ex, "HR_Learning_and_Management, NewRequest");
			}

		}

        public static SPFieldUserValue GetPeopleFromPickerControl(PeopleEditor people, SPWeb web)
        {
            SPFieldUserValue value = null;
            if (people.ResolvedEntities.Count > 0)
            {
                for (int i = 0; i < people.ResolvedEntities.Count; i++)
                {
                    try
                    {
                        PickerEntity user = (PickerEntity)people.ResolvedEntities[i];

                        switch ((string)user.EntityData["PrincipalType"])
                        {
                            case "User":
                                SPUser webUser = web.EnsureUser(user.Key);
                                value = new SPFieldUserValue(web, webUser.ID, webUser.Name);
                                break;

                            case "SharePointGroup":
                                SPGroup siteGroup = web.SiteGroups[user.EntityData["AccountName"].ToString()];
                                value = new SPFieldUserValue(web, siteGroup.ID, siteGroup.Name);
                                break;
                            default:
                                SPUser spUser = web.EnsureUser(people.Accounts[i].ToString());
                                value = new SPFieldUserValue(web, spUser.ID, spUser.Name);
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        // log or do something
                        SPUser spUser = web.EnsureUser(people.Accounts[i].ToString());
                        value = new SPFieldUserValue(web, spUser.ID, spUser.Name);
                    }
                }
            }
            return value;
        }



    }
}
