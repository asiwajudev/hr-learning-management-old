﻿using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Workflow;
using Microsoft.SharePoint.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;

namespace HR_learning_And_Mgmt.Layouts.HR_learning_And_Mgmt
{
	public partial class HRLearningManagementRequestWFTask : LayoutsPageBase
	{
		#region Private Variable
		SPList _TaskList;
		SPListItem _TaskListItem;
		SPList _RequestsList;
		SPListItem _RequestItem;
		string _DisplayFormMode = "";
		static Hashtable _TaskExtendedProperties;
		int _ItemID = 0;
		string _ListTitle = "";
		string _AllComments = "";
		bool isFirstTime = false;
		bool isRequestChange = false;
		#endregion

		//protected void Page_PreInit(object sender, EventArgs e)
		//{
		//	//Page.MasterPageFile = SPContext.Current.Web.ServerRelativeUrl + "/_catalogs/masterpage/MTNNERecruitmentMasterPage.master";
		//	Page.MasterPageFile = SPContext.Current.Web.ServerRelativeUrl + "/_catalogs/masterpage/default.master";
		//}

		//protected override void OnInit(EventArgs e)
		//{
		//	base.OnInit(e);
		//}

		protected void Page_Load(object sender, EventArgs e)
		{
			bool isValid = true;
			try
			{
				if (!IsPostBack)
				{
					if (base.Request.Params["ID"] != null)
					{
						SPWeb CurrentWeb = SPContext.Current.Web;
						_TaskList = CurrentWeb.Lists["HrLearningManagementRequestTasks"];
						_TaskListItem = _TaskList.GetItemById(int.Parse(base.Request.Params["ID"]));
						_TaskExtendedProperties = SPWorkflowTask.GetExtendedPropertiesAsHashtable((SPListItem)_TaskListItem);
						_DisplayFormMode = _TaskExtendedProperties["DisplayForm"].ToString();
						ViewState["DisplayFormMode"] = _DisplayFormMode;
						_ItemID = int.Parse(_TaskExtendedProperties["ItemID"].ToString());
						_ListTitle = _TaskExtendedProperties["ListTitle"].ToString();
						_AllComments = _TaskExtendedProperties["Comments"].ToString();
						ltlCommentsHistory.Text = _AllComments;
						isFirstTime = bool.Parse(_TaskExtendedProperties["isFirstTime"].ToString());

						if (_DisplayFormMode == "Requester")
							isRequestChange = bool.Parse(_TaskExtendedProperties["isRequestChange"].ToString());

						_RequestsList = SPContext.Current.Web.Lists[_ListTitle];
						_RequestItem = _RequestsList.GetItemById(_ItemID);

						SPFieldUserValueCollection RequesterUserValueCollection = new SPFieldUserValueCollection(CurrentWeb, _RequestItem["Author"].ToString());
						SPUser RequesterUser = RequesterUserValueCollection[0].User;

						SPFieldUserValueCollection LineManagerValueCollection = new SPFieldUserValueCollection(CurrentWeb, _RequestItem["Line manager"].ToString());
						SPUser LineManagerUser = LineManagerValueCollection[0].User;

						SPFieldUserValueCollection SeniorManagerValueCollection = new SPFieldUserValueCollection(CurrentWeb, _RequestItem["Senior manager"].ToString());
						SPUser SeniorManagerUser = SeniorManagerValueCollection[0].User;

						SPFieldUserValueCollection GeneralManagerValueCollection = new SPFieldUserValueCollection(CurrentWeb, _RequestItem["General manager"].ToString());
						SPUser GeneralManagerUser = GeneralManagerValueCollection[0].User;

						SPFieldUserValueCollection ProgrammeCoordinatorUserValueCollection = new SPFieldUserValueCollection(CurrentWeb, _RequestItem["Programme coordinator"].ToString());
						SPUser ProgrammeCoordinatorUser = ProgrammeCoordinatorUserValueCollection[0].User;

						//lblIsFirstTime.Text = _TaskExtendedProperties["isFirstTime"].ToString();
						lblProgrammeName.Text = _RequestItem["Programme name"].ToString();
						lblUserName.Text = RequesterUser.Name;
						lblEMail.Text = RequesterUser.Email;
						lblLineManager.Text = LineManagerUser.Name;
						lblSeniorManager.Text = SeniorManagerUser.Name;
						lblGeneralManager.Text = GeneralManagerUser.Name;

						if (_DisplayFormMode == "Requester" && isFirstTime)
						{
							GetProviders();
							//btnRequestChange.Visible = false;
							btnApprove.Text = "Submit";
							btnReject.Text = "Cancel Request";

							rowMessage.Visible = true;
							if (_TaskExtendedProperties["Rejector"].ToString() == HRLearningManagementRequestWFRecjector.LineManager.ToString())
								rowMessage.Cells[0].InnerText = "The Line Manager has requested change from you.";
							else if (_TaskExtendedProperties["Rejector"].ToString() == HRLearningManagementRequestWFRecjector.SeniorManager.ToString())
								rowMessage.Cells[0].InnerText = "The Senior Manager has requested change from you.";
							else if (_TaskExtendedProperties["Rejector"].ToString() == HRLearningManagementRequestWFRecjector.GeneralManager.ToString())
								rowMessage.Cells[0].InnerText = "The General Manager has requested change from you.";

							ShowRequesterControls();

							dtcProgrammeDate.SelectedDate = ((DateTime)_RequestItem["Programme date"]);
							txtProgrammeVenue.Text = _RequestItem["Programme venue"].ToString();
							ddlProvider.Text = _RequestItem["Provider"].ToString();
							rblIsMTNAcademyProgramme.Text = _RequestItem["Is it mtn academy"].ToString();
							//if (rblIsMTNAcademyProgramme.Text == "Others" && !String.IsNullOrEmpty((string)_RequestItem["Is MTN Programme Others"]))
							//{
							//	txtIsMTNProgrammeOthers.Text = _RequestItem["Is MTN Programme Others"].ToString();
							//	IsMTNAcademyProgrammeOthersRow.Style["display"] = "block";
							//}
							peProgrammeCoordinator.CommaSeparatedAccounts = ProgrammeCoordinatorUser.LoginName;
							txtProgrammeFacilitator.Text = _RequestItem["Programme facilitator"].ToString();

							string tempFaculty = _RequestItem["Faculty"].ToString();
							foreach (ListItem item in cblFaculty.Items)
								if (tempFaculty.Contains(item.Value))
									item.Selected = true;

							if (!String.IsNullOrEmpty((string)_RequestItem["Facilitator last programme and date"]))
								lblFacilitatorLastProgrammeAndDate.Text = _RequestItem["Facilitator last programme and date"].ToString();
							if (_RequestItem["Facilitator last evaluation score"] != null)
								lblFacilitatorLastEvaluationScore.Text = _RequestItem["Facilitator last evaluation score"].ToString();

							rblVenueArrangement.Text = _RequestItem["Venue arrangement"].ToString();

							//string tempEquipmentsRequired = _RequestItem["Equipments required"].ToString();
							//foreach (ListItem item in cblEquipmentsRequired.Items)
							//	if (tempEquipmentsRequired.Contains(item.Value))
							//		item.Selected = true;

							//if (!String.IsNullOrEmpty((string)_RequestItem["Others Equipments Required"]))
							//{
							//	txtOthersEquipmentsRequired.Text = _RequestItem["Others Equipments Required"].ToString();
							//	OthersEquipmentsRequiredRow.Style["display"] = "block";
							//}

							if (_RequestItem["Programme content"] != null)
								dtcProgrammeContent.SelectedDate = ((DateTime)_RequestItem["Programme content"]);
							if (_RequestItem["Case study"] != null)
								dtcCaseStudy.SelectedDate = ((DateTime)_RequestItem["Case study"]);
							if (_RequestItem["Activities"] != null)
								dtcActivities.SelectedDate = ((DateTime)_RequestItem["Activities"]);
							if (_RequestItem["Venue booking"] != null)
								dtcBooking.SelectedDate = ((DateTime)_RequestItem["Venue booking"]);
							if (_RequestItem["Training materials"] != null)
								dtcTrainingMaterials.SelectedDate = ((DateTime)_RequestItem["Training materials"]);
							if (_RequestItem["Delegate availabilty sent"] != null)
								dtcDelegateAvailabilitySent.SelectedDate = ((DateTime)_RequestItem["Delegate availabilty sent"]);
							if (_RequestItem["Invites sent"] != null)
								dtcInvitesSent.SelectedDate = ((DateTime)_RequestItem["Invites sent"]);
							if (_RequestItem["Delegate pre-work"] != null)
								dtcDelegatePreWork.SelectedDate = ((DateTime)_RequestItem["Delegate pre-work"]);
							if (_RequestItem["Completed training evaluation form"] != null)
								dtcCustomizedTrainingKP1EvaluationForm.SelectedDate = ((DateTime)_RequestItem["Completed training evaluation form"]);
							if (_RequestItem["Completed training evaluation form"] != null)
								dtcCompletedTrainingEvaluationForms.SelectedDate = ((DateTime)_RequestItem["Completed training evaluation form"]);
							if (_RequestItem["Evaluation report"] != null)
								dtcEvaluationReport.SelectedDate = ((DateTime)_RequestItem["Evaluation report"]);
							rblProgrammeCoordinationChecklist.Text = _RequestItem["Programme coordination checklist"].ToString();
							//if (rblProgrammeCoordinationChecklist.Text == "External")
							//{
							//	lblProgrammeCoordinationChecklist.Text = "External / Inplant / TTT";
							//	if (_RequestItem["Invitation Letter For Visa To Facilitator"] != null)
							//		dtcInvitationLetterForVisaToFacilitator.SelectedDate = ((DateTime)_RequestItem["Invitation Letter For Visa To Facilitator"]);
							//	if (_RequestItem["Protocol or Transport Arrangements"] != null)
							//		dtcProtocolOrTransportArrangements.SelectedDate = ((DateTime)_RequestItem["Protocol or Transport Arrangements"]);
							//	if (_RequestItem["Accomodation Arrangements For Facilitator"] != null)
							//		dtcAccomodationArrangementsForFacilitator.SelectedDate = ((DateTime)_RequestItem["Accomodation Arrangements For Facilitator"]);
							//	if (_RequestItem["Retention Contract"] != null)
							//		dtcRetentionContract.SelectedDate = ((DateTime)_RequestItem["Retention Contract"]);
							//	if (_RequestItem["Travel Requisitions"] != null)
							//		dtcTravelRequisitions.SelectedDate = ((DateTime)_RequestItem["Travel Requisitions"]);

							//	ExternalRow1.Style["display"] = "block";
							//	ExternalRow2.Style["display"] = "block";
							//	ExternalRow3.Style["display"] = "block";
							//	ExternalRow4.Style["display"] = "block";
							//	ExternalRow5.Style["display"] = "block";
							//}
							if (_RequestItem["Itf form 6a"] != null)
								dtcITFForm6A.SelectedDate = ((DateTime)_RequestItem["Itf form 6a"]);
							if (_RequestItem["Facilitators kit"] != null)
								dtcFacilitatorKit.SelectedDate = ((DateTime)_RequestItem["Facilitators kit"]);
							if (_RequestItem["Delegate available to line manager"] != null)
								dtcDelegateAvailabilityToLineManagers.SelectedDate = ((DateTime)_RequestItem["Delegate available to line manager"]);
							if (_RequestItem["Training invitation"] != null)
								dtcTrainingInvitation.SelectedDate = ((DateTime)_RequestItem["Training invitation"]);
							if (_RequestItem["Venue preparation"] != null)
								dtcVenuePreparation.SelectedDate = ((DateTime)_RequestItem["Venue preparation"]);
							if (_RequestItem["Hall"] != null)
								dtcHall.SelectedDate = ((DateTime)_RequestItem["Hall"]);
							if (_RequestItem["Projector"] != null)
								dtcProjector.SelectedDate = ((DateTime)_RequestItem["Projector"]);
							if (_RequestItem["Hall arrangement"] != null)
								dtcHallArrangement.SelectedDate = ((DateTime)_RequestItem["Hall arrangement"]);
							if (_RequestItem["Location"] != null)
								dtcLocation.SelectedDate = ((DateTime)_RequestItem["Location"]);
							if (_RequestItem["Tea break and lunch"] != null)
								dtcTeaBreakAndLunch.SelectedDate = ((DateTime)_RequestItem["Tea break and lunch"]);
							if (_RequestItem["Seating arrangement"] != null)
								dtcSeatingArrangement.SelectedDate = ((DateTime)_RequestItem["Seating arrangement"]);
							if (_RequestItem["Location branding"] != null)
								dtcLocationBranding.SelectedDate = ((DateTime)_RequestItem["Location branding"]);
							if (_RequestItem["Programme materials"] != null)
								dtcProgrammeMaterials.SelectedDate = ((DateTime)_RequestItem["Programme materials"]);
							if (_RequestItem["Wall charts"] != null)
								dtcWallCharts.SelectedDate = ((DateTime)_RequestItem["Wall charts"]);
							if (_RequestItem["Videos/dvd"] != null)
								dtcVidoesOrDVD.SelectedDate = ((DateTime)_RequestItem["Videos/dvd"]);
							if (_RequestItem["Participant guide"] != null)
								dtcParticipantGuide.SelectedDate = ((DateTime)_RequestItem["Participant guide"]);
							if (_RequestItem["Presentation slides"] != null)
								dtcPresentationSlides.SelectedDate = ((DateTime)_RequestItem["Presentation slides"]);
							if (_RequestItem["Learning aids"] != null)
								dtcLearningAidsActivityCardsOrCaseStudies.SelectedDate = ((DateTime)_RequestItem["Learning aids"]);
							if (_RequestItem["Stationaries"] != null)
								dtcStationaries.SelectedDate = ((DateTime)_RequestItem["Stationaries"]);
							if (_RequestItem["External speakers"] != null)
								dtcExternalSpeakers.SelectedDate = ((DateTime)_RequestItem["External speakers"]);
							if (_RequestItem["Pre-course"] != null)
								dtcPreCourse.SelectedDate = ((DateTime)_RequestItem["Pre-course"]);
							if (_RequestItem["Kp1 evaluation"] != null)
								dtcKP1Evaluation.SelectedDate = ((DateTime)_RequestItem["Kp1 evaluation"]);
							if (_RequestItem["Certificates"] != null)
								dtcCertificates.SelectedDate = ((DateTime)_RequestItem["Certificates"]);
							if (_RequestItem["Attendance list"] != null)
								dtcAttendanceList.SelectedDate = ((DateTime)_RequestItem["Attendance list"]);
							if (_RequestItem["Photographs"] != null)
								dtcPhotographs.SelectedDate = ((DateTime)_RequestItem["Photographs"]);
							if (_RequestItem["Post assessment"] != null)
								dtcPostAssessment.SelectedDate = ((DateTime)_RequestItem["Post assessment"]);
						}
						else // Not Request Change (i.e Task assigned to anyone except requester) or Requester for the second cycle
						{
							lblProgrammeDate.Text = ((DateTime)_RequestItem["Programme date"]).ToString("dd MMM yyyy");
							lblProgrammeVenue.Text = _RequestItem["Programme venue"].ToString();


							//changesNew
							lblProvider.Text = _RequestItem["Provider"].ToString();
							lblIsMTNAcademyProgramme.Text = _RequestItem["Is it mtn academy"].ToString();

							/*lblProvider.Text = SPHandler.GetProvider(int.Parse(_RequestItem["Provider"].ToString()), CurrentWeb.Site.RootWeb);*///_RequestItem["ProviderId"].ToString();
							lblIsMTNAcademyProgramme.Text = _RequestItem["Is it mtn academy"].ToString();
							//if (lblIsMTNAcademyProgramme.Text == "Others" && !String.IsNullOrEmpty((string)_RequestItem["Is MTN Programme Others"]))
							//{
							//	lblIsMTNProgrammeOthers.Text = _RequestItem["Is MTN Programme Others"].ToString();
							//	IsMTNAcademyProgrammeOthersRow.Style["display"] = "block";
							//}

							lblProgrammeCoordinator.Text = ProgrammeCoordinatorUser.Name + " (" + ProgrammeCoordinatorUser.Email + ")";

							lblProgrammeFacilitator.Text = _RequestItem["Programme facilitator"].ToString();

							//string[] tempFacultyarr = _RequestItem["Faculty"].ToString().Split(new string[] { ";#" }, StringSplitOptions.RemoveEmptyEntries);
							//string tempFaculty = "";
							//for (int i = 0; i < tempFacultyarr.Length; i++)
							//	tempFaculty += tempFacultyarr[i] + " ; ";
							//lblFaculty.Text = tempFaculty.Remove(tempFaculty.Length - 3);

							//changesNew
							lblFaculty.Text = _RequestItem["Faculty"].ToString();

							if (!String.IsNullOrEmpty((string)_RequestItem["Facilitator last programme and date"]))
								lblFacilitatorLastProgrammeAndDate.Text = _RequestItem["Facilitator last programme and date"].ToString();

							if (_RequestItem["Facilitator last evaluation score"] != null)
								lblFacilitatorLastEvaluationScore.Text = _RequestItem["Facilitator last evaluation score"].ToString();

							lblVenueArrangement.Text = _RequestItem["Venue arrangement"].ToString();

							//string[] tempEquipmentsRequiredarr = _RequestItem["Equipments required"].ToString().Split(new string[] { ";#" }, StringSplitOptions.RemoveEmptyEntries);
							//string tempEquipmentsRequired = "";
							//for (int i = 0; i < tempEquipmentsRequiredarr.Length; i++)
							//	tempEquipmentsRequired += tempEquipmentsRequiredarr[i] + " ; ";


							//changesNew
							lblEquipmentsRequired.Text = _RequestItem["Equipments required"].ToString();

							//if (!String.IsNullOrEmpty((string)_RequestItem["Others Equipments Required"]))
							//{
							//	lblOthersEquipmentsRequired.Text = _RequestItem["Others Equipments Required"].ToString();
							//	OthersEquipmentsRequiredRow.Style["display"] = "block";
							//}

							if (_RequestItem["Programme content"] != null)
								lblProgrammeContent.Text = ((DateTime)_RequestItem["Programme content"]).ToString("dd MMM yyyy");
							if (_RequestItem["Case study"] != null)
								lblCaseStudy.Text = ((DateTime)_RequestItem["Case study"]).ToString("dd MMM yyyy");
							if (_RequestItem["Activities"] != null)
								lblActivities.Text = ((DateTime)_RequestItem["Activities"]).ToString("dd MMM yyyy");
							if (_RequestItem["Venue booking"] != null)
								lblBooking.Text = ((DateTime)_RequestItem["Venue booking"]).ToString("dd MMM yyyy");
							if (_RequestItem["Training materials"] != null)
								lblTrainingMaterials.Text = ((DateTime)_RequestItem["Training materials"]).ToString("dd MMM yyyy");
							if (_RequestItem["Delegate availabilty sent"] != null)
								lblDelegateAvailabilitySent.Text = ((DateTime)_RequestItem["Delegate availabilty sent"]).ToString("dd MMM yyyy");
							if (_RequestItem["Invites sent"] != null)
								lblInvitesSent.Text = ((DateTime)_RequestItem["Invites sent"]).ToString("dd MMM yyyy");
							if (_RequestItem["Delegate pre-work"] != null)
								lblDelegatePreWork.Text = ((DateTime)_RequestItem["Delegate pre-work"]).ToString("dd MMM yyyy");
							if (_RequestItem["Customized training kp1"] != null)
								lblCustomizedTrainingKP1EvaluationForm.Text = ((DateTime)_RequestItem["Customized training kp1"]).ToString("dd MMM yyyy");
							if (_RequestItem["Completed training evaluation form"] != null)
								lblCompletedTrainingEvaluationForms.Text = ((DateTime)_RequestItem["Completed training evaluation form"]).ToString("dd MMM yyyy");
							if (_RequestItem["Evaluation report"] != null)
								lblEvaluationReport.Text = ((DateTime)_RequestItem["Evaluation report"]).ToString("dd MMM yyyy");
							lblProgrammeCoordinationChecklist.Text = _RequestItem["Programme coordination checklist"].ToString();

							//if (lblProgrammeCoordinationChecklist.Text == "External")
							//{
							//	lblProgrammeCoordinationChecklist.Text = "External / Inplant / TTT";
							//	if (_RequestItem["Invites sent"] != null)
							//		lblInvitationLetterForVisaToFacilitator.Text = ((DateTime)_RequestItem["Invites sent"]).ToString("dd MMM yyyy");
							//	if (_RequestItem["Protocol or Transport Arrangements"] != null)
							//		lblProtocolOrTransportArrangements.Text = ((DateTime)_RequestItem["Protocol or Transport Arrangements"]).ToString("dd MMM yyyy");
							//	if (_RequestItem["Accomodation Arrangements For Facilitator"] != null)
							//		lblAccomodationArrangementsForFacilitator.Text = ((DateTime)_RequestItem["Accomodation Arrangements For Facilitator"]).ToString("dd MMM yyyy");
							//	if (_RequestItem["Retention Contract"] != null)
							//		lblRetentionContract.Text = ((DateTime)_RequestItem["Retention Contract"]).ToString("dd MMM yyyy");
							//	if (_RequestItem["Travel Requisitions"] != null)
							//		lblTravelRequisitions.Text = ((DateTime)_RequestItem["Travel Requisitions"]).ToString("dd MMM yyyy");

							//	ExternalRow1.Style["display"] = "block";
							//	ExternalRow2.Style["display"] = "block";
							//	ExternalRow3.Style["display"] = "block";
							//	ExternalRow4.Style["display"] = "block";
							//	ExternalRow5.Style["display"] = "block";
							//}

							if (_RequestItem["Itf form 6a"] != null)
								lblITFForm6A.Text = ((DateTime)_RequestItem["Itf form 6a"]).ToString("dd MMM yyyy");
							if (_RequestItem["Facilitators kit"] != null)
								lblFacilitatorKit.Text = ((DateTime)_RequestItem["Facilitators kit"]).ToString("dd MMM yyyy");
							if (_RequestItem["Delegate available to line manager"] != null)
								lblDelegateAvailabilityToLineManagers.Text = ((DateTime)_RequestItem["Delegate available to line manager"]).ToString("dd MMM yyyy");
							if (_RequestItem["Training invitation"] != null)
								lblTrainingInvitation.Text = ((DateTime)_RequestItem["Training invitation"]).ToString("dd MMM yyyy");
							if (_RequestItem["Venue preparation"] != null)
								lblVenuePreparation.Text = ((DateTime)_RequestItem["Venue preparation"]).ToString("dd MMM yyyy");
							if (_RequestItem["Hall"] != null)
								lblHall.Text = ((DateTime)_RequestItem["Hall"]).ToString("dd MMM yyyy");
							if (_RequestItem["Projector"] != null)
								lblProjector.Text = ((DateTime)_RequestItem["Projector"]).ToString("dd MMM yyyy");
							if (_RequestItem["Hall arrangement"] != null)
								lblHallArrangement.Text = ((DateTime)_RequestItem["Hall arrangement"]).ToString("dd MMM yyyy");
							if (_RequestItem["Location"] != null)
								lblLocation.Text = ((DateTime)_RequestItem["Location"]).ToString("dd MMM yyyy");
							if (_RequestItem["Tea break and lunch"] != null)
								lblTeaBreakAndLunch.Text = ((DateTime)_RequestItem["Tea break and lunch"]).ToString("dd MMM yyyy");
							if (_RequestItem["Seating arrangement"] != null)
								lblSeatingArrangement.Text = ((DateTime)_RequestItem["Seating arrangement"]).ToString("dd MMM yyyy");
							if (_RequestItem["Location branding"] != null)
								lblLocationBranding.Text = ((DateTime)_RequestItem["Location branding"]).ToString("dd MMM yyyy");
							if (_RequestItem["Programme materials"] != null)
								lblProgrammeMaterials.Text = ((DateTime)_RequestItem["Programme materials"]).ToString("dd MMM yyyy");
							if (_RequestItem["Wall charts"] != null)
								lblWallCharts.Text = ((DateTime)_RequestItem["Wall charts"]).ToString("dd MMM yyyy");
							if (_RequestItem["Videos/dvd"] != null)
								lblVidoesOrDVD.Text = ((DateTime)_RequestItem["Videos/dvd"]).ToString("dd MMM yyyy");
							if (_RequestItem["Participant guide"] != null)
								lblParticipantGuide.Text = ((DateTime)_RequestItem["Participant guide"]).ToString("dd MMM yyyy");
							if (_RequestItem["Presentation slides"] != null)
								lblPresentationSlides.Text = ((DateTime)_RequestItem["Presentation slides"]).ToString("dd MMM yyyy");
							if (_RequestItem["Learning aids"] != null)
								lblLearningAidsActivityCardsOrCaseStudies.Text = ((DateTime)_RequestItem["Learning aids"]).ToString("dd MMM yyyy");
							if (_RequestItem["Stationaries"] != null)
								lblStationaries.Text = ((DateTime)_RequestItem["Stationaries"]).ToString("dd MMM yyyy");
							if (_RequestItem["External speakers"] != null)
								lblExternalSpeakers.Text = ((DateTime)_RequestItem["External speakers"]).ToString("dd MMM yyyy");
							if (_RequestItem["Pre-course"] != null)
								lblPreCourse.Text = ((DateTime)_RequestItem["Pre-course"]).ToString("dd MMM yyyy");
							if (_RequestItem["Kp1 evaluation"] != null)
								lblKP1Evaluation.Text = ((DateTime)_RequestItem["Kp1 evaluation"]).ToString("dd MMM yyyy");
							if (_RequestItem["Certificates"] != null)
								lblCertificates.Text = ((DateTime)_RequestItem["Certificates"]).ToString("dd MMM yyyy");
							if (_RequestItem["Attendance list"] != null)
								lblAttendanceList.Text = ((DateTime)_RequestItem["Attendance list"]).ToString("dd MMM yyyy");
							if (_RequestItem["Photographs"] != null)
								lblPhotographs.Text = ((DateTime)_RequestItem["Photographs"]).ToString("dd MMM yyyy");
							if (_RequestItem["Post assessment"] != null)
								lblPostAssessment.Text = ((DateTime)_RequestItem["Post assessment"]).ToString("dd MMM yyyy");

							if (!isFirstTime)
							{
								rowTrainingEvaluationReport.Visible = true;
								PostProgrammeHeaderRow.Visible = true;
								PostProgrammeRow1.Visible = true;
								PostProgrammeRow2.Visible = true;
								if (_DisplayFormMode == "Requester")
								{
									//btnRequestChange.Visible = false;
									btnApprove.Text = "Submit";
									btnReject.Text = "Cancel Request";

									rowMessage.Visible = true;
									if (isRequestChange)
									{
										if (_TaskExtendedProperties["Rejector"].ToString() == HRLearningManagementRequestWFRecjector.LineManager.ToString())
											rowMessage.Cells[0].InnerText = "The Line Manager has requested change from you.";
										else if (_TaskExtendedProperties["Rejector"].ToString() == HRLearningManagementRequestWFRecjector.SeniorManager.ToString())
											rowMessage.Cells[0].InnerText = "The Senior Manager has requested change from you.";
										else if (_TaskExtendedProperties["Rejector"].ToString() == HRLearningManagementRequestWFRecjector.GeneralManager.ToString())
											rowMessage.Cells[0].InnerText = "The General Manager has requested change from you.";

										if (_RequestItem["Completed training evaluation form"] != null)
											dtcCompletedTrainingEvaluationForms.SelectedDate = ((DateTime)_RequestItem["Completed training evaluation form"]);
										if (_RequestItem["Evaluation report"] != null)
											dtcEvaluationReport.SelectedDate = ((DateTime)_RequestItem["Evaluation report"]);

										lblCompletedTrainingEvaluationForms.Visible = false;
										dtcCompletedTrainingEvaluationForms.Visible = true;
										lblEvaluationReport.Visible = false;
										dtcEvaluationReport.Visible = true;

										_hlTrainingEvaluationReport.NavigateUrl = _RequestItem.Attachments.UrlPrefix + _RequestItem.Attachments[0];
										_hlTrainingEvaluationReport.Text = "Training Evaluation Report";
										_fuTrainingEvaluationReport.Style["display"] = "none";
										_hlTrainingEvaluationReport.Style["display"] = "block";
										_fuTrainingEvaluationReportValidator.Enabled = false;
									}
									else
									{
										rowMessage.Cells[0].InnerText = "You should upload Training Evaluation Report & fill missing fields.";

										lblCompletedTrainingEvaluationForms.Visible = false;
										dtcCompletedTrainingEvaluationForms.Visible = true;
										lblEvaluationReport.Visible = false;
										dtcEvaluationReport.Visible = true;

										_fuTrainingEvaluationReport.Style["display"] = "block";
										_hlTrainingEvaluationReport.Style["display"] = "none";
										_fuTrainingEvaluationReportValidator.Enabled = true;
									}
								}
								else
								{
									_hlTrainingEvaluationReport.NavigateUrl = _RequestItem.Attachments.UrlPrefix + _RequestItem.Attachments[0];
									_hlTrainingEvaluationReport.Text = "Training Evaluation Report";
									_fuTrainingEvaluationReport.Style["display"] = "none";
									_hlTrainingEvaluationReport.Style["display"] = "block";
									_fuTrainingEvaluationReportValidator.Enabled = false;
									_btnShowHideFileUpload.Style["display"] = "none";
								}
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.LogToWindows(ex, "HRLearningManagementRequestWFTask, PageLoad");
				isValid = false;
			}
			if (!isValid)
				Response.Redirect(SPContext.Current.Web.Url);
			//https://
			//sharepoint.mtnnigeria.net/sites/HumanResources/Pages/Human-Resource.aspx#/playlist1/local1
			//Response.Redirect("https://" +
				//"sharepoint.mtnnigeria.net/sites/HumanResources/Pages/Human-Resource.aspx#/playlist1/local1");



			//Response.Redirect(SPContext.Current.Web.Url + "/_layouts/HR_learning_And_Mgmt/NewForm.aspx");
		}

		private void ShowRequesterControls()
		{
			lblProgrammeDate.Visible = false;
			dtcProgrammeDate.Visible = true;
			lblProgrammeVenue.Visible = false;
			txtProgrammeVenue.Visible = true;
			lblProvider.Visible = false;
			ddlProvider.Visible = true;
			lblIsMTNAcademyProgramme.Visible = false;
			rblIsMTNAcademyProgramme.Visible = true;
			lblIsMTNProgrammeOthers.Visible = false;
			txtIsMTNProgrammeOthers.Visible = true;
			lblProgrammeCoordinator.Visible = false;
			peProgrammeCoordinator.Visible = true;
			spanProgrammeCoordinator.Visible = true;
			lblProgrammeFacilitator.Visible = false;
			txtProgrammeFacilitator.Visible = true;
			lblFaculty.Visible = false;
			cblFaculty.Visible = true;
			spanFaculty.Visible = true;
			lblFacilitatorLastProgrammeAndDate.Visible = false;
			txtFacilitatorLastProgrammeAndDate.Visible = true;
			lblFacilitatorLastEvaluationScore.Visible = false;
			txtFacilitatorLastEvaluationScore.Visible = true;
			lblVenueArrangement.Visible = false;
			rblVenueArrangement.Visible = true;
			lblEquipmentsRequired.Visible = false;
			cblEquipmentsRequired.Visible = true;
			spanEquipmentsRequired.Visible = true;
			lblOthersEquipmentsRequired.Visible = false;
			txtOthersEquipmentsRequired.Visible = true;
			lblProgrammeContent.Visible = false;
			dtcProgrammeContent.Visible = true;
			lblCaseStudy.Visible = false;
			dtcCaseStudy.Visible = true;
			lblActivities.Visible = false;
			dtcActivities.Visible = true;
			lblBooking.Visible = false;
			dtcBooking.Visible = true;
			lblTrainingMaterials.Visible = false;
			dtcTrainingMaterials.Visible = true;
			lblDelegateAvailabilitySent.Visible = false;
			dtcDelegateAvailabilitySent.Visible = true;
			lblInvitesSent.Visible = false;
			dtcInvitesSent.Visible = true;
			lblDelegatePreWork.Visible = false;
			dtcDelegatePreWork.Visible = true;
			lblCustomizedTrainingKP1EvaluationForm.Visible = false;
			dtcCustomizedTrainingKP1EvaluationForm.Visible = true;
			lblCompletedTrainingEvaluationForms.Visible = false;
			dtcCompletedTrainingEvaluationForms.Visible = true;
			lblEvaluationReport.Visible = false;
			dtcEvaluationReport.Visible = true;
			lblProgrammeCoordinationChecklist.Visible = false;
			rblProgrammeCoordinationChecklist.Visible = true;
			lblITFForm6A.Visible = false;
			dtcITFForm6A.Visible = true;
			lblInvitationLetterForVisaToFacilitator.Visible = false;
			dtcInvitationLetterForVisaToFacilitator.Visible = true;
			lblProtocolOrTransportArrangements.Visible = false;
			dtcProtocolOrTransportArrangements.Visible = true;
			lblAccomodationArrangementsForFacilitator.Visible = false;
			dtcAccomodationArrangementsForFacilitator.Visible = true;
			lblFacilitatorKit.Visible = false;
			dtcFacilitatorKit.Visible = true;
			lblDelegateAvailabilityToLineManagers.Visible = false;
			dtcDelegateAvailabilityToLineManagers.Visible = true;
			lblRetentionContract.Visible = false;
			dtcRetentionContract.Visible = true;
			lblTrainingInvitation.Visible = false;
			dtcTrainingInvitation.Visible = true;
			lblTravelRequisitions.Visible = false;
			dtcTravelRequisitions.Visible = true;
			lblVenuePreparation.Visible = false;
			dtcVenuePreparation.Visible = true;
			lblHall.Visible = false;
			dtcHall.Visible = true;
			lblProjector.Visible = false;
			dtcProjector.Visible = true;
			lblHallArrangement.Visible = false;
			dtcHallArrangement.Visible = true;
			lblLocation.Visible = false;
			dtcLocation.Visible = true;
			lblTeaBreakAndLunch.Visible = false;
			dtcTeaBreakAndLunch.Visible = true;
			lblSeatingArrangement.Visible = false;
			dtcSeatingArrangement.Visible = true;
			lblLocationBranding.Visible = false;
			dtcLocationBranding.Visible = true;
			lblProgrammeMaterials.Visible = false;
			dtcProgrammeMaterials.Visible = true;
			lblWallCharts.Visible = false;
			dtcWallCharts.Visible = true;
			lblVidoesOrDVD.Visible = false;
			dtcVidoesOrDVD.Visible = true;
			lblParticipantGuide.Visible = false;
			dtcParticipantGuide.Visible = true;
			lblPresentationSlides.Visible = false;
			dtcPresentationSlides.Visible = true;
			lblLearningAidsActivityCardsOrCaseStudies.Visible = false;
			dtcLearningAidsActivityCardsOrCaseStudies.Visible = true;
			lblStationaries.Visible = false;
			dtcStationaries.Visible = true;
			lblExternalSpeakers.Visible = false;
			dtcExternalSpeakers.Visible = true;
			lblPreCourse.Visible = false;
			dtcPreCourse.Visible = true;
			lblKP1Evaluation.Visible = false;
			dtcKP1Evaluation.Visible = true;
			lblCertificates.Visible = false;
			dtcCertificates.Visible = true;
			lblAttendanceList.Visible = false;
			dtcAttendanceList.Visible = true;
			lblPhotographs.Visible = false;
			dtcPhotographs.Visible = true;
			lblPostAssessment.Visible = false;
			dtcPostAssessment.Visible = true;
			//////////////////////////////////////////////////////////
			txtIsMTNProgrammeOthersValidator.Visible = true;
			txtOthersEquipmentsRequiredValidator.Visible = true;
			txtProgrammeVenueValidator.Visible = true;
			ddlProviderValidator.Visible = true;
			txtProgrammeFacilitatorValidator.Visible = true;
			txtFacilitatorLastEvaluationScoreValidator.Visible = true;
		}

		private void HideControlM()
		{
			//ControlMRow1.Visible = false;
			//ControlMRow2.Visible = false;
			//ControlMRow3.Visible = false;
			//ControlMRow4.Visible = false;
			//ControlMRow6.Visible = false;
			//ControlMRow7.Visible = false;
			//ControlMRow8.Visible = false;
			//ControlMRow9.Visible = false;
			//ControlMRow10.Visible = false;
			//ControlMRow11.Visible = false;
		}

		private void HideMonitoring()
		{
			//MonitoringRow1.Visible = false;
			//MonitoringRow2.Visible = false;
			//MonitoringRow3.Visible = false;
			//MonitoringRow4.Visible = false;
			//MonitoringRow5.Visible = false;
			//MonitoringRow6.Visible = false;
			//MonitoringRow7.Visible = false;
			//MonitoringRow8.Visible = false;
			//MonitoringRow9.Visible = false;
			//MonitoringRow10.Visible = false;
			//MonitoringRow11.Visible = false;
			//MonitoringRow12.Visible = false;
			//MonitoringRow13.Visible = false;
		}

		protected void btnApprove_Click(object sender, EventArgs e)
		{
			bool Success = true;
			try
			{
				SPWeb CurrentWeb = SPContext.Current.Web;
				_TaskList = CurrentWeb.Lists["HrLearningManagementRequestTasks"];
				_TaskListItem = _TaskList.GetItemById(int.Parse(base.Request.Params["ID"]));
				_TaskExtendedProperties = SPWorkflowTask.GetExtendedPropertiesAsHashtable((SPListItem)_TaskListItem);
				_DisplayFormMode = _TaskExtendedProperties["DisplayForm"].ToString();
				//ViewState["DisplayFormMode"] = _DisplayFormMode;
				isFirstTime = bool.Parse(_TaskExtendedProperties["isFirstTime"].ToString());

				if (_DisplayFormMode == "Requester")
					isRequestChange = bool.Parse(_TaskExtendedProperties["isRequestChange"].ToString());

				//_DisplayFormMode = (string)ViewState["DisplayFormMode"];
				SPContext.Current.Web.AllowUnsafeUpdates = true;
				string _comments = GetComments();
				Hashtable _hash = new Hashtable();
				_hash.Add("CurrentUserId", SPContext.Current.Web.CurrentUser.ID);
				_hash.Add("Action", "Approved");
				_hash.Add("Comments", _comments);

				if (_DisplayFormMode == "Requester")
				{
					bool isFormInvalid = false;

					#region Server Side Validation

					if (isFirstTime)
					{

						//Validate PeopleEditor Controls
						if (peProgrammeCoordinator.ResolvedEntities.Count == 0)
							spanProgrammeCoordinator.Style["display"] = "block";
						else
							spanProgrammeCoordinator.Style["display"] = "none";

						//Validation for the people editors to ensure they are not empty
						//It can't be client sided since in some cases the error message will continue to show
						if (peProgrammeCoordinator.ResolvedEntities.Count == 0)
							isFormInvalid = true;

						if (cblFaculty.SelectedIndex == -1)
						{
							spanFaculty.Style["display"] = "block";
							isFormInvalid = true;
						}
						else
							spanFaculty.Style["display"] = "none";

						if (cblEquipmentsRequired.SelectedIndex == -1)
						{
							spanEquipmentsRequired.Style["display"] = "block";
							isFormInvalid = true;
						}
						else
							spanEquipmentsRequired.Style["display"] = "none";
					}
					else
					{
						if (dtcCompletedTrainingEvaluationForms.IsDateEmpty || dtcEvaluationReport.IsDateEmpty)
							isFormInvalid = true;
					}

					#endregion Server Side Validation

					if (isFormInvalid || !Page.IsValid)
						return;

					string _Url = SPContext.Current.Web.Url;
					SPSite _site = null;
					SPWeb _web = null;

					SPSecurity.RunWithElevatedPrivileges(delegate ()
					{
						_site = new SPSite(_Url);
						_web = _site.OpenWeb();
						_web.AllowUnsafeUpdates = true;
						////////////////////////////////
						_TaskListItem = SPContext.Current.ListItem;
						_TaskExtendedProperties = SPWorkflowTask.GetExtendedPropertiesAsHashtable((SPListItem)_TaskListItem);
						_ListTitle = _TaskExtendedProperties["ListTitle"].ToString();
						SPList _RequestsList = _web.Lists[_ListTitle];
						SPListItem _RequestItem = _RequestsList.GetItemById(int.Parse(_TaskExtendedProperties["ItemID"].ToString()));

						if (isFirstTime)
						{
							SPUser ProgrammeCoordinatorUser = null;
							//Ensure Users
							PickerEntity ProgrammeCoordinatorEntity = (PickerEntity)peProgrammeCoordinator.ResolvedEntities[0];
							if (ProgrammeCoordinatorEntity.IsResolved == true)
								ProgrammeCoordinatorUser = _web.EnsureUser(ProgrammeCoordinatorEntity.Key);
							else
								ProgrammeCoordinatorUser = null;

							/////People Editors///////////
							if (ProgrammeCoordinatorUser != null)
							{
								SPFieldUserValue ProgrammeCoordinatorUserValue = new SPFieldUserValue(_web, ProgrammeCoordinatorUser.ID, ProgrammeCoordinatorUser.LoginName);
								_RequestItem["Programme coordinator"] = ProgrammeCoordinatorUserValue;
							}
							//////////////////////////////
							//_RequestItem["Programme Name"] = txtProgrammeName.Text;
							_RequestItem["Programme date"] = dtcProgrammeDate.SelectedDate;
							_RequestItem["Programme venue"] = SPHandler.RemoveHTMLTags(txtProgrammeVenue.Text);
							//_RequestItem["ProviderId"] = int.Parse(ddlProvider.Text);
							_RequestItem["Is it mtn academy"] = rblIsMTNAcademyProgramme.Text;
							//if (rblIsMTNAcademyProgramme.Text == "Others" && !String.IsNullOrEmpty(txtIsMTNProgrammeOthers.Text))
							//	_RequestItem["Is MTN Programme Others"] = SPHandler.RemoveHTMLTags(txtIsMTNProgrammeOthers.Text);
							//else
							//	_RequestItem["Is MTN Programme Others"] = null;
							_RequestItem["Programme facilitator"] = SPHandler.RemoveHTMLTags(txtProgrammeFacilitator.Text);
							//string tempFaculty = ";#";
							//foreach (ListItem item in cblFaculty.Items)
							//	if (item.Selected)
							//		tempFaculty += item.Value + ";#";
							//_RequestItem["Faculty"] = tempFaculty;
							if (!String.IsNullOrEmpty(txtFacilitatorLastProgrammeAndDate.Text))
								_RequestItem["Facilitator last programme and date"] = SPHandler.RemoveHTMLTags(txtFacilitatorLastProgrammeAndDate.Text);
							else
								_RequestItem["Facilitator last programme and date"] = null;
							if (!String.IsNullOrEmpty(txtFacilitatorLastEvaluationScore.Text))
								_RequestItem["Facilitator last evaluation score"] = double.Parse(txtFacilitatorLastEvaluationScore.Text);
							else
								_RequestItem["Facilitator last evaluation score"] = null;
							_RequestItem["Venue arrangement"] = rblVenueArrangement.Text;
							//string tempEquipmentsRequired = ";#";
							//foreach (ListItem item in cblEquipmentsRequired.Items)
							//	if (item.Selected)
							//		tempEquipmentsRequired += item.Value + ";#";
							//_RequestItem["Equipments required"] = tempEquipmentsRequired;
							//if (cblEquipmentsRequired.Items[cblEquipmentsRequired.Items.Count - 1].Selected && !String.IsNullOrEmpty(txtOthersEquipmentsRequired.Text))
							//	_RequestItem["Others Equipments Required"] = SPHandler.RemoveHTMLTags(txtOthersEquipmentsRequired.Text);
							//else
							//	_RequestItem["Others Equipments Required"] = null;

							if (!dtcProgrammeContent.IsDateEmpty)
								_RequestItem["Programme content"] = dtcProgrammeContent.SelectedDate;
							else
								_RequestItem["Programme content"] = null;

							if (!dtcCaseStudy.IsDateEmpty)
								_RequestItem["Case study"] = dtcCaseStudy.SelectedDate;
							else
								_RequestItem["Case study"] = null;

							if (!dtcActivities.IsDateEmpty)
								_RequestItem["Activities"] = dtcActivities.SelectedDate;
							else
								_RequestItem["Activities"] = null;

							if (!dtcBooking.IsDateEmpty)
								_RequestItem["Venue booking"] = dtcBooking.SelectedDate;
							else
								_RequestItem["Venue booking"] = null;

							if (!dtcTrainingMaterials.IsDateEmpty)
								_RequestItem["Training materials"] = dtcTrainingMaterials.SelectedDate;
							else
								_RequestItem["Training materials"] = null;

							if (!dtcDelegateAvailabilitySent.IsDateEmpty)
								_RequestItem["Delegate availability sent"] = dtcDelegateAvailabilitySent.SelectedDate;
							else
								_RequestItem["Delegate availability sent"] = null;

							if (!dtcInvitesSent.IsDateEmpty)
								_RequestItem["Invites sent"] = dtcInvitesSent.SelectedDate;
							else
								_RequestItem["Invites sent"] = null;

							if (!dtcDelegatePreWork.IsDateEmpty)
								_RequestItem["Delegate pre-work"] = dtcDelegatePreWork.SelectedDate;
							else
								_RequestItem["Delegate pre-work"] = null;

							if (!dtcCustomizedTrainingKP1EvaluationForm.IsDateEmpty)
								_RequestItem["Customized training kp1"] = dtcCustomizedTrainingKP1EvaluationForm.SelectedDate;
							else
								_RequestItem["Customized training kp1"] = null;

							_RequestItem["Programme coordination checklist"] = rblProgrammeCoordinationChecklist.Text;
							//if (rblProgrammeCoordinationChecklist.Text == "External")
							//{
							//	if (!dtcInvitationLetterForVisaToFacilitator.IsDateEmpty)
							//		_RequestItem["Invitation Letter For Visa To Facilitator"] = dtcInvitationLetterForVisaToFacilitator.SelectedDate;
							//	else
							//		_RequestItem["Invitation Letter For Visa To Facilitator"] = null;

							//	if (!dtcProtocolOrTransportArrangements.IsDateEmpty)
							//		_RequestItem["Protocol or Transport Arrangements"] = dtcProtocolOrTransportArrangements.SelectedDate;
							//	else
							//		_RequestItem["Protocol or Transport Arrangements"] = null;

							//	if (!dtcAccomodationArrangementsForFacilitator.IsDateEmpty)
							//		_RequestItem["Accomodation Arrangements For Facilitator"] = dtcAccomodationArrangementsForFacilitator.SelectedDate;
							//	else
							//		_RequestItem["Accomodation Arrangements For Facilitator"] = null;

							//	if (!dtcRetentionContract.IsDateEmpty)
							//		_RequestItem["Retention Contract"] = dtcRetentionContract.SelectedDate;
							//	else
							//		_RequestItem["Retention Contract"] = null;

							//	if (!dtcTravelRequisitions.IsDateEmpty)
							//		_RequestItem["Travel Requisitions"] = dtcTravelRequisitions.SelectedDate;
							//	else
							//		_RequestItem["Travel Requisitions"] = null;
							//}
							if (!dtcITFForm6A.IsDateEmpty)
								_RequestItem["Itf form 6a"] = dtcITFForm6A.SelectedDate;
							else
								_RequestItem["Itf form 6a"] = null;

							if (!dtcFacilitatorKit.IsDateEmpty)
								_RequestItem["Facilitators kit"] = dtcFacilitatorKit.SelectedDate;
							else
								_RequestItem["Facilitators kit"] = null;

							if (!dtcDelegateAvailabilityToLineManagers.IsDateEmpty)
								_RequestItem["Delegate available to line manager"] = dtcDelegateAvailabilityToLineManagers.SelectedDate;
							else
								_RequestItem["Delegate available to line manager"] = null;

							if (!dtcTrainingInvitation.IsDateEmpty)
								_RequestItem["Training invitation"] = dtcTrainingInvitation.SelectedDate;
							else
								_RequestItem["Training invitation"] = null;

							if (!dtcVenuePreparation.IsDateEmpty)
								_RequestItem["Venue preparation"] = dtcVenuePreparation.SelectedDate;
							else
								_RequestItem["Venue preparation"] = null;

							if (!dtcHall.IsDateEmpty)
								_RequestItem["Hall"] = dtcHall.SelectedDate;
							else
								_RequestItem["Hall"] = null;

							if (!dtcProjector.IsDateEmpty)
								_RequestItem["Projector"] = dtcProjector.SelectedDate;
							else
								_RequestItem["Projector"] = null;

							if (!dtcHallArrangement.IsDateEmpty)
								_RequestItem["Hall arrangement"] = dtcHallArrangement.SelectedDate;
							else
								_RequestItem["Hall arrangement"] = null;

							if (!dtcLocation.IsDateEmpty)
								_RequestItem["Location"] = dtcLocation.SelectedDate;
							else
								_RequestItem["Location"] = null;

							if (!dtcTeaBreakAndLunch.IsDateEmpty)
								_RequestItem["Tea break and lunch"] = dtcTeaBreakAndLunch.SelectedDate;
							else
								_RequestItem["Tea break and lunch"] = null;

							if (!dtcSeatingArrangement.IsDateEmpty)
								_RequestItem["Seating arrangement"] = dtcSeatingArrangement.SelectedDate;
							else
								_RequestItem["Seating arrangement"] = null;

							if (!dtcLocationBranding.IsDateEmpty)
								_RequestItem["Location branding"] = dtcLocationBranding.SelectedDate;
							else
								_RequestItem["Location branding"] = null;

							if (!dtcProgrammeMaterials.IsDateEmpty)
								_RequestItem["Programme materials"] = dtcProgrammeMaterials.SelectedDate;
							else
								_RequestItem["Programme materials"] = null;

							if (!dtcWallCharts.IsDateEmpty)
								_RequestItem["Wall charts"] = dtcWallCharts.SelectedDate;
							else
								_RequestItem["Wall charts"] = null;

							if (!dtcVidoesOrDVD.IsDateEmpty)
								_RequestItem["Videos/dvd"] = dtcVidoesOrDVD.SelectedDate;
							else
								_RequestItem["Videos/dvd"] = null;

							if (!dtcParticipantGuide.IsDateEmpty)
								_RequestItem["Participant guide"] = dtcParticipantGuide.SelectedDate;
							else
								_RequestItem["Participant guide"] = null;

							if (!dtcPresentationSlides.IsDateEmpty)
								_RequestItem["Presentation slides"] = dtcPresentationSlides.SelectedDate;
							else
								_RequestItem["Presentation slides"] = null;

							if (!dtcLearningAidsActivityCardsOrCaseStudies.IsDateEmpty)
								_RequestItem["Learning aids"] = dtcLearningAidsActivityCardsOrCaseStudies.SelectedDate;
							else
								_RequestItem["Learning aids"] = null;

							if (!dtcStationaries.IsDateEmpty)
								_RequestItem["Stationaries"] = dtcStationaries.SelectedDate;
							else
								_RequestItem["Stationaries"] = null;

							if (!dtcExternalSpeakers.IsDateEmpty)
								_RequestItem["External speakers"] = dtcExternalSpeakers.SelectedDate;
							else
								_RequestItem["External speakers"] = null;

							if (!dtcPreCourse.IsDateEmpty)
								_RequestItem["Pre-course"] = dtcPreCourse.SelectedDate;
							else
								_RequestItem["Pre-course"] = null;

							if (!dtcKP1Evaluation.IsDateEmpty)
								_RequestItem["Kp1 evaluation"] = dtcKP1Evaluation.SelectedDate;
							else
								_RequestItem["Kp1 evaluation"] = null;

							if (!dtcCertificates.IsDateEmpty)
								_RequestItem["Certificates"] = dtcCertificates.SelectedDate;
							else
								_RequestItem["Certificates"] = null;

							if (!dtcAttendanceList.IsDateEmpty)
								_RequestItem["Attendance list"] = dtcAttendanceList.SelectedDate;
							else
								_RequestItem["Attendance list"] = null;

							if (!dtcPhotographs.IsDateEmpty)
								_RequestItem["Photographs"] = dtcPhotographs.SelectedDate;
							else
								_RequestItem["Photographs"] = null;

							if (!dtcPostAssessment.IsDateEmpty)
								_RequestItem["Post Assessment"] = dtcPostAssessment.SelectedDate;
							else
								_RequestItem["Post Assessment"] = null;
						}
						else
						{
							if (!dtcCompletedTrainingEvaluationForms.IsDateEmpty)
								_RequestItem["Completed training evaluation form"] = dtcCompletedTrainingEvaluationForms.SelectedDate;
							else
								_RequestItem["Completed training evaluation form"] = null;

							if (!dtcEvaluationReport.IsDateEmpty)
								_RequestItem["Evaluation report"] = dtcEvaluationReport.SelectedDate;
							else
								_RequestItem["Evaluation report"] = null;

							if (_hlTrainingEvaluationReport.Style["display"] == "none" || _hfTrainingEvaluationReport.Value != String.Empty)
							{
								Stream fStreamTrainingEvaluationReport = _fuTrainingEvaluationReport.PostedFile.InputStream;
								byte[] contentsUnitDepartmentOC = new byte[fStreamTrainingEvaluationReport.Length];

								fStreamTrainingEvaluationReport.Read(contentsUnitDepartmentOC, 0, (int)fStreamTrainingEvaluationReport.Length);
								fStreamTrainingEvaluationReport.Close();

								for (int i = 0; i < _RequestItem.Attachments.Count; i++)
									_RequestItem.Attachments.DeleteNow(_RequestItem.Attachments[i]);

								_RequestItem.Attachments.AddNow(_fuTrainingEvaluationReport.PostedFile.FileName, contentsUnitDepartmentOC);
							}
						}
						_RequestItem.Update();
						////////////////////////////////
						_web.AllowUnsafeUpdates = false;

						if (_web != null)
							_web.Dispose();
						if (_site != null)
							_site.Dispose();
					}
					 );
				}
				SPWorkflowTask.AlterTask(SPContext.Current.ListItem, _hash, true);
				SPContext.Current.Web.AllowUnsafeUpdates = false;
                Response.Redirect(SPContext.Current.Web.Url);
            }
			catch (Exception ex)
			{
				ErrorHandler.LogToWindows(ex, "HRLearningManagementRequestWFTask, _btnApprove_Click");
				Success = false;
			}
			if (Success)
				GoBack();
		}

        protected void btnRequestChange_Click(object sender, EventArgs e)
        {
            bool Success = true;
            try
            {
                if (base.Request.Params["ID"] != null)
                {
                    SPWeb CurrentWeb = SPContext.Current.Web;
                    _TaskList = CurrentWeb.Lists["HrLearningManagementRequestTasks"];
                    _TaskListItem = _TaskList.GetItemById(int.Parse(base.Request.Params["ID"]));
                    _TaskExtendedProperties = SPWorkflowTask.GetExtendedPropertiesAsHashtable((SPListItem)_TaskListItem);
                    _ItemID = int.Parse(_TaskExtendedProperties["ItemID"].ToString());

                    using (SPSite site = new SPSite(SPContext.Current.Web.Url))
                    {
                        using (SPWeb Currentweb = site.OpenWeb())
                        {
                            _ListTitle = _TaskExtendedProperties["ListTitle"].ToString();
                            SPList lists = CurrentWeb.Lists["HR_Learning_And_Mgmt_List"];
                            SPListItem item = lists.GetItemById(_ItemID);
                            SPFieldUserValue initiator = new SPFieldUserValue(CurrentWeb, item["Created By"].ToString());

                            string requestChangeLink = CurrentWeb.Url + "/_layouts/15/HR_learning_And_Mgmt/UpdateRequestPage.aspx?ID=" + _ItemID;
                            SPFieldUrlValue statusChange = new SPFieldUrlValue();
                            statusChange.Description = "Change Request";
                            statusChange.Url = requestChangeLink;

                            item["Change Requested"] = "Yes";
                            item["Change Requester"] = SPContext.Current.Web.CurrentUser.Name;
                            item["ChangeRequesterEmail"] = SPContext.Current.Web.CurrentUser.Email;
                            item["Change Request Stage"] = "1";
                            item["ChangeRequesterComment"] = txtComment.Text;
                            item["Status Stage"] = statusChange.Description;
                            item["Tasks ID"] = base.Request.Params["ID"].ToString();
                            item.Update();
                            //item.SystemUpdate();
                            //lists.EnableVersioning = true;
                            //lists.Update();

                            //SendRequesterEmail                    
                            string to = initiator.User.Email;
                            string from = "workflows@mtn.com";
                            string changeRequester = SPContext.Current.Web.CurrentUser.Name;

                            string Body = "Dear " + initiator.User.Name + ",<br><br>";
                            Body += "A change have been requested on your HR Learning Management request by " + changeRequester + "<br><br>Request Change Comment: " + item["ChangeRequesterComment"].ToString() + ".<br><br> Click <a href='" + requestChangeLink + "'>here</a> to update the request. <br><br>";

                            Body += "<br><br>Kind Regards,<br>";
                            Body += "HR Learning Management";
                            Body += "<br><br>NOTE: This email message is auto generated, Please do not reply to the email address.";
                            EMailHandler.SendRequestChangeEmail(to, from, null, "HR Learning Management Change Request", Body);

                            //Response.Redirect(CurrentWeb.Url);


                            //_DisplayFormMode = (string)ViewState["DisplayFormMode"];
                            //SPContext.Current.Web.AllowUnsafeUpdates = true;
                            //string _comments = GetComments();
                            //Hashtable _hash = new Hashtable();
                            //_hash.Add("CurrentUserId", SPContext.Current.Web.CurrentUser.ID);
                            //_hash.Add("Action", "RequestedChange");
                            //_hash.Add("Comments", _comments);

                            //SPWorkflowTask.AlterTask(SPContext.Current.ListItem, _hash, true);
                           SPContext.Current.Web.AllowUnsafeUpdates = false;
                        }
                    }                    
                }
                Response.Redirect(SPContext.Current.Web.Url);
            }
            catch (Exception ex)
            {
                File.WriteAllText(@"C:\Temp\requestError.log", ex.ToString());
                ErrorHandler.LogToWindows(ex, "HRLearningManagementRequestWFTask, _btnRequestChange_Click");
                Success = false;
            }
            if (Success)
                GoBack();
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            bool Success = true;
            try
            {
                if (base.Request.Params["ID"] != null)
                {
                    SPWeb CurrentWeb = SPContext.Current.Web;
                    _TaskList = CurrentWeb.Lists["HrLearningManagementRequestTasks"];
                    _TaskListItem = _TaskList.GetItemById(int.Parse(base.Request.Params["ID"]));
                    _TaskExtendedProperties = SPWorkflowTask.GetExtendedPropertiesAsHashtable((SPListItem)_TaskListItem);
                    _ItemID = int.Parse(_TaskExtendedProperties["ItemID"].ToString());

                    using (SPSite site = new SPSite(SPContext.Current.Web.Url))
                    {
                        using (SPWeb Currentweb = site.OpenWeb())
                        {
                            _ListTitle = _TaskExtendedProperties["ListTitle"].ToString();
                            SPList lists = CurrentWeb.Lists["HR_Learning_And_Mgmt_List"];
                            SPListItem item = lists.GetItemById(_ItemID);

                            item["ChangeRequesterComment"] = txtComment.Text;
                            item.Update();
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
            try
            {
                _DisplayFormMode = (string)ViewState["DisplayFormMode"];
                SPContext.Current.Web.AllowUnsafeUpdates = true;
                string _comments = GetComments();
                Hashtable _hash = new Hashtable();
                _hash.Add("CurrentUserId", SPContext.Current.Web.CurrentUser.ID);
                _hash.Add("Action", "Rejected");
                _hash.Add("Comments", _comments);
                //if (_DisplayFormMode == "HRRecruitAdmin" && _TaskExtendedProperties["OD"].ToString() == "FirstTime")
                //{
                //    string OD = _DisplayRRUserControl.GetPeopleEditor();
                //    if (OD == "")
                //        return;
                //    else
                //        _hash.Add("ODName", _DisplayRRUserControl.GetPeopleEditor());
                //}
                //if (_DisplayFormMode == "HRExec")
                //    _hash.Add("Actor", SPContext.Current.Web.CurrentUser.Name + "," + SPContext.Current.Web.CurrentUser.LoginName.Split('\\')[1]);
                SPWorkflowTask.AlterTask(SPContext.Current.ListItem, _hash, true);
                SPContext.Current.Web.AllowUnsafeUpdates = false;

                Response.Redirect(SPContext.Current.Web.Url);
            }
            catch (Exception ex)
            {
                ErrorHandler.LogToWindows(ex, "HRLearningManagementRequestWFTask, _btnReject_Click");
                Success = false;
            }
            if (Success)
                GoBack();        
		}

		protected void btnClose_Click(object sender, EventArgs e)
		{
			GoBack();
		}

		void GoBack()
		{


            Response.Redirect(SPContext.Current.Web.Url);
            //if (Page.Request.QueryString["Source"] != null)
            //{
            //Response.Redirect("https://" +
            //"sharepoint.mtnnigeria.net/sites/HumanResources/Pages/Human-Resource.aspx#/playlist1/local1");
            //Response.Redirect(Page.Request.QueryString["Source"].ToString());
            //Response.Redirect(SPContext.Current.Web.Url);
            //}
            //else
            //Response.Redirect("
            //https://
            //testshare.mtnnigeria.net/sites/HumanResources/Pages/Human-Resource.aspx#/playlist1/local1");
            //Response.Redirect(SPContext.Current.Web.Url);


            //Response.Redirect("https://" +
            //"sharepoint.mtnnigeria.net/sites/HumanResources/Pages/Human-Resource.aspx#/playlist1/local1");
            //Response.Redirect(SPContext.Current.Web.Url + "/_layouts/HR_learning_And_Mgmt/NewForm.aspx");
        }

		public string GetComments()
		{
			System.Text.StringBuilder comments = new System.Text.StringBuilder(ltlCommentsHistory.Text);
			if (!String.IsNullOrEmpty(txtComment.Text))
			{
				comments.Append("<table width='100%' cellpadding='4' cellspacing='0' border='1' class='commentstablestyle'>");
				comments.Append("<tr>");
				comments.Append("<td width='140' valign='top'>");
				comments.Append("<strong>");
				comments.Append(SPContext.Current.Web.CurrentUser.Name);
				comments.Append("</strong><br>");
				comments.Append("<span class='commentsdate'>(");
				comments.Append(DateTime.Now.ToString("dd MMM yyyy hh:mm:ss"));
				comments.Append(")</span>");
				comments.Append("</td>");
				comments.Append("<td>");
				comments.Append(SPHandler.RemoveHTMLTags(txtComment.Text).Replace("\r\n", "<br>"));
				comments.Append("</td>");
				comments.Append("</tr>");
				comments.Append("</table>");
			}
			return comments.ToString();
		}

		private void GetProviders()
		{

			ddlProvider.DataSource = SPHandler.GetProviders(SPContext.Current.Site.RootWeb);
			ddlProvider.DataValueField = "ID";
			ddlProvider.DataTextField = "Title";
			ddlProvider.DataBind();
		}
		public enum HRLearningManagementRequestWFRecjector
		{
			LineManager = 0, SeniorManager = 1, GeneralManager = 2
		}
	}
}
