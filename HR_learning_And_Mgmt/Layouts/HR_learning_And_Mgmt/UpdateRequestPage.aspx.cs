﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using HR_learning_And_Mgmt.Code;
using System.Collections;
using Microsoft.SharePoint.Workflow;
using System.Web.UI.WebControls;
using System.IO;

namespace HR_learning_And_Mgmt.Layouts.HR_learning_And_Mgmt
{
    public partial class UpdateRequestPage : LayoutsPageBase
    {
        //private object faculty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
            try
            {
                if (base.Request.Params["ID"] != null)
                {
                    LoadProvider();
                    SPWeb CurrentWeb = SPContext.Current.Web;
                    int _ItemID = int.Parse(base.Request.Params["ID"]);
                    //    using (SPSite site = new SPSite(SPContext.Current.Web.Url))
                    //{
                    //    using (SPWeb web = site.OpenWeb())
                    //{

                    SPList list = CurrentWeb.Lists["HR_Learning_And_Mgmt_List"];
                    SPListItem item = list.GetItemById(_ItemID);

                    txtProgrammeName.Text = item["Programme name"].ToString();
                    txtProgrammeVenue.Text = item["Programme venue"].ToString();

                    SPFieldUserValue lineManager = new SPFieldUserValue(CurrentWeb, item["Line manager"].ToString());
                    peLineManager.CommaSeparatedAccounts = lineManager.User.LoginName;

                    SPFieldUserValue seniorManager = new SPFieldUserValue(CurrentWeb, item["Senior manager"].ToString());
                    peSeniorManager.CommaSeparatedAccounts = getActualLogin(seniorManager.User.LoginName);

                    SPFieldUserValue generalManager = new SPFieldUserValue(CurrentWeb, item["General manager"].ToString());
                    peGeneralManager.CommaSeparatedAccounts = getActualLogin(generalManager.User.LoginName);

                    dtcProgrammeDate.SelectedDate = DateTime.Parse(item["Programme date"].ToString());
                    ddlProvider.SelectedItem.Text = item["Provider"].ToString();
                    ddlProvider.SelectedItem.Value = item["Provider"].ToString();
                    //rblIsMTNAcademyProgramme.SelectedItem.Value = item["Is it mtn academy"].ToString();
                    try
                    {
                        string academyCollection = item["Is it mtn academy"].ToString();
                        foreach (ListItem academyItem in rblIsMTNAcademyProgramme.Items)
                        {
                            if (academyItem.Text == academyCollection)
                            {
                                //cblFaculty.SelectedItem.Selected = true;
                                academyItem.Selected = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        File.WriteAllText(@"C:\Temp\academyRequestError.log", ex.ToString());
                    }

                    SPFieldUserValue programmeCoordinator = new SPFieldUserValue(CurrentWeb, item["Programme coordinator"].ToString());
                    peProgrammeCoordinator.CommaSeparatedAccounts = getActualLogin(programmeCoordinator.User.LoginName);

                    txtProgrammeFacilitator.Text = item["Programme facilitator"].ToString();
                    //faculty = item["Faculty"].ToString();
                    try
                    {
                        string[] faculties = item["Faculty"].ToString().Split(',');
                        foreach(string facultyItem in faculties)
                        {
                            if(facultyItem != "")
                            {
                                
                               foreach(ListItem listItem in cblFaculty.Items)
                                {
                                    if(listItem.Text == facultyItem)
                                    {
                                       // cblFaculty.SelectedItem.Selected = true;
                                        listItem.Selected = true;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        File.WriteAllText(@"C:\Temp\facultyRequestError.log", ex.ToString());
                    }
                    txtFacilitatorLastProgrammeAndDate.Text = item["Facilitator last programme and date"].ToString();
                    txtFacilitatorLastEvaluationScore.Text = item["Facilitator last evaluation score"].ToString();
                    try
                    {
                        string venueCollection = item["Venue arrangement"].ToString();
                        foreach (ListItem venueItem in rblVenueArrangement.Items)
                        {
                            if (venueItem.Text == venueCollection)
                            {
                                //cblFaculty.SelectedItem.Selected = true;
                                venueItem.Selected = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        File.WriteAllText(@"C:\Temp\venuRequestError.log", ex.ToString());
                    }
                    //rblVenueArrangement.SelectedItem.Value = item["Venue arrangement"].ToString();
                    // equipmentRequest() = item["Equipments required"]
                    //item["Equipments required"] = equipment;
                    try
                    {
                        string[] equipmentCollectionArray = item["Equipments required"].ToString().Split(',');
                        foreach (string equipmentItem in equipmentCollectionArray)
                        {
                            if (equipmentItem != "")
                            {

                                foreach (ListItem elistItem in cblEquipmentsRequired.Items)
                                {
                                    if (elistItem.Text == equipmentItem)
                                    {
                                        //cblFaculty.SelectedItem.Selected = true;
                                        elistItem.Selected = true;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        File.WriteAllText(@"C:\Temp\equipmentRequestError.log", ex.ToString());
                    }

                    //rblProgrammeCoordinationChecklist.SelectedItem.Text = item["Programme coordination checklist"].ToString();
                    try
                    {
                        string[] coodinatorCollectionArray = item["Programme coordination checklist"].ToString().Split(',');
                        foreach (string coodinatorItem in coodinatorCollectionArray)
                        {
                            if (coodinatorItem != "")
                            {

                                foreach (ListItem coordlistItem in rblProgrammeCoordinationChecklist.Items)
                                {
                                    if (coordlistItem.Text == coodinatorItem)
                                    {
											//cblFaculty.SelectedItem.Selected = true;
											coordlistItem.Selected = true;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        File.WriteAllText(@"C:\Temp\coordinationRequestError.log", ex.ToString());
                    }
                    
                    dtcProgrammeContent.SelectedDate = DateTime.Parse(item["Programme content"].ToString());
                    dtcCaseStudy.SelectedDate = DateTime.Parse(item["Case study"].ToString());
                    dtcActivities.SelectedDate = DateTime.Parse(item["Activities"].ToString());
                    dtcBooking.SelectedDate = DateTime.Parse(item["Venue booking"].ToString());
                    dtcTrainingMaterials.SelectedDate = DateTime.Parse(item["Training materials"].ToString());
                    dtcDelegateAvailabilitySent.SelectedDate = DateTime.Parse(item["Delegate availabilty sent"].ToString());
                    dtcInvitesSent.SelectedDate = DateTime.Parse(item["Invites sent"].ToString());
                    dtcDelegatePreWork.SelectedDate = DateTime.Parse(item["Delegate pre-work"].ToString());
                    dtcCustomizedTrainingKP1EvaluationForm.SelectedDate = DateTime.Parse(item["Customized training kp1"].ToString());
                    dtcITFForm6A.SelectedDate = DateTime.Parse(item["Itf form 6a"].ToString());
                    dtcFacilitatorKit.SelectedDate = DateTime.Parse(item["Facilitators kit"].ToString());
                    dtcDelegateAvailabilityToLineManagers.SelectedDate = DateTime.Parse(item["Delegate available to line manager"].ToString());
                    dtcTrainingInvitation.SelectedDate = DateTime.Parse(item["Training invitation"].ToString());
                    dtcVenuePreparation.SelectedDate = DateTime.Parse(item["Venue preparation"].ToString());
                    dtcHall.SelectedDate = DateTime.Parse(item["Hall"].ToString());
                    dtcProjector.SelectedDate = DateTime.Parse(item["Projector"].ToString());
                    dtcHallArrangement.SelectedDate = DateTime.Parse(item["Hall arrangement"].ToString());
                    dtcLocation.SelectedDate = DateTime.Parse(item["Location"].ToString());
                    dtcTeaBreakAndLunch.SelectedDate = DateTime.Parse(item["Tea break and lunch"].ToString());
                    dtcSeatingArrangement.SelectedDate = DateTime.Parse(item["Seating arrangement"].ToString());
                    dtcLocationBranding.SelectedDate = DateTime.Parse(item["Location branding"].ToString());
                    dtcProgrammeMaterials.SelectedDate = DateTime.Parse(item["Programme materials"].ToString());
                    dtcWallCharts.SelectedDate = DateTime.Parse(item["Wall charts"].ToString());
                    dtcVidoesOrDVD.SelectedDate = DateTime.Parse(item["Videos/dvd"].ToString());
                    dtcParticipantGuide.SelectedDate = DateTime.Parse(item["Participant guide"].ToString());
                    dtcPresentationSlides.SelectedDate = DateTime.Parse(item["Presentation slides"].ToString());
                    dtcLearningAidsActivityCardsOrCaseStudies.SelectedDate = DateTime.Parse(item["Learning aids"].ToString());
                    dtcStationaries.SelectedDate = DateTime.Parse(item["Stationaries"].ToString());
                    dtcExternalSpeakers.SelectedDate = DateTime.Parse(item["External speakers"].ToString());
                    dtcPreCourse.SelectedDate = DateTime.Parse(item["Pre-course"].ToString());
                    dtcKP1Evaluation.SelectedDate = DateTime.Parse(item["Kp1 evaluation"].ToString());
                    dtcCertificates.SelectedDate = DateTime.Parse(item["Certificates"].ToString());
                    dtcAttendanceList.SelectedDate = DateTime.Parse(item["Attendance list"].ToString());
                    dtcPhotographs.SelectedDate = DateTime.Parse(item["Photographs"].ToString());
                    dtcPostAssessment.SelectedDate = DateTime.Parse(item["Post assessment"].ToString());
                    item.Update();

                    //string URL = SPContext.Current.Web.Url + "/_layouts/HR_learning_And_Mgmt/NewForm.aspx";
                    //MessagePrompt.ShowMessage(this, "Request Submitted Successfully!!!", URL);


                    //Response.Redirect(SPContext.Current.Web.Url + "/_layouts/HR_learning_And_Mgmt/NewForm.aspx");

                    //Response.Redirect("
                    //https
                    //://sharepoint.mtnnigeria.net/sites/HumanResources/Pages/Human-Resource.aspx#/playlist1/local1
                    //");

                    //Response.Redirect(SPContext.Current.Web.Url);
                    //}
                    //}
                }
            }
            catch (Exception ex)
            {
                File.WriteAllText(@"C:\Temp\requestError.log", ex.ToString());
                ErrorHandler.LogToWindows(ex, "HR_Learning_and_Management, NewRequest");
            }

            }
        }

        private void LoadProvider()
        {
            ddlProvider.DataSource = this.Web.Lists["Providers"].Items.GetDataTable();
			ddlProvider.DataValueField = "Title";
            ddlProvider.DataTextField = "Title";
            ddlProvider.DataBind();
            ddlProvider.Items.Insert(0, new ListItem("", ""));
        }

        //public void equipmentRequest(SPListItem item)
        //{
        //    try
        //    {
        //string[] equipmentCollectionArray = item["Equipment Received"].ToString().Split(',');
        //foreach (string equipmentItem in equipmentCollectionArray)
        //{
        //    if (equipmentItem != "")
        //    {

        //        foreach (ListItem listItem in cblEquipmentsRequired.Items)
        //        {
        //            if (listItem.Text == equipmentItem)
        //            {
        //                cblFaculty.SelectedItem.Selected = true;
        //            }
        //        }
        //    }
        //}
        //string equipmentCollections = item["Equipments required"].ToString();
        //int indexComma = equipmentCollections.IndexOf(",");
        //if(indexComma > -1)
        //{
        //    string[] equipmentCollectionArray = equipmentCollections.Split(',');
        //}
        //else
        //{
        //    string equipmentCollectionArray = equipmentCollections;
        //}
        //foreach (ListItem items in this.cblEquipmentsRequired.Items)
        //{

        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        File.WriteAllText(@"C:\Temp\requestError.log", ex.ToString());
        //    }
        //}

        public string getActualLogin(string claimLogin)
        {
            try
            {
                int barLoc = claimLogin.IndexOf("|");
                if(barLoc > -1)
                {
                    claimLogin = claimLogin.Substring(barLoc + 1);
                }
            }
            catch (Exception ex)
            {
                File.WriteAllText(@"C:\Temp\getActualLoginError.log", ex.ToString());
            }
            return claimLogin;
        }

        //protected void _btnUpdateRequest_Click(object sender, EventArgs e)
        //{
        //    string faculty = string.Empty;
        //    foreach (ListItem item in this.cblFaculty.Items)
        //    {
        //        if (item.Selected)
        //        {
        //            faculty += item + ",";
        //        }
        //    }
             
        //    string equipment = string.Empty;
        //    foreach (ListItem item in this.cblEquipmentsRequired.Items)
        //    {
        //        if (item.Selected)
        //        {
        //            equipment += item + ",";
        //        }
        //    }
             
        //    try
        //    { 
        //        if (base.Request.Params["ID"] != null)
        //        {
        //            using (SPSite site = new SPSite(SPContext.Current.Web.Url))
        //            {
        //                using (SPWeb web = site.OpenWeb())
        //                {
        //                    int _ItemID = int.Parse(base.Request.Params["ID"]);
        //                    SPList list = web.Lists["HR_Learning_And_Mgmt_List"];
        //                    SPListItem item = list.GetItemById(_ItemID);
        //                    //  item["Title"] = "HR_learning_And_Mgmt" + DateTime.Now;
        //                    //item["User name"] = SPContext.Current.Web.CurrentUser.Name;
                             
        //                    item["Programme name"] = txtProgrammeName.Text;
        //                    item["Programme venue"] = txtProgrammeVenue.Text;
        //                    item["Line manager"] = Helper.GetUserValue(peLineManager, web);
        //                    item["Senior manager"] = Helper.GetUserValue(peSeniorManager, web);
        //                    item["General manager"] = Helper.GetUserValue(peGeneralManager, web);
        //                    item["Programme date"] = Convert.ToDateTime(dtcProgrammeDate.SelectedDate.ToString());
        //                    item["Provider"] = ddlProvider.SelectedItem.Text;
        //                    item["Is it mtn academy"] = rblIsMTNAcademyProgramme.SelectedItem.Value;
        //                    item["Programme coordinator"] = Helper.GetUserValue(peProgrammeCoordinator, web);
        //                    item["Programme facilitator"] = txtProgrammeFacilitator.Text;
        //                    item["Faculty"] = faculty;
                             
        //                    item["Facilitator last programme and date"] = txtFacilitatorLastProgrammeAndDate.Text;
        //                    item["Facilitator last evaluation score"] = txtFacilitatorLastEvaluationScore.Text;
        //                    item["Venue arrangement"] = rblVenueArrangement.SelectedItem.Value;
        //                    item["Equipments required"] = equipment;
        //                    item["Programme coordination checklist"] = rblProgrammeCoordinationChecklist.SelectedItem.Text;
        //                    item["Programme content"] = Convert.ToDateTime(dtcProgrammeContent.SelectedDate.ToString());
        //                    item["Case study"] = Convert.ToDateTime(dtcCaseStudy.SelectedDate.ToString());
        //                    item["Activities"] = Convert.ToDateTime(dtcActivities.SelectedDate.ToString());
        //                    item["Venue booking"] = Convert.ToDateTime(dtcBooking.SelectedDate.ToString());
        //                    item["Training materials"] = Convert.ToDateTime(dtcTrainingMaterials.SelectedDate.ToString());
        //                    item["Delegate availabilty sent"] = Convert.ToDateTime(dtcDelegateAvailabilitySent.SelectedDate.ToString());
        //                    item["Invites sent"] = Convert.ToDateTime(dtcInvitesSent.SelectedDate.ToString());
        //                    item["Delegate pre-work"] = Convert.ToDateTime(dtcDelegatePreWork.SelectedDate.ToString());
        //                    item["Customized training kp1"] = Convert.ToDateTime(dtcCustomizedTrainingKP1EvaluationForm.SelectedDate.ToString());
        //                    item["Itf form 6a"] = Convert.ToDateTime(dtcITFForm6A.SelectedDate.ToString());
                             
        //                    item["Facilitators kit"] = Convert.ToDateTime(dtcFacilitatorKit.SelectedDate.ToString());
        //                    item["Delegate available to line manager"] = Convert.ToDateTime(dtcDelegateAvailabilityToLineManagers.SelectedDate.ToString());
        //                    item["Training invitation"] = Convert.ToDateTime(dtcTrainingInvitation.SelectedDate.ToString());
        //                    item["Venue preparation"] = Convert.ToDateTime(dtcVenuePreparation.SelectedDate.ToString());
        //                    item["Hall"] = Convert.ToDateTime(dtcHall.SelectedDate.ToString());
        //                    item["Projector"] = Convert.ToDateTime(dtcProjector.SelectedDate.ToString());
        //                    item["Hall arrangement"] = Convert.ToDateTime(dtcHallArrangement.SelectedDate.ToString());
        //                    item["Location"] = Convert.ToDateTime(dtcLocation.SelectedDate.ToString());
        //                    item["Tea break and lunch"] = Convert.ToDateTime(dtcTeaBreakAndLunch.SelectedDate.ToString());
        //                    item["Seating arrangement"] = Convert.ToDateTime(dtcSeatingArrangement.SelectedDate.ToString());
        //                    item["Location branding"] = Convert.ToDateTime(dtcLocationBranding.SelectedDate.ToString());
                             
        //                    item["Programme materials"] = Convert.ToDateTime(dtcProgrammeMaterials.SelectedDate.ToString());
        //                    item["Wall charts"] = Convert.ToDateTime(dtcWallCharts.SelectedDate.ToString());
        //                    item["Videos/dvd"] = Convert.ToDateTime(dtcVidoesOrDVD.SelectedDate.ToString());
        //                    item["Participant guide"] = Convert.ToDateTime(dtcParticipantGuide.SelectedDate.ToString());
        //                    item["Presentation slides"] = Convert.ToDateTime(dtcPresentationSlides.SelectedDate.ToString());
        //                    item["Learning aids"] = Convert.ToDateTime(dtcLearningAidsActivityCardsOrCaseStudies.SelectedDate.ToString());
        //                    item["Stationaries"] = Convert.ToDateTime(dtcStationaries.SelectedDate.ToString());
        //                    item["External speakers"] = Convert.ToDateTime(dtcExternalSpeakers.SelectedDate.ToString());
        //                    item["Pre-course"] = Convert.ToDateTime(dtcPreCourse.SelectedDate.ToString());
        //                    item["Kp1 evaluation"] = Convert.ToDateTime(dtcKP1Evaluation.SelectedDate.ToString());
        //                    item["Certificates"] = Convert.ToDateTime(dtcCertificates.SelectedDate.ToString());
 
        //                    item["Attendance list"] = Convert.ToDateTime(dtcAttendanceList.SelectedDate.ToString());
        //                    item["Photographs"] = Convert.ToDateTime(dtcPhotographs.SelectedDate.ToString());
        //                    item["Post assessment"] = Convert.ToDateTime(dtcPostAssessment.SelectedDate.ToString());
        //                    item.SystemUpdate();
        //                    list.Update();

        //                    //gets name of the workflow Initiator to send an update message
        //                    SPFieldUserValue initiator = new SPFieldUserValue(web, item["Created By"].ToString());

        //                    string tasksID = item["Tasks ID"].ToString();
        //                    string initiatorName = initiator.User.Name;
        //                    string to = initiator.User.Email;
        //                    string from = "workflows@mtn.com";
        //                    string changeRequester = SPContext.Current.Web.CurrentUser.Name;
        //                    string updateChangeLink = web.Url + "/_layouts/15/HR_learning_And_Mgmt/HRLearningManagementRequestWFTask.aspx?ID=" + tasksID;

        //                    string Body = "Dear " + initiator.User.Name + ",<br><br>";
        //                    //Body += "A new Request from " + _Requester.DisplayName + " is awaiting your kind approval. ";

        //                    Body += "Your requested update has been done by " + initiatorName + ".<br><br> Click <a href='" + updateChangeLink + "'>here</a> to view the updated request. <br><br>";

        //                    Body += "<br><br>Kind Regards,<br>";
        //                    Body += "HR Learning Management";
        //                    Body += "<br><br>NOTE: This email message is auto generated, Please do not reply to the email address.";
        //                    EMailHandler.SendRequestChangeEmail(to, from, null, "HR Learning Management Update", Body);
                                                        
        //                }
        //                Response.Redirect(SPContext.Current.Web.Url);
        //            }
        //        }               
        //    }
        //    catch (Exception ex)
        //    {
        //        //Response.Write("<br>"+ ex.ToString());
        //        //File.WriteAllText(@"C:\Temp\UpdaterequestError.log", ex.ToString());
        //        ErrorHandler.LogToWindows(ex, "HR_Learning_and_Management, NewRequest");
        //    }
        //} 

        protected void _btnCancelRequest_Click(object sender, EventArgs e)
        {
            Response.Redirect(Web.Url + "/_layouts/HR_learning_And_Mgmt/NewForm.aspx");
        }

        protected void updateRequest_Click(object sender, EventArgs e)
        {
            string faculty = string.Empty;
            foreach (ListItem item in this.cblFaculty.Items)
            {
                if (item.Selected)
                {
                    faculty += item + ",";
                }
            }

            string equipment = string.Empty;
            foreach (ListItem item in this.cblEquipmentsRequired.Items)
            {
                if (item.Selected)
                {
                    equipment += item + ",";
                }
            }

            try
            {               
                    using (SPSite site = new SPSite(SPContext.Current.Web.Url))
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            int _ItemID = int.Parse(base.Request.Params["ID"]);
                            SPList list = web.Lists["HR_Learning_And_Mgmt_List"];
                            SPListItem item = list.GetItemById(_ItemID);
                            //  item["Title"] = "HR_learning_And_Mgmt" + DateTime.Now;
                            //item["User name"] = SPContext.Current.Web.CurrentUser.Name;

                            item["Programme name"] = txtProgrammeName.Text;
                            item["Programme venue"] = txtProgrammeVenue.Text;
                            item["Line manager"] = Helper.GetUserValue(peLineManager, web);
                            item["Senior manager"] = Helper.GetUserValue(peSeniorManager, web);
                            item["General manager"] = Helper.GetUserValue(peGeneralManager, web);
                            item["Programme date"] = Convert.ToDateTime(dtcProgrammeDate.SelectedDate.ToString());
                            item["Provider"] = ddlProvider.SelectedItem.Text;
                            item["Is it mtn academy"] = rblIsMTNAcademyProgramme.SelectedItem.Value;
                            item["Programme coordinator"] = Helper.GetUserValue(peProgrammeCoordinator, web);
                            item["Programme facilitator"] = txtProgrammeFacilitator.Text;
                            item["Faculty"] = faculty;

                            item["Facilitator last programme and date"] = txtFacilitatorLastProgrammeAndDate.Text;
                            item["Facilitator last evaluation score"] = txtFacilitatorLastEvaluationScore.Text;
                            item["Venue arrangement"] = rblVenueArrangement.SelectedItem.Value;
                            item["Equipments required"] = equipment;
                            item["Programme coordination checklist"] = rblProgrammeCoordinationChecklist.SelectedItem.Text;
                            item["Programme content"] = Convert.ToDateTime(dtcProgrammeContent.SelectedDate.ToString());
                            item["Case study"] = Convert.ToDateTime(dtcCaseStudy.SelectedDate.ToString());
                            item["Activities"] = Convert.ToDateTime(dtcActivities.SelectedDate.ToString());
                            item["Venue booking"] = Convert.ToDateTime(dtcBooking.SelectedDate.ToString());
                            item["Training materials"] = Convert.ToDateTime(dtcTrainingMaterials.SelectedDate.ToString());
                            item["Delegate availabilty sent"] = Convert.ToDateTime(dtcDelegateAvailabilitySent.SelectedDate.ToString());
                            item["Invites sent"] = Convert.ToDateTime(dtcInvitesSent.SelectedDate.ToString());
                            item["Delegate pre-work"] = Convert.ToDateTime(dtcDelegatePreWork.SelectedDate.ToString());
                            item["Customized training kp1"] = Convert.ToDateTime(dtcCustomizedTrainingKP1EvaluationForm.SelectedDate.ToString());
                            item["Itf form 6a"] = Convert.ToDateTime(dtcITFForm6A.SelectedDate.ToString());

                            item["Facilitators kit"] = Convert.ToDateTime(dtcFacilitatorKit.SelectedDate.ToString());
                            item["Delegate available to line manager"] = Convert.ToDateTime(dtcDelegateAvailabilityToLineManagers.SelectedDate.ToString());
                            item["Training invitation"] = Convert.ToDateTime(dtcTrainingInvitation.SelectedDate.ToString());
                            item["Venue preparation"] = Convert.ToDateTime(dtcVenuePreparation.SelectedDate.ToString());
                            item["Hall"] = Convert.ToDateTime(dtcHall.SelectedDate.ToString());
                            item["Projector"] = Convert.ToDateTime(dtcProjector.SelectedDate.ToString());
                            item["Hall arrangement"] = Convert.ToDateTime(dtcHallArrangement.SelectedDate.ToString());
                            item["Location"] = Convert.ToDateTime(dtcLocation.SelectedDate.ToString());
                            item["Tea break and lunch"] = Convert.ToDateTime(dtcTeaBreakAndLunch.SelectedDate.ToString());
                            item["Seating arrangement"] = Convert.ToDateTime(dtcSeatingArrangement.SelectedDate.ToString());
                            item["Location branding"] = Convert.ToDateTime(dtcLocationBranding.SelectedDate.ToString());

                            item["Programme materials"] = Convert.ToDateTime(dtcProgrammeMaterials.SelectedDate.ToString());
                            item["Wall charts"] = Convert.ToDateTime(dtcWallCharts.SelectedDate.ToString());
                            item["Videos/dvd"] = Convert.ToDateTime(dtcVidoesOrDVD.SelectedDate.ToString());
                            item["Participant guide"] = Convert.ToDateTime(dtcParticipantGuide.SelectedDate.ToString());
                            item["Presentation slides"] = Convert.ToDateTime(dtcPresentationSlides.SelectedDate.ToString());
                            item["Learning aids"] = Convert.ToDateTime(dtcLearningAidsActivityCardsOrCaseStudies.SelectedDate.ToString());
                            item["Stationaries"] = Convert.ToDateTime(dtcStationaries.SelectedDate.ToString());
                            item["External speakers"] = Convert.ToDateTime(dtcExternalSpeakers.SelectedDate.ToString());
                            item["Pre-course"] = Convert.ToDateTime(dtcPreCourse.SelectedDate.ToString());
                            item["Kp1 evaluation"] = Convert.ToDateTime(dtcKP1Evaluation.SelectedDate.ToString());
                            item["Certificates"] = Convert.ToDateTime(dtcCertificates.SelectedDate.ToString());

                            item["Attendance list"] = Convert.ToDateTime(dtcAttendanceList.SelectedDate.ToString());
                            item["Photographs"] = Convert.ToDateTime(dtcPhotographs.SelectedDate.ToString());
                            item["Post assessment"] = Convert.ToDateTime(dtcPostAssessment.SelectedDate.ToString());
                            item.Update();

                            //gets name of the workflow Initiator to send an update message
                            SPFieldUserValue initiator = new SPFieldUserValue(web, item["Created By"].ToString());

                            string tasksID = item["Tasks ID"].ToString();
                            string initiatorName = initiator.User.Name;
                            string to = item["ChangeRequesterEmail"].ToString();
                            string from = "workflows@mtn.com";
                            string changeRequester = item["Change Requester"].ToString();   
                            string updateChangeLink = web.Url + "/_layouts/HR_learning_And_Mgmt/HRLearningManagementRequestWFTask.aspx?List=a932a1ba-ba6e-43c3-857e-b4f8a36fb09a&ID=" +
                                tasksID + "&Source=" + web.Url + "HumanResources/Lists/HrLearningManagementRequestTasks";

                            string Body = "Dear " + changeRequester + ",<br><br>";
                            //Body += "A new Request from " + _Requester.DisplayName + " is awaiting your kind approval. ";

                            Body += "Your requested update has been done by " + initiatorName + ".<br><br> Click <a href='" + updateChangeLink + "'>here</a> to view the updated request. <br><br>";

                            Body += "<br><br>Kind Regards,<br>";
                            Body += "HR Learning Management";
                            Body += "<br><br>NOTE: This email message is auto generated, Please do not reply to the email address.";
                            EMailHandler.SendRequestChangeEmail(to, from, null, "HR Learning Management Update", Body);
                                                    
                        }
                    Response.Redirect(SPContext.Current.Web.Url);
                }                
            }
            catch (Exception ex)
            {
                File.WriteAllText(@"C:\Temp\UpdaterequestError.log", ex.ToString());
                //ErrorHandler.LogToWindows(ex, "HR_Learning_and_Management, NewRequest");
            }
        }
    }
}
